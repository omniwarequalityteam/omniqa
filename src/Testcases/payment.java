package Testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class payment {

	public static WebDriver	driver = new FirefoxDriver();
	public static String downloadPath = Constant.Path_Downloads;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	/*
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	 */

	@Test 
	public  void payment_Filters () throws Exception
	{	
		FirefoxProfile prof = new FirefoxProfile();
		driver	= new FirefoxDriver(prof);

		driver.get(Constant.URL);
		System.out.println("payment_Filters Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			String expectedTitle = "TraknPay Successful Payments Report";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Successful Payments Report page");

			//Define implict Wait           	
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	 
			// TNP ID.
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Customer Name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[3]"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Customer Phone
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng2);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Invoice No.
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[5]"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(strng3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Transaction ID.
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element4 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[6]"));
			String strng4 = element4.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Payment DateTime
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[5]")).click();
			Thread.sleep(3000);
			/*

			// dropdown 
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("All Payment Method");
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			
	//dropdown 
	  WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
	  Select mySelect2= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Card Visa");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			 */
			// dropdown 
			WebElement mySelectElm4 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect4 = new Select(mySelectElm4);
			mySelect4.selectByVisibleText("Manual");
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click(); 

/*
			//  dropdown 
			WebElement mySelectElm3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect3 = new Select(mySelectElm3);
			mySelect3.selectByVisibleText("Netbanking");
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();


	*/		//Received Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element6 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[10]"));
			String strng6 = element6.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(strng6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Charges
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element7 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[11]"));
			String strng7 = element7.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(strng7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Service tax
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element8 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[12]"));
			String strng8 = element8.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/input")).sendKeys(strng8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Settlement Amount(Business)
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element9 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[13]"));
			String strng9 = element9.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[13]/input")).sendKeys(strng9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[13]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Settlement ID.
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element10 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[14]"));
			String strng10 = element10.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[14]/input")).sendKeys(strng10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[14]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Vendor ID.
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[15]"));
			String strng11 = element11.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[15]/input")).sendKeys(strng11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[15]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Nendor Name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element12 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[16]"));
			String strng12 = element12.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[16]/input")).sendKeys(strng12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[16]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			// Vendor split percentage
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			WebElement element13 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[17]"));
			String strng13 = element13.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(strng13);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

	// Vendor split percentage
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
	WebElement element14 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[18]"));
	String strng14 = element14.getText();
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[18]/input")).sendKeys(strng14);
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[18]/input")).sendKeys(Keys.RETURN);
	Thread.sleep(3000);

	// Settlement Amount(Vendor) 
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
	WebElement element15 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[19]"));
	String strng15 = element15.getText();
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[19]/input")).sendKeys(strng15);
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[19]/input")).sendKeys(Keys.RETURN);
	Thread.sleep(3000);

	// Bank Reference
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
	WebElement element16 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[20]"));
	String strng16 = element16.getText();
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[20]/input")).sendKeys(strng16);
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[20]/input")).sendKeys(Keys.RETURN);
	Thread.sleep(3000);

	// Settlement Description
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
	WebElement element17 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[21]"));
	String strng17 = element17.getText();
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[21]/input")).sendKeys(strng17);
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[21]/input")).sendKeys(Keys.RETURN);
	Thread.sleep(3000);

			// Settlement Date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[5]")).click();
			Thread.sleep(3000);

			LogOut_Page.logout(driver); 	 
		}
	}

	@Test 
	public  void payment_previous_next() throws Exception
	{	
		driver.get(Constant.URL);
		System.out.println("-----------------------------------");
		System.out.println("payment_previous_nextStart");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			String expectedTitle = "TraknPay Successful Payments Report";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Successful Payments Report page");

			/*previous and next*/
			//2 
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
			Thread.sleep(5000);
			//3
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[4]/a")).click();
			Thread.sleep(5000);

			// previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);
			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			LogOut_Page.logout(driver); 
		}
	}

	@Test 
	public  void payment_show_entries() throws Exception
	{			
		driver.get(Constant.URL);
		System.out.println("-----------------------------------");
		System.out.println("payment_show_entries Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	


			String expectedTitle = "TraknPay Successful Payments Report";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Successful Payments Report page");

			/* dropdown */
			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("10");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect2= new Select(mySelectElm);
			mySelect.selectByVisibleText("25");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect3= new Select(mySelectElm);
			mySelect.selectByVisibleText("50");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect4= new Select(mySelectElm);
			mySelect.selectByVisibleText("100");
			Thread.sleep(9000);

			LogOut_Page.logout(driver); 

		}
	}

	
	@Test 
	public  void payment_Excel_File_download() throws Exception
	{	
		System.out.println("---------------------------------");
		System.out.println("---payment_Excel_File_download---");
		System.out.println("---------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_excel());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);

		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			//click on download          
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[1]/i")).click(); 
			Thread.sleep(9000);
			LogOut_Page.logout(driver); 
		}
	}
	public static FirefoxProfile FirefoxDriverProfile_excel() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;
	}   



	@Test
	public void payment_Excel_File_Read() throws IOException {
		System.out.println("-------------------------------------");
		System.out.println("payment_Excel_File_Page");
		System.out.println("-------------------------------------");		
		String excelFilePath = "D://DownloadFiles//TraknPay Payment History.xlsx";
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					System.out.print(cell.getStringCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					System.out.print(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_NUMERIC:
					System.out.print(cell.getNumericCellValue());
					break;
				}
				System.out.print(" - ");
			}
			System.out.println();
		}
		workbook.close();
		inputStream.close();
	}


	@Test 
	public  void payment_pdf_File_download() throws Exception
	{	
		System.out.println("-------------------------------------");
		System.out.println("---payment_pdf_File_download---");
		System.out.println("-------------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_pdf());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);

		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			//click on download          
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[2]")).click(); 
			Thread.sleep(9000);
			LogOut_Page.logout(driver); 
		}
	}

	public static FirefoxProfile FirefoxDriverProfile_pdf() throws Exception {
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("browser.download.folderList", 2);
		firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
		firefoxProfile.setPreference("browser.download.dir",downloadPath);
		firefoxProfile.setPreference("browser.helperApps.neverAsk.openFile", "application/pdf");
		firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
		firefoxProfile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		firefoxProfile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		firefoxProfile.setPreference("browser.download.manager.focusWhenStarting", false);
		firefoxProfile.setPreference("browser.download.manager.useWindow", false);
		firefoxProfile.setPreference("browser.download.manager.showAlertOnComplete", false);
		firefoxProfile.setPreference("browser.download.manager.closeWhenDone", false);
		// disable Firefox's built-in PDF viewer
		firefoxProfile.setPreference("pdfjs.disabled", true);

		//disable Adobe Acrobat PDF preview plugin
		firefoxProfile.setPreference("plugin.scan.plid.all", false);
		firefoxProfile.setPreference("plugin.scan.Acrobat", "99.0");
		return firefoxProfile;


	}   

	@Test



	public void payment_pdf_File_Read() throws IOException {
		System.out.println("-------------------------------------");
		System.out.println("payment_Excel_File_Page");
		System.out.println("-------------------------------------");		
		String excelFilePath = "P://seleniumdownloads//TraknPay Settlement History.pdf";
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					System.out.print(cell.getStringCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					System.out.print(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_NUMERIC:
					System.out.print(cell.getNumericCellValue());
					break;
				}
				System.out.print(" - ");
			}
			System.out.println();
		}

		workbook.close();
		inputStream.close();
	}


	@Test 
	public  void payment_Toggle_column() throws Exception
	{	

		driver.get(Constant.URL);
		System.out.println("-----------------------------------");
		System.out.println("payment_Toggle_column Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			String expectedTitle = "TraknPay Successful Payments Report";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Successful Payments Report page");

			// Click on Toggle Column 
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[4]")).click();
			Thread.sleep(3000);

			//TNP ID             
			driver.findElement(By.xpath("html/body/div[4]/a[1]")).click();
			Thread.sleep(3000);

			//Customer Name 
			driver.findElement(By.xpath("html/body/div[4]/a[2]/span")).click();
			Thread.sleep(3000);

			//Customer Phone 
			driver.findElement(By.xpath("html/body/div[4]/a[3]/span")).click();
			Thread.sleep(4000);

			//Invoice No
			driver.findElement(By.xpath("html/body/div[4]/a[4]/span")).click();
			Thread.sleep(3000);

			//Transaction ID
			driver.findElement(By.xpath("html/body/div[4]/a[5]/span")).click();
			Thread.sleep(3000);

			//Payment Date Time
			driver.findElement(By.xpath("html/body/div[4]/a[6]/span")).click();
			Thread.sleep(3000);

			//Payment Mode
			driver.findElement(By.xpath("html/body/div[4]/a[7]/span")).click();
			Thread.sleep(3000);

			//payment channel
			driver.findElement(By.xpath("html/body/div[4]/a[8]/span")).click();
			Thread.sleep(3000);

			// Received amount 
			driver.findElement(By.xpath("html/body/div[4]/a[9]/span")).click();
			Thread.sleep(3000);

			//charges 	
			driver.findElement(By.xpath("html/body/div[4]/a[10]/span")).click();
			Thread.sleep(9000);

			//Services Tax 	
			driver.findElement(By.xpath("html/body/div[4]/a[11]/span")).click();
			Thread.sleep(3000);

			// Settlement Amount 	
			driver.findElement(By.xpath("html/body/div[4]/a[12]/span")).click();
			Thread.sleep(3000);

			// Settlement ID	
			driver.findElement(By.xpath("html/body/div[4]/a[13]/span")).click();
			Thread.sleep(3000);

			// Vendor ID  	
			driver.findElement(By.xpath("html/body/div[4]/a[14]/span")).click();
			Thread.sleep(3000);

			//Vendor Name 	
			driver.findElement(By.xpath("html/body/div[4]/a[15]/span")).click();
			Thread.sleep(3000);

			//Vendor split Percentage 
			driver.findElement(By.xpath("html/body/div[4]/a[16]/span")).click();
			Thread.sleep(3000);

			//Settlement Amount 
			driver.findElement(By.xpath("html/body/div[4]/a[17]/span")).click();
			Thread.sleep(3000);

			//Bank Reference 
			driver.findElement(By.xpath("html/body/div[4]/a[18]/span")).click();
			Thread.sleep(3000);

			//Settlement Description		
			driver.findElement(By.xpath("html/body/div[4]/a[19]/span")).click();
			Thread.sleep(3000);

			//Settlement Date	
			driver.findElement(By.xpath("html/body/div[4]/a[20]/span")).click();
			Thread.sleep(9000);;
			/*
		//click side
			driver.findElement(By.xpath("html/body/div[6]")).click();
			Thread.sleep(3000);

			// Click on Toggle Column 
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[4]/i")).click();
			Thread.sleep(3000);
*/
			//TNP ID             
			driver.findElement(By.xpath("html/body/div[4]/a[1]")).click();
			Thread.sleep(3000);

			//Customer Name 
			driver.findElement(By.xpath("html/body/div[4]/a[2]/span")).click();
			Thread.sleep(3000);

			//Customer Phone 
			driver.findElement(By.xpath("html/body/div[4]/a[3]/span")).click();
			Thread.sleep(4000);

			//Invoice No
			driver.findElement(By.xpath("html/body/div[4]/a[4]/span")).click();
			Thread.sleep(3000);

			//Transaction ID
			driver.findElement(By.xpath("html/body/div[4]/a[5]/span")).click();
			Thread.sleep(3000);

			//Payment Date Time
			driver.findElement(By.xpath("html/body/div[4]/a[6]/span")).click();
			Thread.sleep(3000);

			//Payment Mode
			driver.findElement(By.xpath("html/body/div[4]/a[7]/span")).click();
			Thread.sleep(3000);

			//payment channel
			driver.findElement(By.xpath("html/body/div[4]/a[8]/span")).click();
			Thread.sleep(3000);

			// Received amount 
			driver.findElement(By.xpath("html/body/div[4]/a[9]/span")).click();
			Thread.sleep(3000);

			//charges 	
			driver.findElement(By.xpath("html/body/div[4]/a[10]/span")).click();
			Thread.sleep(9000);

			//Services Tax 	
			driver.findElement(By.xpath("html/body/div[4]/a[11]/span")).click();
			Thread.sleep(3000);

			// Settlement Amount 	
			driver.findElement(By.xpath("html/body/div[4]/a[12]/span")).click();
			Thread.sleep(3000);

			// Settlement ID	
			driver.findElement(By.xpath("html/body/div[4]/a[13]/span")).click();
			Thread.sleep(3000);

			// Vendor ID  	
			driver.findElement(By.xpath("html/body/div[4]/a[14]/span")).click();
			Thread.sleep(3000);

			//Vendor Name 	
			driver.findElement(By.xpath("html/body/div[4]/a[15]/span")).click();
			Thread.sleep(3000);

			//Vendor split Percentage 
			driver.findElement(By.xpath("html/body/div[4]/a[16]/span")).click();
			Thread.sleep(3000);

			//Settlement Amount 
			driver.findElement(By.xpath("html/body/div[4]/a[17]/span")).click();
			Thread.sleep(3000);

			//Bank Reference 
			driver.findElement(By.xpath("html/body/div[4]/a[18]/span")).click();
			Thread.sleep(3000);

			//Settlement Description		
			driver.findElement(By.xpath("html/body/div[4]/a[19]/span")).click();
			Thread.sleep(3000);

			//Settlement Date	
			driver.findElement(By.xpath("html/body/div[4]/a[20]/span")).click();
			Thread.sleep(9000);

			//click on side
			driver.findElement(By.xpath("html/body/div[5]")).click();

			LogOut_Page.logout(driver); 

		}
	}


	@Test 
	public  void payment_Print_file() throws Exception
	{	
		System.out.println("-------------------------------------");
		System.out.println("---payment_Print_File---");
		System.out.println("-------------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_print());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);

		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			//click on download          
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[3]/i")).click(); 
			Thread.sleep(9000);
			LogOut_Page.logout(driver); 
		}
	}
	public static FirefoxProfile FirefoxDriverProfile_print() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;
	}   		


	@Test
	public  void payment_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("payment session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.payment)).click();
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[18]/ul/li/a/span")).click();	     	

			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}