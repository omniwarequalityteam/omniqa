package Testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class Settlement {
	
	public static WebDriver	driver = new FirefoxDriver();
	public static String downloadPath = Constant.Path_Downloads;
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	/*
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
*/
	
@Test 
	public  void Settlement_Filters () throws Exception
 {	
	driver.get(Constant.URL);
	System.out.println("Settlement_Filters Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement,"Sheet1");
	 driver.findElement(By.xpath(Xpath.Settlement)).click();
		    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

	//Define implict Wait           
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	 
	  
	  //Settlement ID.
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[1]"));
	  String strng= element.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(strng);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);	 

	//Settlement DateTime
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).click();
	  driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[6]")).click();
		
				//Settlement Description
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element2 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[3]/div"));
	  String strng2= element2.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(strng2);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//dropdown 
	  WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("All SettlementStatus");
	  Thread.sleep(5000);

	  //dropdown
	  WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/select")); 
	  Select mySelect2= new Select(mySelectElm);
	  mySelect.selectByVisibleText("completed");
	  Thread.sleep(5000);

	  //dropdown 
	  WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/select")); 
	  Select mySelect3= new Select(mySelectElm);
	  mySelect.selectByVisibleText("pending");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[12]/button")).click();

	  //Bank refer
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element3 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[5]/div"));
	  String strng3= element3.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/input")).sendKeys(strng3);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//Payout Amount
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element4 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]"));
	  String strng4= element4.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/input")).sendKeys(strng4);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//account name
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element5 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[7]"));
	  String strng5= element5.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/input")).sendKeys(strng5);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//account number
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element6 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[8]"));
	  String strng6= element6.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[8]/input")).sendKeys(strng6);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[8]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//IFSC Code
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element7 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[9]"));
	  String strng7= element7.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/input")).sendKeys(strng7);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

		//bank name
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element8 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[10]"));
	  String strng8= element8.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[10]/input")).sendKeys(strng8);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[10]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

	//bank branch
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element9 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[11]"));
	  String strng9= element9.getText();
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[11]/input")).sendKeys(strng9);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[11]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);

      LogOut_Page.logout(driver); 
				 					}
 					}
	@Test 
	public  void Settlement_previous_next() throws Exception
 {	
	driver.get(Constant.URL);
	System.out.println("-----------------------------------");
	System.out.println("Settlement_previous_nextStart");
	System.out.println("--------------------------------");
	for(int i=1 ; i<2; i++ )
	{
	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	System.out.println("loaded: " +i);
	String sUserName = ExcelUtils.getCellData(i,1);
	String sPassword = ExcelUtils.getCellData(i,2);
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement,"Sheet1");
    driver.findElement(By.xpath(Xpath.Settlement)).click();
	    	
    String expectedTitle = "TraknPay Settlement History";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("Traknpay Settlement History page");
		 
 /*previous and next*/
    //2 
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
   Thread.sleep(5000);
// previous
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
   Thread.sleep(3000);
 //next
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
   Thread.sleep(4000);
		    
   LogOut_Page.logout(driver); 
  	 			}
 			}

	@Test 
	public  void Settlement_show_entries() throws Exception
 {
	driver.get(Constant.URL);
	System.out.println("-----------------------------------");
	System.out.println("Settlement_show_entries Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement,"Sheet1");
     driver.findElement(By.xpath(Xpath.Settlement)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");
		 
	 /* dropdown */
		WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect= new Select(mySelectElm);
		mySelect.selectByVisibleText("10");
		Thread.sleep(9000);

/* dropdown */
		WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect2= new Select(mySelectElm);
		mySelect.selectByVisibleText("25");
		Thread.sleep(9000);

/* dropdown */
		WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect3= new Select(mySelectElm);
		mySelect.selectByVisibleText("50");
		Thread.sleep(9000);

/* dropdown */
		WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect4= new Select(mySelectElm);
		mySelect.selectByVisibleText("100");
		Thread.sleep(9000);
   LogOut_Page.logout(driver); 
  	 			}
 			}
	
	
	@Test 
	public  void Settlement_Excel_File_download() throws Exception
 {	
		System.out.println("-------------------------------------");
		System.out.println("---Settlement_Excel_File_download---");
		System.out.println("-------------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_excel());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);
		
			for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			 System.out.println("loaded: " +i);
			 String sUserName = ExcelUtils.getCellData(i,1);
			 String sPassword = ExcelUtils.getCellData(i,2);
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			        
	    driver.findElement(By.xpath(Xpath.Settlement)).click();
//click on download          
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[1]/i")).click(); 
		Thread.sleep(9000);
		LogOut_Page.logout(driver); 
}
 }
		public static FirefoxProfile FirefoxDriverProfile_excel() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();
	
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;
		}   


		
 @Test
	 public void Settlement_Excel_File_Read() throws IOException {
    System.out.println("-------------------------------------");
		 System.out.println("Settlement_Excel_File_Page");
		 System.out.println("-------------------------------------");		
	    String excelFilePath = "D://DownloadFiles//TraknPay Settlement History.xlsx";
	    FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
	     
	    Workbook workbook = new XSSFWorkbook(inputStream);
	    Sheet firstSheet = workbook.getSheetAt(0);
	    Iterator<Row> iterator = firstSheet.iterator();
	     
	    while (iterator.hasNext()) {
	        Row nextRow = iterator.next();
	        Iterator<Cell> cellIterator = nextRow.cellIterator();
	         
	        while (cellIterator.hasNext()) {
	            Cell cell = cellIterator.next();
	             
	            switch (cell.getCellType()) {
	                case Cell.CELL_TYPE_STRING:
	                    System.out.print(cell.getStringCellValue());
	                    break;
	                case Cell.CELL_TYPE_BOOLEAN:
	                    System.out.print(cell.getBooleanCellValue());
	                    break;
	                case Cell.CELL_TYPE_NUMERIC:
	                    System.out.print(cell.getNumericCellValue());
	                    break;
	            }
	            System.out.print(" - ");
	        }
	        System.out.println();
	    }
	     
	    workbook.close();
	    inputStream.close();
	    
	}

 
 @Test 
	public  void Settlement_pdf_File_download() throws Exception
{	
		System.out.println("-------------------------------------");
		System.out.println("---Settlement_pdf_File_download---");
		System.out.println("-------------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_pdf());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);
		
		for(int i=1 ; i<2; i++ ) {
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			 System.out.println("loaded: " +i);
			 String sUserName = ExcelUtils.getCellData(i,1);
			 String sPassword = ExcelUtils.getCellData(i,2);
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			        
	    driver.findElement(By.xpath(Xpath.Settlement)).click();
//click on download          
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[2]/i")).click(); 
		Thread.sleep(9000);
		LogOut_Page.logout(driver); 
}
}
 
 public static FirefoxProfile FirefoxDriverProfile_pdf() throws Exception {
 FirefoxProfile firefoxProfile = new FirefoxProfile();
 firefoxProfile.setPreference("browser.download.folderList", 2);
 firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
 firefoxProfile.setPreference("browser.download.dir",downloadPath);
 firefoxProfile.setPreference("browser.helperApps.neverAsk.openFile", "application/pdf");
 firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
 firefoxProfile.setPreference("browsser.helperApps.alwaysAsk.force", false);
 firefoxProfile.setPreference("browser.download.manager.alertOnEXEOpen", false);
 firefoxProfile.setPreference("browser.download.manager.focusWhenStarting", false);
 firefoxProfile.setPreference("browser.download.manager.useWindow", false);
 firefoxProfile.setPreference("browser.download.manager.showAlertOnComplete", false);
 firefoxProfile.setPreference("browser.download.manager.closeWhenDone", false);
 // disable Firefox's built-in PDF viewer
 firefoxProfile.setPreference("pdfjs.disabled", true);

//disable Adobe Acrobat PDF preview plugin
 firefoxProfile.setPreference("plugin.scan.plid.all", false);
		 firefoxProfile.setPreference("plugin.scan.Acrobat", "99.0");
  return firefoxProfile;
 

} 


 @Test
 public void Settlement_pdf_File_Read() throws IOException {
System.out.println("-------------------------------------");
	 System.out.println("Settlement_Excel_File_Page");
	 System.out.println("-------------------------------------");		
    String excelFilePath = "D://DownloadFiles//TraknPay Settlement History.pdf";
    FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
     
    Workbook workbook = new XSSFWorkbook(inputStream);
    Sheet firstSheet = workbook.getSheetAt(0);
    Iterator<Row> iterator = firstSheet.iterator();
     
    while (iterator.hasNext()) {
        Row nextRow = iterator.next();
        Iterator<Cell> cellIterator = nextRow.cellIterator();
         
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
             
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    System.out.print(cell.getStringCellValue());
                    break;
                case Cell.CELL_TYPE_BOOLEAN:
                    System.out.print(cell.getBooleanCellValue());
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    System.out.print(cell.getNumericCellValue());
                    break;
            }
            System.out.print(" - ");
        }
        System.out.println();
    }
     
    workbook.close();
    inputStream.close();
    }

 
 @Test 
	public  void Settlement_Toggle_column() throws Exception
{	
	driver.get(Constant.URL);
	System.out.println("-----------------------------------");
	System.out.println("Settlement_Toggle_column Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement,"Sheet1");
  driver.findElement(By.xpath(Xpath.Settlement)).click();
	    	
  String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

	// Click on Toggle Column 
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/a[4]/i")).click();
	Thread.sleep(3000);
	
 //settlement date
	driver.findElement(By.xpath("html/body/div[4]/a[1]")).click();
	Thread.sleep(3000);
	
//settlement description
	driver.findElement(By.xpath("html/body/div[4]/a[2]/span")).click();
	Thread.sleep(3000);
	
//settlement status
	driver.findElement(By.xpath("html/body/div[4]/a[3]/span")).click();
	Thread.sleep(4000);

//Bank reference
	driver.findElement(By.xpath("html/body/div[4]/a[4]/span")).click();
	Thread.sleep(3000);
	
//Payout Amount
	driver.findElement(By.xpath("html/body/div[4]/a[5]/span")).click();
	Thread.sleep(3000);
	
//Account Name
	driver.findElement(By.xpath("html/body/div[4]/a[6]/span")).click();
	Thread.sleep(3000);
	
//Account Number
	driver.findElement(By.xpath("html/body/div[4]/a[7]/span")).click();
	Thread.sleep(3000);

//IFSC code
	driver.findElement(By.xpath("html/body/div[4]/a[8]/span")).click();
	Thread.sleep(3000);
	
//Bank Name
	driver.findElement(By.xpath("html/body/div[4]/a[9]/span")).click();
	Thread.sleep(3000);
	
//Bank branch	
	driver.findElement(By.xpath("html/body/div[4]/a[10]/span")).click();
	Thread.sleep(9000);
     
	driver.findElement(By.xpath("html/body/div[5]")).click();
		    
	// Click on Toggle Column 
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/a[4]/i")).click();
	Thread.sleep(3000);
	
 //settlement date
	driver.findElement(By.xpath("html/body/div[3]/a[1]/span")).click();
	Thread.sleep(3000);
	
//settlement description
	driver.findElement(By.xpath("html/body/div[3]/a[2]/span")).click();
	Thread.sleep(3000);
	
//settlement status
	driver.findElement(By.xpath("html/body/div[3]/a[3]/span")).click();
	Thread.sleep(4000);

//Bank reference
	driver.findElement(By.xpath("html/body/div[3]/a[4]/span")).click();
	Thread.sleep(3000);
	
//Payout Amount
	driver.findElement(By.xpath("html/body/div[3]/a[5]/span")).click();
	Thread.sleep(3000);
	
//Account Name
	driver.findElement(By.xpath("html/body/div[3]/a[6]/span")).click();
	Thread.sleep(3000);
	
//Account Number
	driver.findElement(By.xpath("html/body/div[3]/a[7]/span")).click();
	Thread.sleep(3000);

//IFSC code
	driver.findElement(By.xpath("html/body/div[3]/a[8]/span")).click();
	Thread.sleep(3000);
	
//Bank Name
	driver.findElement(By.xpath("html/body/div[3]/a[9]/span")).click();
	Thread.sleep(3000);
	
//Bank branch	
	driver.findElement(By.xpath("html/body/div[3]/a[10]/span")).click();
	Thread.sleep(9000);
	     
		driver.findElement(By.xpath("html/body/div[4]")).click();
		
LogOut_Page.logout(driver); 
	 			}
			}


 @Test 
	public  void Settlement_Print_file() throws Exception
{	
		System.out.println("-------------------------------------");
		System.out.println("---Settlement_Print_File---");
		System.out.println("-------------------------------------");
		driver = new FirefoxDriver(FirefoxDriverProfile_print());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);
		
		for(int i=1 ; i<2; i++ ) {
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
			 System.out.println("loaded: " +i);
			 String sUserName = ExcelUtils.getCellData(i,1);
			 String sPassword = ExcelUtils.getCellData(i,2);
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			        
	    driver.findElement(By.xpath(Xpath.Settlement)).click();
//click on download          
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder_wrapper']/div[1]/div[2]/div/a[3]/i")).click(); 
		Thread.sleep(9000);
		LogOut_Page.logout(driver); 
}
}
		public static FirefoxProfile FirefoxDriverProfile_print() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();
	
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;
		}   		
 

		@Test
		public  void Settlement_sessiontimeout() throws Exception 
		{	
			System.out.println("--------------------------------");
			System.out.println("Settlement session time out");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ )
			{
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				
				driver.findElement(By.xpath(Xpath.Settlement)).click();
				//start system time
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

				try
				{
					Thread.sleep(16*60*1000);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				date = new Date();
				Thread.sleep(2000);
				System.out.println(dateFormat.format(date)); 
			}		
		}
		}