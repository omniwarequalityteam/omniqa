package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class Challanpayments_page {
	public static WebDriver driver = new FirefoxDriver();
	public static String downloadPath = "C:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\downloaded files";

	@BeforeTest
	public void beforeTest() {
		//driver = new FirefoxDriver();
		driver.get(Constant.URL1);
	}
	/*@AfterMethod
	public void afterTest() throws Exception 
	{
			LogOut_Page.logout(driver);
	}*/

	@Test
	public void challanpayments_Exporttoexcelfile() throws Exception
	{

		System.out.println("export to excel file of challan payments");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{

			WebDriver driver = new FirefoxDriver(FirefoxDriverProfile1());	
			driver.manage().window().maximize();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.get(Constant.URL1);
			driver.findElement(By.name("busi_login")).sendKeys(sUserName);
			driver.findElement(By.name("password")).sendKeys(sPassword);
			driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath(Xpath.challanpayments)).click();

			//title check
			String expectedTitle = "TraknPay List Challan Payments";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/a[1]/i")).click();
			LogOut_Page.logout(driver);
		}

	}
	public static FirefoxProfile FirefoxDriverProfile1() throws Exception 
	{
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;



	}
	@Test
	public void challanpayments_Exporttopdffile() throws Exception
	{

		System.out.println("export to pdf file of challan payments");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			WebDriver driver = new FirefoxDriver(FirefoxDriverProfile11());	
			driver.manage().window().maximize();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.get(Constant.URL1);
			driver.findElement(By.name("busi_login")).sendKeys(sUserName);
			driver.findElement(By.name("password")).sendKeys(sPassword);
			driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath(Xpath.challanpayments)).click();

			//title check
			String expectedTitle = "TraknPay List Challan Payments";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/a[2]/i")).click();
			LogOut_Page.logout(driver); 
		}
	}
	public static FirefoxProfile FirefoxDriverProfile11() throws Exception 
	{
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;



	}



	@Test
	public void challanpayments_removetogglecolumn() throws Exception
	{

		System.out.println("Remove toggle column visibility in challan payments");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.challanpayments)).click();

			//title check
			String expectedTitle = "TraknPay List Challan Payments";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			//Toggle column visibility
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/a[4]/i")).click();
			//date
			driver.findElement(By.xpath("html/body/div[4]/a[1]")).click();
			//purpose of payment
			driver.findElement(By.xpath("html/body/div[4]/a[2]/span")).click();
			//Total
			driver.findElement(By.xpath("html/body/div[4]/a[3]/span")).click();
			//name
			driver.findElement(By.xpath("html/body/div[4]/a[4]/span")).click();
			//phone
			driver.findElement(By.xpath("html/body/div[4]/a[5]/span")).click();
			//email
			driver.findElement(By.xpath("html/body/div[4]/a[6]/span")).click();
			//status
			driver.findElement(By.xpath("html/body/div[4]/a[7]/span")).click();
			//out screen
			driver.findElement(By.xpath("html/body/div[5]")).click();
			LogOut_Page.logout(driver); 
		}
	}

	@Test
	public void challanpayments_addtogglecolumnvisible() throws Exception
	{

		System.out.println("Add Toggle column visibility of challan payments");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.challanpayments)).click();

			//title check
			String expectedTitle = "TraknPay List Challan Payments";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			//Toggle column visibility
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/a[4]/i")).click();
			//date
			driver.findElement(By.xpath("html/body/div[3]/a[1]/span")).click();
			//purpose of payment
			driver.findElement(By.xpath("html/body/div[3]/a[2]/span")).click();
			//Total
			driver.findElement(By.xpath("html/body/div[3]/a[3]/span")).click();
			//name
			driver.findElement(By.xpath("html/body/div[3]/a[4]/span")).click();
			//phone
			driver.findElement(By.xpath("html/body/div[3]/a[5]/span")).click();
			//email
			driver.findElement(By.xpath("html/body/div[3]/a[6]/span")).click();
			//status
			driver.findElement(By.xpath("html/body/div[3]/a[7]/span")).click();
			//out screen
			driver.findElement(By.xpath("html/body/div[4]")).click();
			LogOut_Page.logout(driver); 
		}
	}

	@Test
	public void challanpayments_filters() throws Exception
	{

		System.out.println("Add Toggle column visibility of challan payments");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.challanpayments)).click();

			//title check
			String expectedTitle = "TraknPay List Challan Payments";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			//filters
			//TNP id
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[4]")).click();
			Thread.sleep(2000);

			//purpose of testing
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[3]/div"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//total
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element2=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng2);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element3=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[5]/div"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(strng3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//phone
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element4=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[6]"));
			String strng4 = element4.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//email id
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element5=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[7]/div"));
			String strng5 = element5.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(strng5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//status
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("settled");
			Thread.sleep(2000);
			LogOut_Page.logout(driver); 
		}
	}	
	@Test
	public  void challanpayment_sessiontimeout() throws Exception 
	{	

		System.out.println("challan payment page session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	
			driver.findElement(By.xpath(Xpath.challanpayments)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 

}
	}



}

