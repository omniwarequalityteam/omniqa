package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class ManageInvoices_Page {

	public static WebDriver driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String Name;
	public static String downloadPath = "C:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\downloaded files";


	@BeforeTest
	public void beforeTest() 
	{
		// driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}


	@Test
	public void manageinvoice_show() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("manageinvoice_show");
			System.out.println("--------------------------------");
			System.out.println("loaded: " +i);

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[1]")).click();

			//Get text from page  

			String winHandleBefore = driver.getWindowHandle();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div[1]/div/a/span")).click();

			Thread.sleep(3000);

			// the set will contain only the child window now. Switch to child window and close it.
			for(String winHandle : driver.getWindowHandles()) 
			{
				driver.switchTo().window(winHandle);
			}
			driver.close();
			driver.switchTo().window(winHandleBefore);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[3]/button")).click();
			Thread.sleep(2000);
		}

	}
	@Test
	public void manageinvoice_edit() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("manageinvoice_edit");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			String Fans = ExcelUtils.getCellData(i,4);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			/*WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);*/

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[2]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.name("invd_f6")).clear();
			driver.findElement(By.name("invd_f6")).sendKeys(Fans);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);


		}
	}


	@Test

	public void manageinvoice_editerrorcatch() throws Exception
	{

		for(int i=1; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("manageinvoice_editerrorcatch");
			System.out.println("--------------------------------");
			System.out.println("loaded: " +i);


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");



			String Adress1 = ExcelUtils.getCellData(i,2);
			String Adress2 = ExcelUtils.getCellData(i,3);
			String Fans = ExcelUtils.getCellData(i,4);
			String Ac = ExcelUtils.getCellData(i,5);
			String Bulbs = ExcelUtils.getCellData(i,6);
			String Tv = ExcelUtils.getCellData(i,7);
			String mediumsize = ExcelUtils.getCellData(i,8);
			String editerrorwithoutname = ExcelUtils.getCellData(i,10);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			/*WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);*/

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[2]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.name("invd_name")).clear();
			driver.findElement(By.name("invd_name")).sendKeys(editerrorwithoutname);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Name field is required."))
			{

				System.out.println("pass:name field is required");

			}
			else
			{
				System.out.println("Fail:name field is required");


			}
		}
	}

	@Test

	public void  manageinvoice_send() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice_send");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			/*WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);*/


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[3]")).click();

			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();

			Thread.sleep(2000);

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}

	}
	@Test
	public void manageinvoice_manualpaymentcash() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice_manualpaymentcash");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");
			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[5]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);



			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));


			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.id("mapa_payment_amount")).sendKeys(strng);
			driver.findElement(By.id("mapa_payment_note")).sendKeys("amount has been paid ");
			driver.findElement(By.xpath(" html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}

	}

	@Test
	public void manageinvoice_updatemanualpayment() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice_updatemanualpayment");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			//filter

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("manually paid");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[5]/span")).click();
			Thread.sleep(2000);

			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/form/div[1]/div/ul/li[2]/a/i/span[1]")).click();
			driver.findElement(By.id("mapa_transaction_no")).sendKeys("123");
			driver.findElement(By.id("mapa_payment_bank")).sendKeys("SBM");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}

	}



	@Test
	public void manageinvoice_removemanualpayment() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice_removemanualpayment");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(3000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");
			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("manually paid");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[5]/span")).click();

			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			expectedTitle = "TraknPay Update Manual Payment";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay update manual payment page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/a[1]")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
		}


	}
	@Test
	public void manageinvoice_delete() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("manageinvoice_delete");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(1,1);
			String sPassword = ExcelUtils.getCellData(1,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  1, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(4000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");


			/* filter search*/


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect2= new Select(mySelectElm2);
			mySelect2.selectByVisibleText("viewed");
			Thread.sleep(2000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[4]/td[12]/a[6]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
		}
	}


	@Test
	public void manageinvoice_filtersearch() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice_filtersearch");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(3000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");


			/* filter search*/

			/*   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			   Thread.sleep(2000);
			   WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect= new Select(mySelectElm);
		       mySelect.selectByVisibleText("cancelled");
		       Thread.sleep(2000);

		       driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
		       WebElement mySelectElm9 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect9= new Select(mySelectElm9);
		       mySelect9.selectByVisibleText("viewed");
		       Thread.sleep(2000);

		       driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
		       WebElement mySelectElm5 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect5= new Select(mySelectElm5);
		       mySelect5.selectByVisibleText("expired");
		       Thread.sleep(2000);


		       driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
		       WebElement mySelectElm8 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect8= new Select(mySelectElm8);
		       mySelect8.selectByVisibleText("viewed");
		       Thread.sleep(2000);

		       driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
		       WebElement mySelectElm14 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect14= new Select(mySelectElm14);
		       mySelect14.selectByVisibleText("settled");
		       Thread.sleep(2000);


		       driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
		       WebElement mySelectElm141 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
		       Select mySelect141= new Select(mySelectElm141);
		       mySelect141.selectByVisibleText("manually paid");
		       Thread.sleep(2000);

			 */

			//TNP ID
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			// invoice no
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[4]")).click();
			Thread.sleep(3000);

			//Total
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element11=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
			String strng11 = element11.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Split
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm10 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
			Select mySelect10= new Select(mySelectElm10);
			mySelect10.selectByVisibleText("yes");
			Thread.sleep(3000);

			//Due date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).click();
			driver.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[4]")).click();
			Thread.sleep(3000);


			//Name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[7]/div"));
			String strng111 = element111.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(strng111);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Phone
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element2=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng2);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);


			//Email id
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement element3=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/div"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(strng3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);


			//entries
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm12 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
			Select mySelect12= new Select(mySelectElm12);
			mySelect12.selectByVisibleText("25");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm13 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
			Select mySelect13= new Select(mySelectElm13);
			mySelect13.selectByVisibleText("10");
			Thread.sleep(3000);

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm131 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect131= new Select(mySelectElm131);
			mySelect131.selectByVisibleText("no");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm1311 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect1311= new Select(mySelectElm1311);
			mySelect1311.selectByVisibleText("yes");
		}
	}



	@Test
	public void manageinvoice_duplicateinvoiceandedit() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("manageinvoice_duplicateinvoiceandedit");
			System.out.println("--------------------------------");
			System.out.println("loaded: " +i);


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");


			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String name =Name;

			String Phone = ExcelUtils.getCellData(i,13);
			String Email = ExcelUtils.getCellData(i,14);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			/*WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);*/


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[4]/span")).click();
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			System.out.println("Traknpay show invoice page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			ExcelUtils.setCellData("Pass", i,15 ,Constant.Path_TestData + Constant.File_ManageInvoicing);

		}		 


	}


	@Test

	public void  manageinvoice_emailconfirmation() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("manageinvoice email confirmation");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			//filter

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[11]/a[3]/span")).click(); 
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();

			Thread.sleep(2000);

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}
	}

	@Test
	public void manageinvoice_paidstatusimage() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("manageinvoice_paid status image");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");



			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");
			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[5]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);



			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));


			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.id("mapa_payment_amount")).sendKeys(strng);
			driver.findElement(By.id("mapa_payment_note")).sendKeys("amount has been paid ");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);


			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(3000);

			// manually paid

			WebElement manually_paid_element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select manually_paid= new Select(manually_paid_element);
			manually_paid.selectByVisibleText("manually paid");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[1]/span/span")).click();



			// driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[11]/a[2]/span/span")).click();
			Thread.sleep(3000);
			WebElement ImageFile = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div[3]/div[5]/div/div[2]/img"));

			Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
			if (ImagePresent)
			{
				System.out.println("Image displayed.");
			}
			else
			{
				System.out.println("Image not displayed.");
			}


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[3]/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			


		}

	}
	@Test
	public void manageinvoice_edit_splitpayment() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("manageinvoice edit split payment");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");

			
			String splitpercentage = ExcelUtils.getCellData(i,16);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("viewed");
			Thread.sleep(2000);

			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("no");
			Thread.sleep(2000);
			
			//Click on edit button
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[2]/span")).click();
			//click on split payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/div/div/div/input")).click();
			
			WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div[2]/div[1]/div/div/div/select")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByIndex(1);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/input")).sendKeys(splitpercentage);
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			


		}
	}

	
	@Test
	public  void manageinvoice_sessiontimeout() throws Exception 
	{	

		System.out.println("manage invoice session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			
			driver.findElement(By.xpath(Xpath.manageinvoices)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
			
			

}
	}

}








