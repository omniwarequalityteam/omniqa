package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;


import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class Managecustomer {

	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element= null;
	private static String CustomerMobile;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
	//	driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}


	@Test
	public void Managecustomer_filters() throws Exception
	{

		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_Filters Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");
			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  

			//Define implict Wait           
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
			//customer ID
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[1]"));
			String strng= element.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);	 

			//Name
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element1 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[2]/div"));
			String strng1= element1.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//email Id
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element2 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[3]/div"));
			String strng2= element2.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(strng2);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//phone 
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element3 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[4]/div"));
			String strng3= element3.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(strng3);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//address1
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element4 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[5]"));
			String strng4= element4.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/input")).sendKeys(strng4);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//address2
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element5 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]"));
			String strng5= element5.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/input")).sendKeys(strng5);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//city 
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element6 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[7]"));
			String strng6= element6.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/input")).sendKeys(strng6);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//state
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element7 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[8]"));
			String strng7= element7.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[8]/input")).sendKeys(strng7);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[8]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//country 
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element8 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[9]"));
			String strng8= element8.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/input")).sendKeys(strng8);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//zip code
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element9 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[10]"));
			String strng9= element9.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[10]/input")).sendKeys(strng9);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[10]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//REference ID
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element10 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[11]"));
			String strng10= element10.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[11]/input")).sendKeys(strng10);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[11]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Note
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/button")).click();
			WebElement element11 =driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[12]"));
			String strng11= element11.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[12]/input")).sendKeys(strng11);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[12]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

		}
	}


	@Test
	public void Managecustomer_add() throws Exception
	{

		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_add Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");

			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();

			String CustomerName = ExcelUtils.getCellData(i,2);
			String CustomerEmail = ExcelUtils.getCellData(i,3);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,4)+formattedDate;


			String CustomerAddressLine1 = ExcelUtils.getCellData(i,5);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,6);
			String CustomerCity = ExcelUtils.getCellData(i,7);

			String CustomerState = ExcelUtils.getCellData(i,8);
			String Customercountry = ExcelUtils.getCellData(i,9);
			String CustomerZipCode = ExcelUtils.getCellData(i,10);
			String ReferenceID = ExcelUtils.getCellData(i,11);
			String Note = ExcelUtils.getCellData(i,12);

			// driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.name("bcus_zipcode")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(6000);	    
		}
	}



	@Test
	public void Managecustomer_Edit() throws Exception
	{
		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_Edit Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");
			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  

			//Edit
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[13]/a[1]")).click();

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


			String CustomerAddressLine1 = ExcelUtils.getCellData(i,13);
			driver.findElement(By.name("bcus_address_line_1")).clear();
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			Thread.sleep(4000);

			String CustomerMobile = ExcelUtils.getCellData(i,14);
			driver.findElement(By.name("bcus_state")).clear();
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerMobile);
			Thread.sleep(4000);

			//update
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(6000);	    
		}
	}

	@Test
	public void Managecustomer_Delete() throws Exception
	{
		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_Delete Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");


			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  
			Thread.sleep(4000);
			//Delete
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[13]/a[2]/span")).click();
			Thread.sleep(3000);	 
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
			Thread.sleep(3000);	    
		}

	}


	@Test
	public void Managecustomer_mandatoryfields() throws Exception
	{

		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_mandatoryfields Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");
			driver.findElement(By.xpath(Xpath.Managecustomer)).click();

			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  

			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();

			String CustomerName = ExcelUtils.getCellData(i,2);
			String CustomerEmail = ExcelUtils.getCellData(i,3);

			String CustomerMobile = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine1 = ExcelUtils.getCellData(i,5);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,6);
			String CustomerCity = ExcelUtils.getCellData(i,7);

			String CustomerState = ExcelUtils.getCellData(i,8);
			String Customercountry = ExcelUtils.getCellData(i,9);
			String CustomerZipCode = ExcelUtils.getCellData(i,10);

			//add
			// driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();


			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			// System.out.println("Design name: " +Designname);
			// System.out.println("design name created successfully");
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			// driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);

			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.name("bcus_zipcode")).sendKeys(CustomerZipCode);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(6000);	       
		}
	}

	@Test

	public  void Managecustomer_previous_next() throws Exception
	{
		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_previous_next Start");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");

			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  

			/*previous and next*/
			//2 
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[3]/div/ul/li[2]/a")).click();
			Thread.sleep(3000);
			// 3
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[3]/div/ul/li[2]/a")).click();
			Thread.sleep(3000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next                        
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);

			//previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);
		}
	}

	@Test
	public void Managecustomer_show_entries() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("-----------------------------------");
		System.out.println("Managecustomer_show_entries");
		System.out.println("-----------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managecustomer,"Sheet1");

			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			String expectedTitle = "TraknPay Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Customers");  

			/* dropdown */
			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("10");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect2= new Select(mySelectElm);
			mySelect.selectByVisibleText("25");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect3= new Select(mySelectElm);
			mySelect.selectByVisibleText("50");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect4= new Select(mySelectElm);
			mySelect.selectByVisibleText("100");
			Thread.sleep(9000);
		}
	}

	@Test
	public  void Managecustomer_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("Managecustomer session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.Managecustomer)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}