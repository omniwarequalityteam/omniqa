package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class Settlement_Details {
	
	public static WebDriver	driver = new FirefoxDriver();
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	
	
	@Test 
	public  void Settlement_Details_Filters () throws Exception
 {	
	
	driver.get(Constant.URL);
	System.out.println("Settlement_Details_Filters Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
	 driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	             
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");
			
			//Define implict Wait           
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	 
	  
	  //Settlement ID.
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
	  String strng= element.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);	 

	  //Settlement_Details_ID.
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element1 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
	  String strng1= element1.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);	 

	  //Invoice_No 
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element2 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[3]"));
	  String strng2= element2.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng2);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);	 

	  //Payer_Name 
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element3 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
	  String strng3= element3.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng3);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);	 

	  //Payment_Date
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[4]")).click();
	  Thread.sleep(3000);
	  
	//Transaction ID
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element4 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[6]"));
	  String strng4= element4.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng4);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Invoice total
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element5 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[7]"));
	  String strng5= element5.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(strng5);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Convenience Fee model
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element6 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[8]"));
	  String strng6= element6.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng6);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	
	//Amount paid by payer
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element7 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[9]"));
	  String strng7= element7.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(strng7);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Trak n' Pay charges
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element8 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[10]"));
	  String strng8= element8.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(strng8);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Trak n' Pay Service Tax
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement element9 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[11]"));
	  String strng9= element9.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(strng9);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Receiver name
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements1 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[12]"));
	  String strngs1= elements1.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/input")).sendKeys(strngs1);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[12]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  	  
	//Receiver split share
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements2 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[13]"));
	  String strngs2= elements2.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[13]/input")).sendKeys(strngs2);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[13]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Amount reimbursed to receiver
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements3 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[14]"));
	  String strngs3= elements3.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[14]/input")).sendKeys(strngs3);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[14]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Settlement Status
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements4 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[15]"));
	  String strngs4= elements4.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[15]/input")).sendKeys(strngs4);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[15]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Bank settlement date
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[16]/input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[4]")).click();
	  Thread.sleep(3000);
	  
	//Bank settlement amount
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements6 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[17]"));
	  String strngs6= elements6.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(strngs6);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	
	//Bank settlement amount
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements7 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[17]"));
	  String strngs7= elements7.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(strngs7);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[17]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Bank reference number
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements8 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[18]"));
	  String strngs8= elements8.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[18]/input")).sendKeys(strngs8);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[18]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Name of bank settled to
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements9 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[19]"));
	  String strngs9= elements9.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[19]/input")).sendKeys(strngs9);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[19]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Payment type
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements10 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[20]"));
	  String strngs10= elements10.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[20]/input")).sendKeys(strngs10);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[20]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	 
	//Payer mobile number
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement elements11 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[21]"));
	  String strngs11= elements11.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[21]/input")).sendKeys(strngs11);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[21]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//dropdown 
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("yes");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	  
		 
	  WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[22]/select")); 
	  Select mySelect2= new Select(mySelectElm);
	  mySelect.selectByVisibleText("no");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[23]/button")).click();
	 	}
 	}
	
	
	@Test 
	public  void Settlement_Details_show_entries() throws Exception
 {	
	
	driver.get(Constant.URL);
	System.out.println("-------------------------------------");
	System.out.println("Settlement_Details_show_entries Start");
	System.out.println("-------------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
      driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

		 /* dropdown */
			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("10");
			Thread.sleep(9000);

	/* dropdown */
			WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect2= new Select(mySelectElm);
			mySelect.selectByVisibleText("25");
			Thread.sleep(9000);

	/* dropdown */
			WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect3= new Select(mySelectElm);
			mySelect.selectByVisibleText("50");
			Thread.sleep(9000);

	/* dropdown */
			WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect4= new Select(mySelectElm);
			mySelect.selectByVisibleText("100");
			Thread.sleep(9000);
							}
 			}
 
	@Test 
	public  void Settlement_Details_previous_next() throws Exception
 {	
	
	driver.get(Constant.URL);
	System.out.println("--------------------------------");
	System.out.println("Settlement_Details_previous_next Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
      driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

		 /*previous and next*/
		    //2 
		   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
		   Thread.sleep(5000);
		// 3
		 //  driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[4]/a")).click();
		 //  Thread.sleep(3000);
		 //previous
		   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
		   Thread.sleep(4000);
		 //previous
		   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
		   Thread.sleep(4000);
		 //next
		   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
		   Thread.sleep(4000);
		 //next
		   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
		   Thread.sleep(4000);
		   	 			}
 		}

	@Test 
	public  void Settlement_Details_Change_Status_NotApptoved() throws Exception
 {	
	
	driver.get(Constant.URL);
	System.out.println("--------------------------------");
	System.out.println("Settlement_Details_Change_Status_NotApptoved Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
      driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

		 //click on Approved
		 driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[22]/a")).click();
		 Thread.sleep(4000);
		 //click on conform button
		 driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
		 Thread.sleep(4000);
		 //click on okay 
		 driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
		 Thread.sleep(4000);
					 			}
		 		}
	 

	@Test 
	public  void Settlement_Details_Change_Status_Apptoved() throws Exception
 {	
	 
 	 driver.get(Constant.URL);
	 System.out.println("--------------------------------");
	 System.out.println("Settlement_Details_Change_Status_Apptoved Start");
	 System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
      driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

		 //click on Approved
		 driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[22]/a")).click();
		 Thread.sleep(4000);
		 //click on conform button
		 driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
		 Thread.sleep(4000);
		 //click on okay 
		 driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
		 Thread.sleep(4000);
		
			 			}
		 		}
	
	@Test 
	public  void Settlement_Details_toggle() throws Exception
 {	
	 driver.get(Constant.URL);
	 System.out.println("--------------------------------");
	 System.out.println("Settlement_Details_toggle  Start");
	 System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_Details,"Sheet1");
      driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
	    	
     String expectedTitle = "TraknPay Settlement History";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay Settlement History page");

		 //click on toggle
		 driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/a[4]/i")).click();
		 Thread.sleep(4000);
		 
		//settlement details id 
		 driver.findElement(By.xpath("html/body/div[5]/a[1]/span")).click();
		 Thread.sleep(4000);
		 
		 //Invoice No
		 driver.findElement(By.xpath("html/body/div[5]/a[2]/span")).click();
		 Thread.sleep(4000);
		 
		 // payer name
		 driver.findElement(By.xpath("html/body/div[5]/a[3]/span")).click();
		 Thread.sleep(4000);
		 
		 // payment date
		 driver.findElement(By.xpath("html/body/div[5]/a[4]/span")).click();
		 Thread.sleep(4000);
		 
		 // transaction id
		 driver.findElement(By.xpath("html/body/div[5]/a[5]/span")).click();
		 Thread.sleep(4000);
		 
		 //invoice total
		 driver.findElement(By.xpath("html/body/div[5]/a[6]/span")).click();
		 Thread.sleep(4000);
		 
		 //convenience fee mode 
		 driver.findElement(By.xpath("html/body/div[5]/a[7]/span")).click();
		 Thread.sleep(4000);
		 
		 //Amount paid by payer
		 driver.findElement(By.xpath("html/body/div[5]/a[8]/span")).click();
		 Thread.sleep(4000);
		 
		 //traknpay charges
		 driver.findElement(By.xpath("html/body/div[5]/a[9]/span")).click();
		 Thread.sleep(4000);
		 
		 //
		 driver.findElement(By.xpath("html/body/div[5]/a[10]/span")).click();
		 Thread.sleep(4000);
		 
		 //Traknpay service tax
		 driver.findElement(By.xpath("html/body/div[5]/a[11]/span")).click();
		 Thread.sleep(4000);
		 
		 //Receiver name
		 driver.findElement(By.xpath("html/body/div[5]/a[12]/span")).click();
		 Thread.sleep(4000);
		
		 //Receiver split share
		 driver.findElement(By.xpath("html/body/div[5]/a[13]/span")).click();
		 Thread.sleep(4000);
		 
		 //Amount reimbursed 
		 driver.findElement(By.xpath("html/body/div[5]/a[14]/span")).click();
		 Thread.sleep(4000);
		 
		 //settlement status
		 driver.findElement(By.xpath("html/body/div[5]/a[15]/span")).click();
		 Thread.sleep(4000);
		 
		 //bank settlement date
		 driver.findElement(By.xpath("html/body/div[5]/a[16]/span")).click();
		 Thread.sleep(4000);
		 
		//bank settlement amount
		 driver.findElement(By.xpath("html/body/div[5]/a[17]/span")).click();
		 Thread.sleep(4000);
		 
		//bank reference number
		 driver.findElement(By.xpath("html/body/div[5]/a[18]/span")).click();
		 Thread.sleep(4000);
		 
		//name of the banksettled
		 driver.findElement(By.xpath("html/body/div[5]/a[19]/span")).click();
		 Thread.sleep(4000);
		 
		//payment type
		 driver.findElement(By.xpath("html/body/div[5]/a[20]/span")).click();
		 Thread.sleep(4000);
		 
		//payer mobile number
		 driver.findElement(By.xpath("html/body/div[5]/a[21]/span")).click();
		 Thread.sleep(4000);
		 
		//Approved?
		 driver.findElement(By.xpath("html/body/div[5]/a[22]/span")).click();
		 Thread.sleep(5000);
		 /*
			 
		 //click on side
		 driver.findElement(By.xpath("html/body/div[3]/a[1]/span")).click();
		 Thread.sleep(13000);
		
		 //click on toggle
		 driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/a[4]/i")).click();
		 Thread.sleep(6000);
		 	*/	 
		//settlement details id 
		 driver.findElement(By.xpath(" html/body/div[3]/a[2]/span")).click();
		 Thread.sleep(4000);
		 
		
		
			 			}
		 		}
			
	@Test
	public  void Settlement_Details_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("Settlement_Details session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			
			driver.findElement(By.xpath(Xpath.Settlement_Details)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
	}