package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;


public class NACH_Page {
	public static WebDriver driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String Name;
	private static String scheduleName;
	private static String designName;
	private static String CustomerName;

	@BeforeTest
	public void beforeTest() 
	{
		//driver = new FirefoxDriver();
		driver.get(Constant.URL1);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}
	@Test
	public void nachmandates_createnewNACH() throws Exception
	{

		System.out.println("Create new nach button ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(customername);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);


		}

	}


	@Test
	public void nachmandates_customernamemandatory() throws Exception
	{

		System.out.println("Customer name mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Customer Name field is required."))

			{
				System.out.println("pass:The Customer Name field is required.");

			}
			else
			{

				System.out.println("pass:The Customer Name field is required.");

			}

		}


	}

	@Test
	public void nachmandates_accounttypenamemandatory() throws Exception
	{

		System.out.println("account type mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			/*WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
		Select mySelect1211= new Select(mySelectElm1211);
		mySelect1211.selectByVisibleText("SB"); */

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/p")).getText().equalsIgnoreCase("The Account Type field is required."))

			{
				System.out.println("pass:The Account Type field is required.");

			}
			else
			{

				System.out.println("fail:The Account Type field is required.");
			}

		}

	}

	@Test
	public void nachmandates_accountnumbermandatory() throws Exception
	{

		System.out.println("account number mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			//String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/p")).getText().equalsIgnoreCase("The Account Number field is required."))

			{
				System.out.println("pass:The Account Number field is required.");

			}
			else
			{
				System.out.println("fail:The Account Number field is required.");


			}

		}


	}

	@Test
	public void nachmandates_banknamemandatory() throws Exception
	{

		System.out.println("bank name mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			//String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/p")).getText().equalsIgnoreCase("The Bank Name field is required."))

			{
				System.out.println("pass:The Bank Name field is required.");


			}
			else
			{


				System.out.println("fail:The Bank Name field is required.");


			}
		}

	}


	@Test
	public void nachmandates_bankifsccodemandatory() throws Exception
	{

		System.out.println("bank ifsc code mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			//String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/p")).getText().equalsIgnoreCase("The Bank IFSC Code field is required when Bank MICR is not present."))

			{
				System.out.println("pass:The Bank IFSC Code field is required when Bank MICR is not present.");

			}
			else
			{
				System.out.println("fail:The Bank IFSC Code field is required when Bank MICR is not present.");


			}

		}

	}

	@Test
	public void nachmandates_amountmandatory() throws Exception
	{

		System.out.println("Amount field mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Amount field is required."))

			{
				System.out.println("pass:The Amount field is required.");

			}
			else
			{
				System.out.println("fail:The Amount field is required.");


			}

		}


	}

	@Test
	public void nachmandates_frequencymandatory() throws Exception
	{

		System.out.println("frequency mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			/*WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
		Select mySelect12111= new Select(mySelectElm12111);
		mySelect12111.selectByVisibleText("Mthly");*/
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/p")).getText().equalsIgnoreCase("The Frequency field is required."))

			{
				System.out.println("pass:The Frequency field is required.");


			}
			else
			{

				System.out.println("fail:The Frequency field is required.");


			}

		}

	}
	@Test
	public void nachmandates_debittypemandatory() throws Exception
	{

		System.out.println("Debit mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			/*WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
		Select mySelect121111= new Select(mySelectElm121111);
		mySelect121111.selectByVisibleText("Fixed Amount"); */

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/p")).getText().equalsIgnoreCase("The Debit Type field is required."))

			{
				System.out.println("pass:The Frequency field is required.");

			}
			else
			{
				System.out.println("fail:The Frequency field is required.");

			}

		}	

	}
	@Test
	public void nachmandates_startdatemandatory() throws Exception
	{

		System.out.println("start date mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(5000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;
			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type

			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount"); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).clear();

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/p")).getText().equalsIgnoreCase("The Start Date field is required."))

			{
				System.out.println("pass:The Start Date field is required.");

			}
			else
			{

				System.out.println("fail:The Start Date field is required.");



			}

		}

	}
	@Test
	public void nachmandates_IFSCinvalid() throws Exception
	{

		System.out.println("Create new nach button ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String Invalidbankifsccode = ExcelUtils.getCellData(i,19);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(Invalidbankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/p")).getText().equalsIgnoreCase("The Bank IFSC Code format is invalid."))

			{
				System.out.println("pass:The Bank IFSC Code format is invalid.");

			}
			else
			{
				System.out.println("fail:The Bank IFSC Code format is invalid.");

			}
		}

	}
	@Test
	public void nachmandates_Invalidaccountnumber() throws Exception
	{

		System.out.println("Create new nach button ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String Invalidaccountnumber = ExcelUtils.getCellData(i,21);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(Invalidaccountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();



			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/p")).getText().equalsIgnoreCase("The Account Number must be a number."))

			{
				System.out.println("pass:The Account Number must be a number.");

			}
			else
			{

				System.out.println("fail:The Account Number must be a number.");

			}

		}

	}


	@Test
	public void nachmandates_Invalidamount() throws Exception
	{

		System.out.println("Invalid amount field ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String Invalidamount = ExcelUtils.getCellData(i,23);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(Invalidamount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Amount must be at least 2."))

			{
				System.out.println("pass:The Amount must be at least 2.");

			}
			else
			{

				System.out.println("fail:The Amount must be at least 2.");


			}

		}
	}



	@Test
	public void nachmandates_Invalidstartdate() throws Exception
	{

		System.out.println("Invalid start date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String Invalidstartdate= ExcelUtils.getCellData(i,25);
			String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(Invalidstartdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/p")).getText().equalsIgnoreCase("The Start Date must be a date after yesterday."))

			{
				System.out.println("pass:start date must be day after yesterday");


			}
			else
			{

				System.out.println("fail:start date must be day after yesterday");


			}

		}

	}

	@Test
	public void nachmandates_Edit() throws Exception
	{

		System.out.println("Edit amount field in nach");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String Invalidstartdate= ExcelUtils.getCellData(i,25);
			String enddate= ExcelUtils.getCellData(i,8);
			String Editamount= ExcelUtils.getCellData(i,27);

			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(customername);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);


			//Filters clear
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();

			//name filter search
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(customername);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);
			//Edit button
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[16]/a[1]/span")).click();
			expectedTitle = "TraknPay Edit NACH Form";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(Editamount);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

		}
	}
	@Test
	public void nachmandates_Modifyedit() throws Exception
	{

		System.out.println("Edit Account type in nach Modify");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm17 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect17= new Select(mySelectElm17);
			mySelect17.selectByVisibleText("MODIFY");
			Thread.sleep(2000);
			//Edit Modify nach
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[16]/a[1]/span")).click();
			//account type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("CA");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

		}
	}
	@Test
	public void nachmandates_withoutUMRN() throws Exception
	{

		System.out.println("Modify UMRN with empty number");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm17 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect17= new Select(mySelectElm17);
			mySelect17.selectByVisibleText("MODIFY");
			Thread.sleep(2000);

			//Edit Modify nach
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[16]/a[1]/span")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The UMRN field is required when namn action type is MODIFY."))

			{
				System.out.println("Pass:The UMRN field is required when namn action type is MODIFY.");

			}
			else
			{

				System.out.println("Fail:The UMRN field is required when namn action type is MODIFY.");

			}
		}
	}

	@Test
	public void nachmandates_withoutenddate() throws Exception
	{

		System.out.println("Create new with out giving end date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			//String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(7000);
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/p")).getText().equalsIgnoreCase("The End Date field is required when Until Cancelled is not present."))

			{
				System.out.println("Pass:The End Date field is required when Until Cancelled is not present..");

			}
			else
			{

				System.out.println("Fail:The End Date field is required when Until Cancelled is not present.");

			}
		}

	}
	@Test
	public void nachmandates_untilcancelledbuttonclick() throws Exception
	{

		System.out.println("click on until cancelled button");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			//String enddate= ExcelUtils.getCellData(i,8);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(enddate);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[16]/div/input")).click();


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay NACH";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}
	}

	@Test
	public void nachmandates_samestartdateandenddate() throws Exception
	{

		System.out.println("same start date and end date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay NACH Mandate";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			CustomerName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String customername = CustomerName;

			String accountnumber = ExcelUtils.getCellData(i,2);
			String bankname = ExcelUtils.getCellData(i,3);
			String bankifsccode = ExcelUtils.getCellData(i,4);
			String amount = ExcelUtils.getCellData(i,5);
			String reference1 = ExcelUtils.getCellData(i,6);
			String startdate= ExcelUtils.getCellData(i,7);
			String samestartandenddate= ExcelUtils.getCellData(i,28);

			//Action type drop down
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("CREATE");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(customername);
			//action type
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("SB");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(accountnumber);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(bankname);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(bankifsccode);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(amount);
			//Frequency
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[10]/div/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Mthly");
			//Debit type
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[11]/div/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("Fixed Amount");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[12]/div/input")).sendKeys(reference1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[14]/div/input")).sendKeys(startdate);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(samestartandenddate);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/input")).sendKeys(Keys.RETURN);

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[15]/div/p")).getText().equalsIgnoreCase("The End Date must be a date after Start Date."))

			{
				System.out.println("Pass:The End Date must be a date after Start Date.");

			}
			else
			{

				System.out.println("Fail:The End Date must be a date after Start Date.");

			}
		}
	}
	@Test
	public void nachmandates_filters() throws Exception
	{

		System.out.println("filter fields");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			//Request ID
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Status
			//All Status
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm20 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect20= new Select(mySelectElm20);
			mySelect20.selectByVisibleText("All Status");
			Thread.sleep(2000);
			
			//requested
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("requested");
			Thread.sleep(2000);
			
			//approved
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm12 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect12= new Select(mySelectElm12);
			mySelect12.selectByVisibleText("approved");
			Thread.sleep(2000);
			
			//confirmed
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm13 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect13= new Select(mySelectElm13);
			mySelect13.selectByVisibleText("confirmed");
			Thread.sleep(2000);
			
			//processing
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm14 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect14= new Select(mySelectElm14);
			mySelect14.selectByVisibleText("processing");
			Thread.sleep(2000);
			
			//rejected
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm15 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect15= new Select(mySelectElm15);
			mySelect15.selectByVisibleText("rejected");
			Thread.sleep(2000);
			

			//Type
			//All Type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm19 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect19= new Select(mySelectElm19);
			mySelect19.selectByVisibleText("All Type");
			Thread.sleep(2000);
			
			
			//CREATE
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm16 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect16= new Select(mySelectElm16);
			mySelect16.selectByVisibleText("CREATE");
			Thread.sleep(2000);
			
			//MODIFY
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm17 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect17= new Select(mySelectElm17);
			mySelect17.selectByVisibleText("MODIFY");
			Thread.sleep(2000);
			
			//CANCEL
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm18 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/select")); 
			Select mySelect18= new Select(mySelectElm18);
			mySelect16.selectByVisibleText("CANCEL");
			Thread.sleep(2000);
			
	
			//Mandate date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[3]")).click();
			Thread.sleep(2000);

			//customer name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element3=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]/div"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//account type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm21 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect21= new Select(mySelectElm21);
			mySelect21.selectByVisibleText("SB");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm22 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect22= new Select(mySelectElm22);
			mySelect22.selectByVisibleText("CA");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm23 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect23= new Select(mySelectElm23);
			mySelect23.selectByVisibleText("CC");
			Thread.sleep(2000);

			//Account number
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element4=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]/div"));
			String strng4 = element4.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Bank name

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element5=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/div"));
			String strng5 = element5.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(strng5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//IFSC code
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element6=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[10]/div"));
			String strng6 = element6.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(strng6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//amount 
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement element7=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[11]"));
			String strng7 = element7.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(strng7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Frequency
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm24 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/select")); 
			Select mySelect24= new Select(mySelectElm24);
			mySelect24.selectByVisibleText("Mthly");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("Qtly");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("H-Yrly");
			Thread.sleep(2000);

			//Debit type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm123 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/select")); 
			Select mySelect123= new Select(mySelectElm123);
			mySelect123.selectByVisibleText("Fixed Amount");
			Thread.sleep(2000);

			//Debit type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm124 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/select")); 
			Select mySelect124= new Select(mySelectElm124);
			mySelect124.selectByVisibleText("Maximum Amount");
			Thread.sleep(2000);

		}

	}
	@Test
	public void nachpullrequest_pullamount() throws Exception
	{

		System.out.println("pull amount which you need to pay ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			String pullamount= ExcelUtils.getCellData(i,29);


			//Status
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm23 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/select")); 
			Select mySelect23= new Select(mySelectElm23);
			mySelect23.selectByVisibleText("approved");
			Thread.sleep(2000);

			//Debit type
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm24 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/select")); 
			Select mySelect24= new Select(mySelectElm24);
			mySelect24.selectByVisibleText("Maximum Amount");
			Thread.sleep(2000);

			//Frequency
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm25 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/select")); 
			Select mySelect25= new Select(mySelectElm25);
			mySelect25.selectByVisibleText("As & when presented");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[16]/a[3]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/fieldset/input")).sendKeys(pullamount);
			
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();

			driver.findElement(By.xpath(Xpath.NACHPullRequests)).click();

			expectedTitle = "TraknPay NACH Pull Request ";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(pullamount);

		}

	}
	@Test
	public void pullrequest_pullamountmorethanmaxamount() throws Exception
	{

		System.out.println("pulled amount is more than maximum amount  ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHMandates)).click();

			String expectedTitle = "TraknPay NACH";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Nach,"Sheet1");
			String pullamountmorethanmaxamount= ExcelUtils.getCellData(i,30);



			//Debit type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm123 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[13]/select")); 
			Select mySelect123= new Select(mySelectElm123);
			mySelect123.selectByVisibleText("Maximum Amount");
			Thread.sleep(2000);

			//Frequency
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[16]/button")).click();
			WebElement mySelectElm1232 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/select")); 
			Select mySelect1232= new Select(mySelectElm1232);
			mySelect1232.selectByVisibleText("As & when presented");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[16]/a[3]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/fieldset/input")).sendKeys(pullamountmorethanmaxamount);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(2000);
			if(driver.findElement(By.xpath("html/body/div[6]/p")).getText().equalsIgnoreCase("Entered amount should not be greater than the maximum amount"))

			{
				System.out.println("Pass:Entered amount should not be greater than the maximum amount");
				driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();

			}
			else
			{

				System.out.println("Fail:Entered amount should not be greater than the maximum amount");

			}
		}
	}

	@Test
	public void Nachpullrequest_filters() throws Exception
	{

		System.out.println("nach pull request filters ");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.NACHPullRequests)).click();

			//Pull Request ID
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Nach request id
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//umrn
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement element2=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/div"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng2);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement element3=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Frequency
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("As & when presented");
			Thread.sleep(2000);

			//Debit type
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Maximum Amount");
			Thread.sleep(2000);

			//Requested date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[5]")).click();
			Thread.sleep(2000);
			
			//Status
			//All Status
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm20 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect20= new Select(mySelectElm20);
			mySelect20.selectByVisibleText("All Status");
			Thread.sleep(2000);
			
			//requested
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm21 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect21= new Select(mySelectElm21);
			mySelect21.selectByVisibleText("requested");
			Thread.sleep(2000);
			
			
			//approved
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm22 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect22= new Select(mySelectElm22);
			mySelect22.selectByVisibleText("approved");
			Thread.sleep(2000);
			
			//rejected
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
			WebElement mySelectElm23 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select")); 
			Select mySelect23= new Select(mySelectElm23);
			mySelect23.selectByVisibleText("rejected");
			Thread.sleep(2000);
			
		}
	}

	@Test
	public  void nachmandates_sessiontimeout() throws Exception 
	{	

		System.out.println("nach mandates page session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			
			driver.findElement(By.xpath(Xpath.NACHMandates)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 
			

}
	}



}
