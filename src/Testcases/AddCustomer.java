package Testcases;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class AddCustomer {

	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String CustomerMobile;
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	@Test
	public void AddCustomer_allfileds() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("AddCustomer_all fileds Start");
		System.out.println("-----------------------------");
		for(int i=1; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);

			String sPassword = ExcelUtils.getCellData(i,2);
			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);
			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);


			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			Thread.sleep(5000);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);

			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);

			driver.findElement(By.name("bcus_zipcode")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
		} 
	}


	@Test
	public void AddCustomer_mandatoryfiled_CustomerZip_minmum() throws Exception
	{
		System.out.println("-----------------------------------------------------");
		System.out.println("AddCustomer_mandatory_CustomerZip_minmum fields Start");
		System.out.println("-----------------------------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,12);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Customer Zip Code must be 6 digits."))
			{                            
				System.out.println("pass :The Customer Zip Code must be 6 digits.");
			}
			else
			{
				System.out.println("Fail:The Design Name has already been taken.");
			}
			}
		}



	@Test
	// phone number already exists
	public void AddCustomer_phonenumber_exists() throws Exception
	{
		System.out.println("AddCustomer_phonenumber_exists Start");
		System.out.println("--------------------------------------");
		for(int i=1; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			String CustomerMobile = ExcelUtils.getCellData(i,11);
			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);


			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);

			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Customer Phone has already been taken."))
			{                            
				System.out.println("pass :The Customer Phone has already been taken.");
			}
			else
			{
				System.out.println("Fail:The Customer Phone has already been taken.");
			}
			}
		}


	@Test
	public void AddCustomer_add_vendor() throws Exception
	{
		System.out.println("AddCustomer_all fileds Start");
		System.out.println("----------------------");
		for(int i=1; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);

			String sPassword = ExcelUtils.getCellData(i,2);
			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);


			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);

			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.name("bcus_zipcode")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			/* dropdown */
			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bcus_bven_id']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("shreeja");
			Thread.sleep(9000);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

		}
	}


	@Test
	public void AddCustomer_Back() throws Exception
	{

		System.out.println("AddCustomer_all fileds Start");
		System.out.println("------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);

			String sPassword = ExcelUtils.getCellData(i,2);
			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/a")).click();
			Thread.sleep(4000);
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_customerName() throws Exception
	{
		System.out.println("AddCustomer_mandatory_customerName fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			//String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;
			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);
			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Customer Name field is required."))
			{                            
				System.out.println("pass:The Customer Name field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer Name field is required.");
			}
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_Email() throws Exception
	{
		System.out.println("AddCustomer_mandatory_Email fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			//String CustomerEmail = ExcelUtils.getCellData(i,2);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;
			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);
			String CustomerState = ExcelUtils.getCellData(i,7);

			String Customercountry = ExcelUtils.getCellData(i,8);
			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);    
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Customer Email field is required."))
			{                            
				System.out.println("Pass:The Customer Email field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer Email field is required.");
				}
			}
		}

	@Test
	public void AddCustomer_mandatoryfileds_phone() throws Exception
	{
		System.out.println("-----------------------------------");
		System.out.println("AddCustomer_mandatory_phone Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);
			/*
		 Date date = new Date();
	     SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
		    String formattedDate = sdf.format(date);
		String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;
			 */     
			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			//  driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);

			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Customer Phone field is required."))
			{                            
				System.out.println("Pass:The Customer Phone field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer Phone field is required.");
			}
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_Adderssline1() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("AddCustomer_mandatory_Adderss_line_1 fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			//	String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);


			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);

			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			//driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);

			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[4]/div/p")).getText().equalsIgnoreCase("The Customer Adderss line 1 field is required."))
			{                            
				System.out.println("Pass:The Customer Adderss line 1 field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer Address line 1 field is required.");
			}
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_city() throws Exception
	{
		System.out.println("---------------------------------------");
		System.out.println("AddCustomer_mandatory_city fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			//String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);

			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			//driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);

			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[6]/div/p")).getText().equalsIgnoreCase("The Customer city field is required."))
			{                            
				System.out.println(" Pass :The Customer city field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer city field is required.");
			}
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_state() throws Exception
	{
		System.out.println("----------------------------------");
		System.out.println("AddCustomer_mandatory_state fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			//	String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			// System.out.println("Design name: " +Designname);
			// System.out.println("design name created successfully");

			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			//driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);

			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note); 
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[7]/div/p")).getText().equalsIgnoreCase("The Customer state field is required."))
			{                            
				System.out.println(" Error:The Customer state field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer state field is required.");
			}
		}
	}


	@Test
	public void AddCustomer_mandatoryfileds_country() throws Exception
	{

		System.out.println("--------------------------------");
		System.out.println("AddCustomer_mandatory_country fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();

			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			//String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);

			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);

			//driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);

			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[8]/div/p")).getText().equalsIgnoreCase("The Customer Email field is required."))
			{                            
				System.out.println("Pass:The Customer country field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer country field is required.");
			}
		}
	}	

	@Test
	public void AddCustomer_mandatoryfileds_zip_code() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("AddCustomer_mandatory_zip_code fields Start");
		System.out.println("-----------------------------------");
		for(int i=1; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			String result = ExcelUtils.getCellData(i,3);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1,"Sheet1");
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();

			String expectedTitle = "TraknPay Business Customers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");

			String CustomerName = ExcelUtils.getCellData(i,1);
			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMmmss");
			String formattedDate = sdf.format(date);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);
			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);
			//String CustomerZipCode = ExcelUtils.getCellData(i,9);
			String ReferenceID = ExcelUtils.getCellData(i,13);
			String Note = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("bcus_name")).sendKeys(CustomerName);
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);

			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);
			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);

			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);
			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);
			//driver.findElement(By.xpath(".//*[@id='bcus_zipcode']")).sendKeys(CustomerZipCode);
			driver.findElement(By.name("bcus_reference_id")).sendKeys(ReferenceID);
			driver.findElement(By.name("bcus_note")).sendKeys(Note);
			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Customer Zip Code field is required."))
			{                            
				System.out.println("pass: The Customer Zip Code field is required.");
			}
			else
			{
				System.out.println("Fail:The Customer Zip Code field is required.");
			}
		}
	}

	@Test
	public  void AddCustomer_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("AddCustomer session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.Addcustomer)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}

