package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class Profile_new {
	
	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element = null;
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
	 //  driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
			
	@Test 
	public  void Profile_Business_Details () throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_Business_Details");
	  System.out.println("--------------------------------");
		  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
		  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click(); 
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(5000);
	
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,4); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	     }
  
		 }
	


	@Test 
	public  void Profile_mandatoryfiled_Business_Name () throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_Business_Name");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
		   
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(3000);
	
		  
	 // String BusinessName = ExcelUtils.getCellData(i,1);
	  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).sendKeys(BusinessName);
	  
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,4); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	
	   String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).getAttribute("data-content");
	   System.out.println("Error:"+a);    
	 
		 }
	 }

	
	@Test 
	public  void Profile_mandatoryfiled_company_Address () throws Exception
	 {	
	  
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_company_Address");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();

	  //clear filters
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(4000);
		 
	 // String Address = ExcelUtils.getCellData(i,1);
	  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,4); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	   
	   String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).getAttribute("data-content");
	   System.out.println("Error:"+a);
		 }
	 }
	  
	
	@Test 
	public  void Profile_mandatoryfiled_City() throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_City");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
		  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");

		  System.out.println("loaded: " + i);
		  String sUserName = ExcelUtils.getCellData(i, 1);
		  String sPassword = ExcelUtils.getCellData(i, 2);
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
		  
		  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
		  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	 
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();
	  //clear filters
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(4000);
	  
		  
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	 // String City = ExcelUtils.getCellData(i,2);
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,4); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	    
	   String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).getAttribute("data-content");
	   System.out.println("Error:"+a);
	 		 }
	 }
	  
	
	@Test 
	public  void Profile_mandatoryfiled_State() throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_State");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");

	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();

	  //clear filters
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(4000);
	  
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	 // String State = ExcelUtils.getCellData(i,3); 
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,4); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	  
	  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).getAttribute("data-content");
	  System.out.println("Error:"+a);
		 }
	 }


	@Test 
	public  void Profile_mandatoryfiled_ZipCode() throws Exception
	 {	
	 
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_ZipCode");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");

	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();

	  //clear filters
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(4000);
	 	 
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	 // String ZipCode = ExcelUtils.getCellData(i,4); 
	  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	 
	  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).getAttribute("data-content");
	  System.out.println("Error:"+a);
	  	}
	 }

	@Test 
	public  void Profile_mandatoryfiled_ZipCode_Max() throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_mandatoryfiled_ZipCode_Max");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Business Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
	  Thread.sleep(6000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[1]")).click();

	  //clear filters
	 // driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear();
	  Thread.sleep(4000);
		  
	  String Address = ExcelUtils.getCellData(i,1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(Address);
	  
	  String City = ExcelUtils.getCellData(i,2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(City);
	 
	  String State = ExcelUtils.getCellData(i,3); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[5]/input")).sendKeys(State);
	  
	  String ZipCode = ExcelUtils.getCellData(i,12); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).sendKeys(ZipCode);
	  Thread.sleep(3000);
	  //dropdown
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[1]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Insurance");
	  Thread.sleep(5000);
	
	  WebElement mySelectElme = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[2]/select")); 
	  Select mySelecte= new Select(mySelectElme);
	  mySelecte.selectByVisibleText("Trust");
	  Thread.sleep(5000);
	  
	  String Turnover = ExcelUtils.getCellData(i,5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[4]/input")).sendKeys(Turnover);
	  
	  String transactionValue = ExcelUtils.getCellData(i,6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[5]/input")).sendKeys(transactionValue);
	  Thread.sleep(3000);
	  
	  String TANnumber = ExcelUtils.getCellData(i,7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[7]/input")).sendKeys(TANnumber);
	  
	  String Link = ExcelUtils.getCellData(i,8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[8]/input")).sendKeys(Link);
	  
	  String BusinessDescription = ExcelUtils.getCellData(i,9);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[2]/div/div/div[10]/input")).sendKeys(BusinessDescription);
	  Thread.sleep(3000);
	  
	  String PANnumber = ExcelUtils.getCellData(i,10); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(PANnumber);
	  
	  String NamePANcard = ExcelUtils.getCellData(i,11);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(NamePANcard);
	  Thread.sleep(3000);
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	  
	  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[7]/input")).getAttribute("data-content");
	  System.out.println("Error:"+a);

	  	}
	 }

@Test 
	public  void Profile_Contact() throws Exception
	 {	
	  System.out.println("--------------------------------");
	  System.out.println("Profile_Contact");
	  System.out.println("--------------------------------");
	  for(int i=1 ; i<2; i++ ) {
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
	  System.out.println("loaded: " + i);
	  String sUserName = ExcelUtils.getCellData(i, 1);
	  String sPassword = ExcelUtils.getCellData(i, 2);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
	  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
	  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
	  
	  driver.findElement(By.xpath(Xpath.Profile)).click();
	  String expectedTitle = "TraknPay Show Profile";
	  String actualTitle = driver.getTitle();
	  Assert.assertEquals(expectedTitle,actualTitle);
	  System.out.println("Traknpay Show Profile");
	  
	  //click on Contact Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[1]/div/div/button")).click();
	  Thread.sleep(4000);
	  
	  //click on edit              
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[1]")).click();

	  //clear filters
	 //contact Name
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
	 //Contact Number*
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
	  //Contact Email*
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();
	
	  //Secondary Contact Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).clear(); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).clear(); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).clear(); 
	  
	  //Authorized Signatory Details
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear(); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear(); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).clear(); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).clear(); 
	  Thread.sleep(4000);
	 
	  String contactName = ExcelUtils.getCellData(i,13);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).sendKeys(contactName);
	  
	  String contactNumber = ExcelUtils.getCellData(i,14);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(contactNumber);
	 
	  String contactEmail = ExcelUtils.getCellData(i,15); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(contactEmail);
	  
	  String ScontactName = ExcelUtils.getCellData(i,16); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).sendKeys(ScontactName); 
	
	  String ScontactNumber= ExcelUtils.getCellData(i,17);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).sendKeys(ScontactNumber); 
	 
	  String ScontactEmail = ExcelUtils.getCellData(i,18);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).sendKeys(ScontactEmail); 
	  Thread.sleep(3000);
	  
	  //Authorized Signatory Details	
	  String signatoryName = ExcelUtils.getCellData(i,19);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(signatoryName);
	  
	  String signatoryDesignation = ExcelUtils.getCellData(i,20);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(signatoryDesignation); 
	 
	  String signatoryContact = ExcelUtils.getCellData(i,21);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).sendKeys(signatoryContact); 
	  
	  String signatoryEmail  = ExcelUtils.getCellData(i,22);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).sendKeys(signatoryEmail);  
	  Thread.sleep(6000);
	 
 //update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(6000);
	  	}
	 }


@Test 
public  void Profile_Contact_mandatoryfiled_Name() throws Exception
 {	
 
  System.out.println("--------------------------------");
  System.out.println("Profile_Contact_mandatoryfiled_Name");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " + i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[1]")).click();

  //clear filters
 //contact Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
 //Contact Number*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
  //Contact Email*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();

  //Secondary Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).clear(); 
  
  //Authorized Signatory Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).clear(); 
  Thread.sleep(4000);
  
	 
 // String contactName = ExcelUtils.getCellData(i,13);
  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).sendKeys(contactName);
	  Thread.sleep(3000);
  String contactNumber = ExcelUtils.getCellData(i,14);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(contactNumber);
 
  String contactEmail = ExcelUtils.getCellData(i,15); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(contactEmail);
  
  String ScontactName = ExcelUtils.getCellData(i,16); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).sendKeys(ScontactName); 

  String ScontactNumber= ExcelUtils.getCellData(i,17);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).sendKeys(ScontactNumber); 
 
  String ScontactEmail = ExcelUtils.getCellData(i,18);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).sendKeys(ScontactEmail); 
  Thread.sleep(3000);
  
  //Authorized Signatory Details	
  String signatoryName = ExcelUtils.getCellData(i,19);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(signatoryName);
  
  String signatoryDesignation = ExcelUtils.getCellData(i,20);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(signatoryDesignation); 
 
  String signatoryContact = ExcelUtils.getCellData(i,21);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).sendKeys(signatoryContact); 
  
  String signatoryEmail  = ExcelUtils.getCellData(i,22);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).sendKeys(signatoryEmail);  
  Thread.sleep(4000);
 
//update
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[2]")).click();
  Thread.sleep(6000);

  String a=driver.findElement(By.xpath("  html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).getAttribute("data-content");
  System.out.println("Error:"+a); 
  	}
 }


@Test 
public  void Profile_Contact_mandatoryfiled_Number() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_Contact_mandatoryfiled_Number");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " + i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
   driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[1]")).click();

  //clear filters
 //contact Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
 //Contact Number*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
  //Contact Email*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();

  //Secondary Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).clear(); 
  
  //Authorized Signatory Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).clear(); 
  Thread.sleep(4000);	  
 
  String contactName = ExcelUtils.getCellData(i,13);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).sendKeys(contactName);
  
  //String contactNumber = ExcelUtils.getCellData(i,14);
  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(contactNumber);
 
  String contactEmail = ExcelUtils.getCellData(i,15); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(contactEmail);
  
  String ScontactName = ExcelUtils.getCellData(i,16); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).sendKeys(ScontactName); 

  String ScontactNumber= ExcelUtils.getCellData(i,17);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).sendKeys(ScontactNumber); 
 
  String ScontactEmail = ExcelUtils.getCellData(i,18);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).sendKeys(ScontactEmail); 
  Thread.sleep(3000);
  
  //Authorized Signatory Details	
  String signatoryName = ExcelUtils.getCellData(i,19);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(signatoryName);
  
  String signatoryDesignation = ExcelUtils.getCellData(i,20);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(signatoryDesignation); 
 
  String signatoryContact = ExcelUtils.getCellData(i,21);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).sendKeys(signatoryContact); 
  
  String signatoryEmail  = ExcelUtils.getCellData(i,22);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).sendKeys(signatoryEmail);  
  Thread.sleep(4000);
 
//update
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[2]")).click();
  Thread.sleep(6000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
  	}
 }

@Test 
public  void Profile_Contact_mandatoryfiled_email() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_Contact_mandatoryfiled_email");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[1]")).click();

  //clear filters
 //contact Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).clear();
 //Contact Number*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).clear();
  //Contact Email*
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).clear();

  //Secondary Contact Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).clear(); 
  
  //Authorized Signatory Details
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).clear(); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).clear(); 
  Thread.sleep(4000);
 
  String contactName = ExcelUtils.getCellData(i,13);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[1]/input")).sendKeys(contactName);
  
  String contactNumber = ExcelUtils.getCellData(i,14);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).sendKeys(contactNumber);
 
  //String contactEmail = ExcelUtils.getCellData(i,15); 
  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).sendKeys(contactEmail);
  
  String ScontactName = ExcelUtils.getCellData(i,16); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[1]/input")).sendKeys(ScontactName); 

  String ScontactNumber= ExcelUtils.getCellData(i,17);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[2]/input")).sendKeys(ScontactNumber); 
 
  String ScontactEmail = ExcelUtils.getCellData(i,18);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[2]/div/div[1]/div[4]/input")).sendKeys(ScontactEmail); 
  Thread.sleep(3000);
  
  //Authorized Signatory Details	
  String signatoryName = ExcelUtils.getCellData(i,19);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[1]/input")).sendKeys(signatoryName);
  
  String signatoryDesignation = ExcelUtils.getCellData(i,20);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[2]/input")).sendKeys(signatoryDesignation); 
 
  String signatoryContact = ExcelUtils.getCellData(i,21);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[4]/input")).sendKeys(signatoryContact); 
  
  String signatoryEmail  = ExcelUtils.getCellData(i,22);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[3]/div/div[1]/div[5]/input")).sendKeys(signatoryEmail);  
  Thread.sleep(4000);
 
//update
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[3]/div/button[2]")).click();
  Thread.sleep(6000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[4]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    

  	}
 }









@Test 
public  void Profile_BankDetail() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
 
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
 
  
  String Name = ExcelUtils.getCellData(i,23);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
  
  String AccounrNumber = ExcelUtils.getCellData(i,24);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
 
  String BankName = ExcelUtils.getCellData(i,25); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
  
  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
  Select mySelect= new Select(mySelectElm);
  mySelect.selectByVisibleText("Savings Account");
  Thread.sleep(3000);
  
  String Branch = ExcelUtils.getCellData(i,26); 
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

  String IFSC = ExcelUtils.getCellData(i,27);
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 
 
//update
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
  Thread.sleep(3000);
  	}
 }


@Test 
public  void Profile_BankDetail_mandatoryfiled_Name() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail_mandatoryfiled_Name");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
   driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
 
	  
	 // String Name = ExcelUtils.getCellData(i,23);
	//driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
	  
	  String AccounrNumber = ExcelUtils.getCellData(i,24);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
	 
	  String BankName = ExcelUtils.getCellData(i,25); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
	  
	  WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
	  Select mySelect= new Select(mySelectElm);
	  mySelect.selectByVisibleText("Savings Account");
	  Thread.sleep(3000);
	  
	  String Branch = ExcelUtils.getCellData(i,26); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

	  String IFSC = ExcelUtils.getCellData(i,27);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 	  
 
//update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
  	}
 }


@Test 
public  void Profile_BankDetail_mandatoryfiled_AccountNO() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail_mandatoryfiled_AccountNo");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
	
	String Name = ExcelUtils.getCellData(i,23);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
	  
	//String AccounrNumber = ExcelUtils.getCellData(i,24);
	//driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
	 
	String BankName = ExcelUtils.getCellData(i,25); 
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
	  
	WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
	Select mySelect= new Select(mySelectElm);
	mySelect.selectByVisibleText("Savings Account");
	Thread.sleep(3000);
	  
	String Branch = ExcelUtils.getCellData(i,26); 
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

	String IFSC = ExcelUtils.getCellData(i,27);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 
	
	//update
	 driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
  	}
 }

@Test 
public  void Profile_BankDetail_mandatoryfiled_BankName() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail_mandatoryfiled_BankName");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " + i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
  
	String Name = ExcelUtils.getCellData(i,23);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
	  
	String AccounrNumber = ExcelUtils.getCellData(i,24);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
	 
  //String BankName = ExcelUtils.getCellData(i,25); 
  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
	  
	WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
	Select mySelect= new Select(mySelectElm);
	mySelect.selectByVisibleText("Savings Account");
	Thread.sleep(3000);
	  
	String Branch = ExcelUtils.getCellData(i,26); 
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

	String IFSC = ExcelUtils.getCellData(i,27);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 
	 
//update
	 driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
	  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
  	}
 }



@Test 
public  void Profile_BankDetail_mandatoryfiled_Branch() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail_mandatoryfiled_Branch");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
  
	  String Name = ExcelUtils.getCellData(i,23);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
	  
	  String AccounrNumber = ExcelUtils.getCellData(i,24);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
	 
	  String BankName = ExcelUtils.getCellData(i,25); 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
	  
	 WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
	 Select mySelect= new Select(mySelectElm);
	 mySelect.selectByVisibleText("Savings Account");
	 Thread.sleep(3000);
	  
	//String Branch = ExcelUtils.getCellData(i,26); 
	//driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

	  String IFSC = ExcelUtils.getCellData(i,27);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 
	 
//update
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
 
  	}
 }


@Test 
public  void Profile_BankDetail_mandatoryfiled_IFSC() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_BankDetail_mandatoryfiled_IFSC");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
  //click on Bank Detail
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[1]/div/div/button")).click();
  Thread.sleep(4000);
  
  //click on edit              
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[1]")).click();

  //clear filters
  //Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).clear();
  //account no
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).clear();
  //Bank Name
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).clear();
  //Branch
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).clear();
  //ISFC code
  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).clear(); 
  Thread.sleep(4000);
  
	String Name = ExcelUtils.getCellData(i,23);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[1]/input")).sendKeys(Name);
	  
	String AccounrNumber = ExcelUtils.getCellData(i,24);
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[2]/input")).sendKeys(AccounrNumber);
	 
	String BankName = ExcelUtils.getCellData(i,25); 
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[4]/input")).sendKeys(BankName);
	  
	WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[5]/select")); 
	Select mySelect= new Select(mySelectElm);
	mySelect.selectByVisibleText("Savings Account");
	Thread.sleep(3000);
	  
	String Branch = ExcelUtils.getCellData(i,26); 
	driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[7]/input")).sendKeys(Branch); 

   //String IFSC = ExcelUtils.getCellData(i,27);
  //driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).sendKeys(IFSC); 
	 
//update
	 driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[3]/div/button[2]")).click();
	  Thread.sleep(3000);
  
  String a=driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[6]/div/div/div[2]/div/form/div/ul/li/div/div/div[8]/input")).getAttribute("data-content");
  System.out.println("Error:"+a);    
   	 }
   }
 

@Test 
public  void Profile_upload() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_upload");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(6000);
  
    //Company Logo	  
	  WebElement UploadImg = driver.findElement(By.id("bdoc_logo_original_file"));
	  UploadImg.sendKeys("D:\\profile\\asha.jpg");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[1]/div/button")).click();
	  Thread.sleep(4000);
	
	  
	//Upload Pan
	  WebElement Uploadpan = driver.findElement(By.id("bdoc_pan_file"));
	  Uploadpan.sendKeys("D:\\profile\\bmp.bmp");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[2]/div/button")).click();
	  Thread.sleep(4000);

	//Bank statement	  
	  WebElement UploadBankstatement = driver.findElement(By.id("bdoc_bank_file"));
	  UploadBankstatement.sendKeys("D:\\profile\\doc.doc");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[3]/div/button")).click();
	  Thread.sleep(4000);
	
	//Government Issued Certificate	  
	  WebElement UploadCertificate = driver.findElement(By.id("bdoo_file[0]"));
	  UploadCertificate.sendKeys("D:\\profile\\pdf.pdf");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[4]/div/button")).click();
	  Thread.sleep(4000);
	
	//Address proof  
	  WebElement UploadAddressproof= driver.findElement(By.id("bdoo_file[1]"));
	  UploadAddressproof.sendKeys("D:\\profile\\png.png");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[5]/div/button")).click();
	  Thread.sleep(4000);
	
	//BusinessRegistrationProof  
	  WebElement UploadRegistrationProof= driver.findElement(By.id("bdoo_file[2]"));
	  UploadRegistrationProof.sendKeys("D:\\profile\\doc.doc");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[6]/div/button")).click();
	  Thread.sleep(4000);
	  
	//Authorized Signatory document
	  WebElement UploadSignatorydocument = driver.findElement(By.id("bdoo_file[3]"));
	  UploadSignatorydocument.sendKeys("D:\\profile\\pdf.pdf");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[7]/div/button")).click();
	  Thread.sleep(4000);
	  
	//Cancelled cheque  
	  WebElement UploadCancelledcheque = driver.findElement(By.id("bdoo_file[4]"));
	  UploadCancelledcheque.sendKeys("D:\\profile\\doc.doc");
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[8]/div/button")).click();
	  Thread.sleep(4000);
	         
  		}
 	}


@Test 
public  void Profile_upload_CompanyLogo() throws Exception
 {	
  
  System.out.println("--------------------------------");
  System.out.println("Profile_upload_CompanyLogo");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);

    //Company Logo	  
	  WebElement UploadImg = driver.findElement(By.id("bdoc_logo_original_file"));
	  UploadImg.sendKeys("D:\\profile\\doc.doc");
	  Thread.sleep(2000);
		  
   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[1]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]")).getText().equalsIgnoreCase("The Business Logo must be a file of type: jpeg, bmp, png, jpg."))
	  {                            
	System.out.println(" Error:The Business Logo must be a file of type: jpeg, bmp, png, jpg.");
	  }
  		}
 	}


@Test 
public  void Profile_upload_Pan() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_upload_Pan");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);
 	
	//Upload Pan
	  WebElement Uploadpan = driver.findElement(By.id("bdoc_pan_file"));
	  Uploadpan.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	
   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[2]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The PAN file must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The PAN file must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
  		}
 	}


@Test 
public  void Profile_upload_Bank_statement() throws Exception
 {	
  
  System.out.println("--------------------------------");
  System.out.println("Profile_Bank_statement");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);

    //Bank statement	  
	  WebElement UploadBankstatement = driver.findElement(By.id("bdoc_bank_file"));
	  UploadBankstatement.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
		
   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[3]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Bank file must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Bank file must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
  		}
 	}


@Test 
public  void Profile_upload_Government_Issued_Certificate() throws Exception
 {	
  System.out.println("--------------------------------");
  System.out.println("Profile_Government_Issued_Certificate	");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);

    //Government Issued Certificate	  
	  WebElement UploadCertificate = driver.findElement(By.id("bdoo_file[0]"));
	  UploadCertificate.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[4]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Government Issued Certificate must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Government Issued Certificate must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
  }
 }

@Test 
public  void Profile_upload_Address_proof () throws Exception
 {	

  System.out.println("--------------------------------");
  System.out.println("Profile_Address_proof");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);

   //Address proof  
	  WebElement UploadAddressproof= driver.findElement(By.id("bdoo_file[1]"));
	  UploadAddressproof.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	
	driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[5]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Address proof must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Address proof must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
   		}
 	}


@Test 
public  void Profile_upload_Business_RegistrationProof() throws Exception
 {	

  System.out.println("--------------------------------");
  System.out.println("Profile_Business_RegistrationProof");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);

	//BusinessRegistrationProof  
	  WebElement UploadRegistrationProof= driver.findElement(By.id("bdoo_file[2]"));
	  UploadRegistrationProof.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[6]/div/button")).click();
	  Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Business Registration Proof must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Business Registration Proof must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
   		}
 	}


@Test 
public  void Profile_upload_Authorized_Signatory() throws Exception
 {	

  System.out.println("--------------------------------");
  System.out.println("Profile_Authorized_Signatory_document");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);
   
	//Authorized Signatory document
	  WebElement UploadSignatorydocument = driver.findElement(By.id("bdoo_file[3]"));
	  UploadSignatorydocument.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	  
   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[7]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Authorized Signatory Document must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Authorized Signatory Document must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
   		}
 	}


@Test 
public  void Profile_upload_Cancelled_cheque() throws Exception
 {	
 
  System.out.println("--------------------------------");
  System.out.println("Profile_Cancelled_cheque");
  System.out.println("--------------------------------");
  for(int i=1 ; i<2; i++ ) {
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
  System.out.println("loaded: " +i);
  String sUserName = ExcelUtils.getCellData(i, 1);
  String sPassword = ExcelUtils.getCellData(i, 2);
  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
  ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Profile,"Sheet1");
  driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
  
  driver.findElement(By.xpath(Xpath.Profile)).click();
  String expectedTitle = "TraknPay Show Profile";
  String actualTitle = driver.getTitle();
  Assert.assertEquals(expectedTitle,actualTitle);
  System.out.println("Traknpay Show Profile");
  
   driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[1]/div/button")).click();
   Thread.sleep(4000);
 
   //Cancelled cheque  
	  WebElement UploadCancelledcheque = driver.findElement(By.id("bdoo_file[4]"));
	  UploadCancelledcheque.sendKeys("D:\\profile\\xls.xlsx");
	  Thread.sleep(2000);
	  
   driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/div/ul/li/div/div[1]/form[8]/div/button")).click();
	Thread.sleep(3000);
 
	if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[8]/div/div/div[2]/div/ul[1]/li")).getText().equalsIgnoreCase("The Cancelled cheque must be a file of type: jpeg, bmp, png, jpg, pdf, doc."))
	  {                            
	System.out.println(" Error:The Cancelled cheque must be a file of type: jpeg, bmp, png, jpg, pdf, doc.");
	  }
   		}
 	}


@Test
public  void Profile_sessiontimeout() throws Exception 
{	
	System.out.println("--------------------------------");
	System.out.println("Profile_sessiontimeout time out");
	System.out.println("--------------------------------");
	for(int i=1 ; i<2; i++ )
	{
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		System.out.println("loaded: " +i);
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
		
		driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a")).click();
		driver.findElement(By.xpath(Xpath.Profile)).click();
		//start system time
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

		try
		{
			Thread.sleep(16*60*1000);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		date = new Date();
		Thread.sleep(2000);
		System.out.println(dateFormat.format(date)); 
	}		
}

}