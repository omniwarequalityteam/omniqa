package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;


public class Products_page {

	public static WebDriver driver=new FirefoxDriver();
	private static WebElement element=null;
	private static String Name;

	@BeforeTest
	public void beforeTest() 
	{
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}
	@Test
	public void add_product() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("add product");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			//driver.findElement(By.xpath(Xpath.addproduct)).click();
			driver.findElement(By.xpath(Xpath.addproduct)).click();

			String expectedTitle = "TraknPay Business Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String name = Name;
			String Description = ExcelUtils.getCellData(i,2);
			String price = ExcelUtils.getCellData(i,3);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/textarea")).sendKeys(Description);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(price);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Products";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		} 

	}
	@Test
	public void addproduct_namemandatory() throws Exception
	{

		for(int i=1 ; i<2; i++ )
		{

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("name mandatory field");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.addproduct)).click();
			String expectedTitle = "TraknPay Business Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			//String name = ExcelUtils.getCellData(i,1);
			String Description = ExcelUtils.getCellData(i,2);
			String price = ExcelUtils.getCellData(i,3);

			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/textarea")).sendKeys(Description);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(price);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();



			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Product Name field is required."))

			{
				System.out.println(" pass:product Name field is required");

			}
			else
			{

				System.out.println(" fail:product Name field is required");

			}

		}

	}

	@Test
	public void addproduct_description_mandatory() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("description mandatory field");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.addproduct)).click();
			String expectedTitle = "TraknPay Business Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			String name = ExcelUtils.getCellData(i,1);
			//String Description = ExcelUtils.getCellData(i,2);
			String price = ExcelUtils.getCellData(i,3);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(name);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/textarea")).sendKeys(Description);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(price);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Product Description field is required."))

			{
				System.out.println(" pass:product description field is required");

			}
			else
			{

				System.out.println("fail:product description field is required");

			}


		} 

	}
	@Test
	public void addproduct_productprice_mandatory() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("product price mandatory field");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.addproduct)).click();
			String expectedTitle = "TraknPay Business Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			String name = ExcelUtils.getCellData(i,1);
			String Description = ExcelUtils.getCellData(i,2);
			//String price = ExcelUtils.getCellData(i,3);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/textarea")).sendKeys(Description);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(price);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Product Price field is required."))

			{
				System.out.println(" pass:product description field is required");

			}
			else
			{

				System.out.println("fail:product description field is required");

			}

		} 

	}
	@Test
	public void manageproduct_filters() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("filters");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			//driver.findElement(By.xpath(Xpath.manageproduct)).click();
			driver.findElement(By.xpath(Xpath.manageproduct)).click();

			String expectedTitle = "TraknPay Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//product id
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(Keys.RETURN);

			//product name
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element1=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[2]/div"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(Keys.RETURN);


			//product description
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element2=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[3]/div"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(strng2);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(Keys.RETURN);


			//product price
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element3=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[4]"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(strng3);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(Keys.RETURN);


		}
	}

	@Test
	public void manageproduct_edit() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			System.out.println("product edit");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.manageproduct)).click();


			String expectedTitle = "TraknPay Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay prducts");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			String editproductprice = ExcelUtils.getCellData(i,8);

			//edit button
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[5]/a[1]/span")).click();
			Thread.sleep(3000);

			expectedTitle = "TraknPay Edit Product";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			driver.findElement(By.xpath(".//*[@id='bpro_product_price']")).clear();
			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@id='bpro_product_price']")).sendKeys(editproductprice);
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Products";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		} 

	}

	@Test
	public void manageproduct_addproductbutton() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("add product button");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.manageproduct)).click();

			String expectedTitle = "TraknPay Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");	

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a/i")).click();
			expectedTitle = "TraknPay Business Products";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Products,"Sheet1");
			String name = ExcelUtils.getCellData(i,1);
			String Description = ExcelUtils.getCellData(i,2);
			String price = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/textarea")).sendKeys(Description);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(price);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Products";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		} 
	}
	@Test
	public void manageproduct_deletebutton() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("delete product");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.manageproduct)).click();

			String expectedTitle = "TraknPay Products";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]/a[2]/span")).click();
			                             
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Products";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);



		}
	}
	@Test
	public  void products_sessiontimeout() throws Exception 
	{	

		System.out.println("products page session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.manageproduct)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 


		}
	}




}
