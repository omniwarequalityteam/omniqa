package Testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Testing.WriteToExcel;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class BulkInvoices_page {
	public static WebDriver driver = new FirefoxDriver();


	private static WebElement element = null;
	private static String Name;
	private static String designName;

	public static String downloadPath = "C:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\downloaded files";
	public static String writedatapath = "C:\\eclipse\\workspace\\TraknPay_testing\\testdocs";

	@BeforeTest
	public void beforeTest() 
	{
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}

	@Test 
	public  void createdesign_foruploadexcel() throws Exception

	{	
		System.out.println("createdesign_create Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");
			//driver.findElement(By.xpath(Xpath.createdesign)).click();

			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);



			String Designname = ExcelUtils.getCellData(i,1);
			//String Description = ExcelUtils.getCellData(i,2);
			String adress = ExcelUtils.getCellData(i,3);
			//String Field2 = ExcelUtils.getCellData(i,4);
			//String Field3 = ExcelUtils.getCellData(i,5);
			//String Field4 = ExcelUtils.getCellData(i,6);
			String  standard= ExcelUtils.getCellData(i,7);
			String rollno = ExcelUtils.getCellData(i,8);
			String section = ExcelUtils.getCellData(i,9);
			//String Field8 = ExcelUtils.getCellData(i,10);
			String schoolfee = ExcelUtils.getCellData(i,11);
			//String Itemname2 = ExcelUtils.getCellData(i,12);
			//String SubTotal = ExcelUtils.getCellData(i,13);
			//String Tax = ExcelUtils.getCellData(i,14);
			//String servicetax = ExcelUtils.getCellData(i,15);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			//driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(adress);   
			//driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			//driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			//driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(standard);
			driver.findElement(By.name("invd_f6_label")).sendKeys(rollno);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(section);
			//driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(schoolfee);
			// To add + 
			//driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			//driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			//driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotadriver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			//driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div[1]/h3/strong"));


			String strng = element.getText();
			System.out.println(strng);


		}
	}


	@Test
	public void uploadexcel_exceldownload() throws Exception
	{
		System.out.println("---Managedesigns_exceldownload---");
		System.out.println("-------------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
		WebDriver driver = new FirefoxDriver(FirefoxDriverProfile1());	
		driver.manage().window().maximize();
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.get(Constant.URL);
		driver.findElement(By.name("busi_login")).sendKeys(sUserName);
		driver.findElement(By.name("password")).sendKeys(sPassword);
		driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
		Thread.sleep(3000);
		//driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[8]/a/span")).click();
		driver.findElement(By.xpath(Xpath.managedesign)).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]/a[2]/span")).click(); 
		//LogOut_Page.logout(driver); 
	}
		}

	public static FirefoxProfile FirefoxDriverProfile1() throws Exception 
	{
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;


	}

	@Test
	@Parameters({"filePath","fileName","sheetName","dataToWrite"})
	public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws Exception
	{
		System.out.println("write data to excel");
		File file = new File(filePath+"\\"+fileName);
		System.out.println("sheet name:"+file);

		for(int i=0 ; i<1; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Bulk_InvoiceData,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHH");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,0)+formattedDate;
			String name = Name;
			String phoneno = ExcelUtils.getCellData(i,1);
			String email = ExcelUtils.getCellData(i,2);
			String adress = ExcelUtils.getCellData(i,3);
			String standard = ExcelUtils.getCellData(i,4);
			String invoicedate = ExcelUtils.getCellData(i,5);
			String duedate = ExcelUtils.getCellData(i,6);
			String rollno = ExcelUtils.getCellData(i,7);
			String section = ExcelUtils.getCellData(i,8);
			String schoolfee = ExcelUtils.getCellData(i,9);
			String nettotal = ExcelUtils.getCellData(i,10);
			String sendondate = ExcelUtils.getCellData(i,11);

			String[] valueToWrite  = {name,phoneno,email,adress,standard,invoicedate,duedate,rollno,section,schoolfee,nettotal,sendondate};

			FileInputStream inputStream = new FileInputStream(file);
			Workbook guru99Workbook = null;
			String fileExtensionName = fileName.substring(fileName.indexOf("."));
			if(fileExtensionName.equals(".xlsx")){
				guru99Workbook = new XSSFWorkbook(inputStream);
			}  
			System.out.println("sheet name:"+sheetName);
			Sheet sheet = guru99Workbook.getSheet(sheetName);
			System.out.println("value of sheet is: "+sheet);

			int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
			System.out.println("rowcount: "+rowCount);
			Row row = sheet.getRow(0);
			Row newRow = sheet.createRow(rowCount+1);

			for(int j = 0; j < row.getLastCellNum(); j++){
				Cell cell = newRow.createCell(j);

				cell.setCellValue(dataToWrite[j]);

			}
			inputStream.close();
			FileOutputStream outputStream = new FileOutputStream(file);
			guru99Workbook.write(outputStream);
			outputStream.close();


			//WriteToExcel objExcelFile = new WriteToExcel();
			//objExcelFile.writeExcel(System.getProperty("user.dir")+"\\InputFiles\\excel files","excel_file_excel_file_dakshi07132016163608.xlsx","Invoice Data",valueToWrite);
		}
	}

	/*public static void main(String args[]) throws Exception{

	    //Create an array with the data in the same order in which you expect to be filled in excel file




		 for(int i=0 ; i<1; i++ )
			 {
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Bulk_InvoiceData,"Sheet1");
			 Date date = new Date();
		     SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHH");
		     String formattedDate = sdf.format(date);
		           Name=ExcelUtils.getCellData(i,0)+formattedDate;
		           String name = Name;
				    String phoneno = ExcelUtils.getCellData(i,1);
					String email = ExcelUtils.getCellData(i,2);
					String adress = ExcelUtils.getCellData(i,3);
					String standard = ExcelUtils.getCellData(i,4);
					String invoicedate = ExcelUtils.getCellData(i,5);
					String duedate = ExcelUtils.getCellData(i,6);
					String rollno = ExcelUtils.getCellData(i,7);
					String section = ExcelUtils.getCellData(i,8);
					String schoolfee = ExcelUtils.getCellData(i,9);
					String nettotal = ExcelUtils.getCellData(i,10);
					String sendondate = ExcelUtils.getCellData(i,11);

	    String[] valueToWrite = {name,phoneno,email,adress,standard,invoicedate,duedate,rollno,section,schoolfee,nettotal,sendondate};

	    //Create an object of current class

	    WriteToExcel objExcelFile = new WriteToExcel();

	    System.out.println("value is "+valueToWrite );

	    //Write the file using file name , sheet name and the data to be filled

	    //objExcelFile.writeExcel(System.getProperty("user.dir")+"D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files","shravya.xlsx","Sheet1",valueToWrite);
	    objExcelFile.writeExcel(System.getProperty("user.dir")+"\\InputFiles\\excel files","excel_file_excel_file_dakshi07132016163608.xlsx","Invoice Data",valueToWrite);

	    ExcelUtils.setCellData("Pass", i,12 ,Constant.Path_TestData + Constant.File_Bulk_InvoiceData);



			 }



	} */



	@Test
	public void BulkInvoices_Uploadexcelfile() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("Upload excel file");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			// bulk invoice click
			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			//upload excel file click
			//   driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();

			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);

			driver.findElement(By.id("invh_name")).sendKeys(name);
			Thread.sleep(2000);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlineshopping_upload.xlsx");

			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);



		}

	}
	@Test
	public void Uploadexcelfile_reviewandsendinvoices() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("review and send invoices");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			//driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();

			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			//filters
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("no");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a[1]")).click();
			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}
	}


	@Test
	public void Uploadexcelfile_collectionnameerror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {


			System.out.println("Collection name error");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_divya805022016140446-uploadexcel.xlsx");
			Thread.sleep(1000); 



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[1]/div[2]/p")).getText().equalsIgnoreCase("The Collection Name field is required."))

			{
				System.out.println("pass:The Collection Name field is required");


			}
			else
			{
				System.out.println("fail:The Collection Name field is required");


			}

		}

	}

	@Test
	public void Uploadexcelfile_excelfileerror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{


			System.out.println("Excel file error");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;			     
			String description = ExcelUtils.getCellData(i,2);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[4]/div[2]/p")).getText().equalsIgnoreCase("The Invoice data file field is required."))

			{
				System.out.println("pass:The Invoice data file field is required.");

			}
			else
			{
				System.out.println("fail:The Invoice data file field is required.");

			}

		}

	}

	@Test
	public void Uploadexcelfile_sendonpreviousdateerror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {


			System.out.println("Send on previous date error");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;			     
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);



			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);
			//driver.findElement(By.id("invh_send_on")).clear();
			//driver.findElement(By.id("invh_send_on")).sendKeys(sendondate);

			//select date after yesterday

			driver.findElement(By.id("invh_send_on")).click(); //click field
			driver.findElement(By.xpath("html/body/div[3]/div[1]/div[2]/table/thead/tr[1]/th[1]/i")).click(); //click next month
			driver.findElement(By.xpath("html/body/div[3]/div[1]/div[2]/table/tbody/tr[3]/td[3]")).click(); //click day

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_hostelmanagement (1).xlsx");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[3]/div[2]/p")).getText().equalsIgnoreCase("The Send On Date must be a date after yesterday."))

			{
				System.out.println("pass:The Send On Date must be a date after yesterday. ");

			}
			else
			{

				System.out.println("fail:The Send On Date must be a date after yesterday. ");



			}
		}

	}




	@Test
	public void Uploadexcelfile_emptyexcelfileerror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {


			System.out.println("Empty excel file error");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay upload page");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;			     
			String description = ExcelUtils.getCellData(i,2);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlinemarketing_emptyfile.xlsx");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			//Get text from page

			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]"));
			String strng = element.getText();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]")).getText().equalsIgnoreCase(strng))

			{
				System.out.println("Pass:Something not right with your datafile, check if there is data in uploaded file ");

			}
			else
			{
				System.out.println("fail:Something not right with your datafile, check if there is data in uploaded file ");


			}

		}

	}


	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_show() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {


			System.out.println("List invoices send/scheduled show");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[1]/span/span")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div[1]/div/a")).click();
			String winHandleBefore = driver.getWindowHandle();
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div[1]/div/a")).click();

			// Close child window

			for(String winHandle : driver.getWindowHandles()) 
			{
				driver.switchTo().window(winHandle);
			}
			driver.close();
			driver.switchTo().window(winHandleBefore);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[3]/button")).click();

		}

	}
	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_edit() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled edit");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[3]/td[9]/a[3]/span")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			String roomtypeedit = ExcelUtils.getCellData(i,9);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[2]/span")).click();
			driver.findElement(By.name("invd_f8")).clear();
			driver.findElement(By.name("invd_f8")).sendKeys(roomtypeedit);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
		}


	}

	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_send() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled send");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[3]/div[2]/div/ul/li[4]/a")).click();

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[3]/span")).click();

			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}

	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_duplicateandedit() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled duplicate and edit");
			System.out.println("--------------------------"); 
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String name = ExcelUtils.getCellData(i,11)+formattedDate;
			String phoneno = ExcelUtils.getCellData(i,12);
			String emailid = ExcelUtils.getCellData(i,13);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[4]/span")).click();
			//html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[4]
			expectedTitle = "TraknPay Create Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			driver.findElement(By.name("invd_phone")).sendKeys(phoneno);
			driver.findElement(By.name("invd_email")).sendKeys(emailid);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}


	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentcash() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/schedule manual payment cash");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			//html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span
			Thread.sleep(3000);


			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(3000);

			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));
			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.id("mapa_payment_amount")).sendKeys(strng);
			driver.findElement(By.id("mapa_payment_note")).sendKeys("amount has been paid ");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}

	}

	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentcashremove() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment remove");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(3000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/a[1]")).click();
			Thread.sleep(3000);

			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}



	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentcheque() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment cheque");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			//filter

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/ul/li[2]/a/i/span[1]")).click();
			Thread.sleep(2000);
			driver.findElement(By.id("mapa_transaction_no")).sendKeys("123456");
			Thread.sleep(2000);
			
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));
			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[2]/div[3]/div/input")).sendKeys(strng);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[2]/div[3]/div/input")).sendKeys("17");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[2]/div[4]/div/input")).sendKeys("SBI");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}

	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentchequeremove() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment cheque remove");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			//filter

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);



			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(3000);

			Thread.sleep(3000);
			expectedTitle = "TraknPay Update Manual Payment";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/a[1]")).click();
			Thread.sleep(3000);

			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}



	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentcredit() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment credit");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();


			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/ul/li[3]/a/i/span[1]")).click();
			Thread.sleep(2000);
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));
			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[3]/div[2]/div/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[3]/div[3]/div/input")).sendKeys("123456");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[3]/div[4]/div/input")).sendKeys("visa");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[3]/div[5]/div/input")).sendKeys("SBI");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}

	}


	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentcreditremove() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment credit remove");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay Update Manual Payment";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay update manual payment page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/a[1]")).click();
			Thread.sleep(3000);

			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}


	}


	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentnetbanking() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment cheque");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			//filter

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/ul/li[4]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[4]/div[2]/div/input")).sendKeys("123456");
			Thread.sleep(2000);
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[1]/div/div[3]/div[2]"));
			String strng = element.getText();
			System.out.println(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[4]/div[3]/div/input")).sendKeys(strng);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div/div[2]/div[4]/div[4]/div/input")).sendKeys("SBI");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}


	}

	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentnetbankingremove() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment cheque remove");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay Update Manual Payment";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay update manual payment page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/a[1]")).click();
			Thread.sleep(3000);

			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}





	@Test
	public void Uploadexcelfile_listinvoicessendscheduled_manualpaymentamounterror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("List invoices send/scheduled manual payment amount error");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices page");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[12]/a[5]/span")).click();
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();


			Thread.sleep(2000);
			driver.findElement(By.id("mapa_payment_amount")).sendKeys("20");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();


			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]"));
			String strng = element.getText();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]")).getText().equalsIgnoreCase(strng))

			{
				System.out.println("pass:Whoops! There were some problems with your input. Please see below, correct errors and submit the form again ");

			}
			else
			{
				System.out.println("fail:Whoops! There were some problems with your input. Please see below, correct errors and submit the form again ");


			}

		}


	}

	@Test
	public void Uploadexcelfile_listofuploadsdelete() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("list of uploads delete");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			//filters
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("no");
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[2]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}

	}		     



	@Test
	public void Uploadexcelfile_button() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("upload excel file button");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoices headers page");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
			expectedTitle = "TraknPay Upload Excel";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);

			driver.findElement(By.id("invh_name")).sendKeys(name);
			driver.findElement(By.id("invh_description")).sendKeys(description);
			Thread.sleep(3000); 

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));

			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlineshopping_addbutton.xlsx");
			Thread.sleep(4000); 



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}
	}

	@Test
	public void bulkinvoices_filter() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("bulk invoices filters");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			//filters

			//Upload id
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);

			//name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[2]/div"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			//
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element11=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/div"));
			String strng11 = element11.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]"));
			String strng111 = element111.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(strng111);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);




			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select"));
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("no");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element1111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[7]"));
			String strng1111 = element1111.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(strng1111);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element11111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]"));
			String strng11111 = element11111.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng11111);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select"));
			Select mySelect3= new Select(mySelectElm3);
			mySelect3.selectByVisibleText("25");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/div[2]/table/tbody/tr[5]/td[5]")).click();

		}

	}

	@Test
	public void bulkinvoices_pages() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("bulk invoices pages");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);



			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();

			Thread.sleep(3000);
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();


		}
	}
	@Test
	public void Uploadexcelfile_confirmdisable() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("confirm button disable");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			String expectedTitle = "TraknPay List Invoices Headers";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("yes");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]/span")).click();
			expectedTitle = "TraknPay List Uploaded Excels Data";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a[1]")).click();

			File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(screenshot, new File("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\screenshots\\screenshot_confirmdisable.jpg")); 
			System.out.println("Sendinvoice page is captured and stored in your D: Drive");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a[2]")).click();
			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}
	}


	@Test
	public void BulkInvoices_excel_duedateerror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("due date error");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay upload page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlineshopping_duedateerror.xlsx");
			Thread.sleep(2000); 



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("no");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]")).click();
			expectedTitle = "TraknPay List Uploaded Excels Data";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]/ul/li[1]")).getText().equalsIgnoreCase("The Invoice Due Date must be on or after the Invoice Date"))

			{
				System.out.println("pass:The Invoice Due Date must be on or after the Invoice Date");


			}
			else
			{

				System.out.println("fail:The Invoice Due Date must be on or after the Invoice Date");


			}

		}
	}


	@Test
	public void BulkInvoices_excel_totalamounterror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("Total amount error");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlineshopping_amounterror.xlsx");
			Thread.sleep(2000); 



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//filters
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("no");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]")).click();
			expectedTitle = "TraknPay List Uploaded Excels Data";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[8]/ul/li")).getText().equalsIgnoreCase("The Total Amount field is required."))

			{
				System.out.println("pass:The Total Amount field is required");

			}
			else
			{

				System.out.println("fail:The Total Amount field is required");


			}


		}
	}



	@Test
	public void BulkInvoices_addcustomlineitems() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("Add custom line items");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_onlineshopping_addcustomitems.xlsx");
			Thread.sleep(2000); 



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//filters
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("no");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a[1]")).click();

			expectedTitle = "TraknPay List Invoices Headers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
		}
	}


	@Test
	public void BulkInvoices_addcustom_lineitemserror() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("Add 6 custom line items");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.uploadexcelfile)).click();
			String expectedTitle = "TraknPay Upload Excel";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_BulkInvoicing,"Sheet1");


			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			String collectionname = ExcelUtils.getCellData(i,1)+formattedDate;
			String name = collectionname;	
			String description = ExcelUtils.getCellData(i,2);
			//String sendondate = ExcelUtils.getCellData(i,3);


			driver.findElement(By.id("invh_name")).sendKeys(collectionname);
			driver.findElement(By.id("invh_description")).sendKeys(description);

			//upload file

			WebElement fileInput = driver.findElement(By.id("invh_original_file"));
			fileInput.sendKeys("D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files\\excel_file_divya805172016134346_errorcustomlineitems.xlsx");
			Thread.sleep(2000); 
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div/div[5]/div/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]"));

			String strng = element.getText();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]")).getText().equalsIgnoreCase(strng))

			{
				System.out.println("pass: Something not right with your datafile, You can add upto 5 line items in Excel ");

			}
			else
			{
				System.out.println(" Fail: Something not right with your datafile, You can add upto 5 line items in Excel ");

			}

		}
		}
		@Test
		public void listinvoicessendscheduled_filter() throws Exception
		{

			for(int i=1 ; i<2; i++ ) {

				System.out.println("bulk invoices filters");
				System.out.println("--------------------------");
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


				driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
				driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
				String expectedTitle = "TraknPay List Invoices Headers";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
				//sent invoice
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
				WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
				Select mySelect= new Select(mySelectElm);
				mySelect.selectByVisibleText("yes");
				Thread.sleep(2000);
				//click on list of invoices send scheduled
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();
				
				expectedTitle = "TraknPay List Invoices";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
				//filters
				//TNP ID
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
				String strng1 = element1.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng1);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//Invoice no
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element2=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
				String strng2 = element2.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng2);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//Date
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).click();
				driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[4]")).click();
				Thread.sleep(2000);
				
				//Total
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element3=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
				String strng3 = element3.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng3);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//split no
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
				Select mySelect1= new Select(mySelectElm1);
				mySelect1.selectByVisibleText("no");
				Thread.sleep(2000);
				
				//split yes
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/select")); 
				Select mySelect2= new Select(mySelectElm2);
				mySelect2.selectByVisibleText("yes");
				Thread.sleep(2000);
				
				//due date
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).click();
				driver.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[4]")).click();
				Thread.sleep(2000);
				
				//name
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element4=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[7]/div"));
				String strng4 = element4.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(strng4);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//phone
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element5=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]"));
				String strng5 = element5.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng5);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//email
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement element6=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/div"));
				String strng6 = element6.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(strng6);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				
				//Recurring invoice no
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement mySelectElm3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
				Select mySelect3= new Select(mySelectElm3);
				mySelect3.selectByVisibleText("yes");
				Thread.sleep(2000);
				
				//recurring invoice yes
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
				WebElement mySelectElm4 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/select")); 
				Select mySelect4= new Select(mySelectElm4);
				mySelect4.selectByVisibleText("no");
					
			}

	}

		@Test
		public void bulkinvoices_Exporttoexcelfile() throws Exception
		{

			System.out.println("export to excel file of bulk invoice list of invoices send and scheduled");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) 
			{

				WebDriver driver = new FirefoxDriver(FirefoxDriverProfile1());	
				driver.manage().window().maximize();
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.get("http://biz.traknpay.in/auth/login");
				driver.findElement(By.name("busi_login")).sendKeys(sUserName);
				driver.findElement(By.name("password")).sendKeys(sPassword);
				driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
				Thread.sleep(3000);

				driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
				
				//sent invoice
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
				WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
				Select mySelect= new Select(mySelectElm);
				mySelect.selectByVisibleText("yes");
				Thread.sleep(2000);
				//click on list of invoices send scheduled
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[3]/span")).click();

				//title check
				String expectedTitle = "TraknPay List Invoices";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				Thread.sleep(2000);

				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div/a[1]/i")).click();
				LogOut_Page.logout(driver);
			}

		}
		


	@Test
	public  void bulkinvoice_sessiontimeout() throws Exception 
	{	

		System.out.println("bulk invoice session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.bulkinvoices)).click();
			driver.findElement(By.xpath(Xpath.reviewandsendinvoices)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 

			

}
	}




}










