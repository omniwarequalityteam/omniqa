package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class Users_Page {

	public static WebDriver driver = new FirefoxDriver();
	private static String Email;

	@BeforeTest
	public void beforeTest() 
	{
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}

	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}

	@Test
	public  void users_Adduser() throws Exception

	{	

		System.out.println("Add user");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

			driver.findElement(By.xpath(Xpath.addusers)).click();
			String expectedTitle = "TraknPay Business User";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			
			String username = ExcelUtils.getCellData(i,1);
			String password = ExcelUtils.getCellData(i,2);
			String retypepassword = ExcelUtils.getCellData(i,3);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Email=ExcelUtils.getCellData(i,4)+formattedDate;
			String email =Email;


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(username);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Users";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}

	}


	@Test
	public  void username_mandatory() throws Exception

	{	

		System.out.println("username mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

			driver.findElement(By.xpath(Xpath.addusers)).click();
			String expectedTitle = "TraknPay Business User";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			// String username = ExcelUtils.getCellData(i,1);
			String password = ExcelUtils.getCellData(i,2);
			String retypepassword = ExcelUtils.getCellData(i,3);

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Email=ExcelUtils.getCellData(i,4)+formattedDate;
			String email =Email; 

		
				// driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Name field is required."))

				{
					System.out.println("pass:Name field is required");

				}
				else
				{


					System.out.println("fail:Name field is required");

				}

			}
		}



		@Test
		public  void password_mandatory() throws Exception

		{	

			System.out.println("password mandatory");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.addusers)).click();
				String expectedTitle = "TraknPay Business User";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);


				String username = ExcelUtils.getCellData(i,1);
				//String password = ExcelUtils.getCellData(i,2);
				String retypepassword = ExcelUtils.getCellData(i,3);

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
				String formattedDate = sdf.format(date);
				Email=ExcelUtils.getCellData(i,4)+formattedDate;
				String email =Email; 


				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Password field is required."))

				{
					System.out.println("pass:The Password field is required.");


				}
				else
				{

					System.out.println("fail:The Password field is required.");


				}

			}
		}

		@Test
		public  void email_mandatory() throws Exception

		{	

			System.out.println("email mandatory");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.addusers)).click();
				String expectedTitle = "TraknPay Business User";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);


				String username = ExcelUtils.getCellData(i,1);
				String password = ExcelUtils.getCellData(i,2);
				String retypepassword = ExcelUtils.getCellData(i,3);




				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/p")).getText().equalsIgnoreCase("The Email ID field is required."))

				{
					System.out.println("pass:The Email ID field is required.");

				}
				else
				{

					System.out.println("fail:The Email ID field is required.");

				}

			}
		}

		@Test
		public  void users_edituser() throws Exception

		{	

			System.out.println("Edit user");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.manageusers)).click();
				String expectedTitle = "TraknPay Users";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);

				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr/td[3]/a[1]")).click();
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
				String formattedDate = sdf.format(date);
				Email=ExcelUtils.getCellData(i,9)+formattedDate;
				String email =Email; 

				driver.findElement(By.xpath(".//*[@id='buse_email']")).clear();
				driver.findElement(By.xpath(".//*[@id='buse_email']")).sendKeys(email);

				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
				Thread.sleep(3000);
				expectedTitle = "TraknPay Users";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);


			}

		}


		@Test
		public  void users_addbutton() throws Exception

		{	


			System.out.println("Add button");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.manageusers)).click();
				String expectedTitle = "TraknPay Users";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);

				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				expectedTitle = "TraknPay Business User";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);

				String username = ExcelUtils.getCellData(i,1);
				String password = ExcelUtils.getCellData(i,2);
				String retypepassword = ExcelUtils.getCellData(i,3);

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
				String formattedDate = sdf.format(date);
				Email=ExcelUtils.getCellData(i,4)+formattedDate;
				String email =Email;


				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				expectedTitle = "TraknPay Users";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);


			}

		}
		@Test
		public  void users_deletebutton() throws Exception

		{	

			System.out.println("delete button");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

				driver.findElement(By.xpath(Xpath.manageusers)).click();
				String expectedTitle = "TraknPay Users";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				System.out.println("Traknpay users");
				Thread.sleep(2000);

				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/a[2]/span")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();

				expectedTitle = "TraknPay Users";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
			}
		}

		@Test
		public  void users_namefielderror() throws Exception

		{	


			System.out.println("name field error");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.addusers)).click();
				String expectedTitle = "TraknPay Business User";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);


				String username = ExcelUtils.getCellData(i,11);
				String password = ExcelUtils.getCellData(i,2);
				String retypepassword = ExcelUtils.getCellData(i,3);
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
				String formattedDate = sdf.format(date);
				Email=ExcelUtils.getCellData(i,4)+formattedDate;
				String email =Email;


				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Name may only contain letters, numbers, and dashes."))

				{
					System.out.println("pass:The Name may only contain letters, numbers, and dashes.");

				}
				else
				{


					System.out.println("fail:The Name may only contain letters, numbers, and dashes.");

				}

			}

		}


		@Test
		public  void mismatch_passworderror() throws Exception

		{	


			System.out.println("password mismatch");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");

				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.addusers)).click();
				String expectedTitle = "TraknPay Business User";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);

				String username = ExcelUtils.getCellData(i,1);
				String password = ExcelUtils.getCellData(i,13);
				String retypepassword = ExcelUtils.getCellData(i,14);
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
				String formattedDate = sdf.format(date);
				Email=ExcelUtils.getCellData(i,4)+formattedDate;
				String email =Email;


				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Password confirmation does not match."))

				{
					System.out.println("pass:The Password confirmation does not match.");

				}
				else
				{

					System.out.println("fail:The Password confirmation does not match.");


				}

			}

		}
		@Test
		public void duplicate_email() throws Exception

		{	


			System.out.println("password mismatch");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

				driver.findElement(By.xpath(Xpath.addusers)).click();
				String expectedTitle = "TraknPay Business User";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);

				String username = ExcelUtils.getCellData(i,1);
				String password = ExcelUtils.getCellData(i,2);
				String retypepassword = ExcelUtils.getCellData(i,3);
				String email = ExcelUtils.getCellData(i,4);


				driver.findElement(By.xpath(".//*[@id='buse_name']")).sendKeys(username);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(retypepassword);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(email);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/p")).getText().equalsIgnoreCase("The Email ID has already been taken."))

				{
					System.out.println("pass:The Email ID has already been taken.");

				}
				else
				{
					System.out.println("fail:The Email ID has already been taken.");

				}

			}
		}

			@Test
			public  void users_filters() throws Exception

			{	


				System.out.println("Filters in manage users");
				System.out.println("--------------------------------");
				for(int i=1 ; i<2; i++ ) {
					ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
					System.out.println("loaded: " +i);
					String sUserName = ExcelUtils.getCellData(i,1);
					String sPassword = ExcelUtils.getCellData(i,2);
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
					ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Users,"Sheet1");

					driver.findElement(By.xpath(Xpath.manageusers)).click();
					String expectedTitle = "TraknPay Users";
					String actualTitle = driver.getTitle();
					Assert.assertEquals(expectedTitle,actualTitle);
					Thread.sleep(2000);
					
					//Filter name
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/button")).click();
					WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]/div"));
					String strng1 = element1.getText();
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng1);
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
					Thread.sleep(2000);
					
					//email id
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/button")).click();
					WebElement element2=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]/div"));
					String strng2 = element2.getText();
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng2);
					driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
				}
					
					
		}

		@Test
		public  void users_sessiontimeout() throws Exception 
		{	

			System.out.println("users page session time out");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ )
			{
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
				System.out.println("loaded: " +i);
				String sUserName = ExcelUtils.getCellData(i,1);
				String sPassword = ExcelUtils.getCellData(i,2);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				
				driver.findElement(By.xpath(Xpath.manageusers)).click();
				//start system time
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

				try
				{
					Thread.sleep(16*60*1000);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				date = new Date();
				System.out.println(dateFormat.format(date)); 

	}
		}

	}
