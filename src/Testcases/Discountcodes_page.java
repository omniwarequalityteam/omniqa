package Testcases;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class Discountcodes_page {
	public static WebDriver driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String Name;
	private static String designName;
	private static String CodeName;

	@BeforeTest
	public void beforeTest() 
	{

		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		LogOut_Page.logout(driver);

	}
	@Test
	public void discountcode_add() throws Exception
	{	

		System.out.println("Add discount code");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName;
			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);
			String name = ExcelUtils.getCellData(i,9);
			String phone = ExcelUtils.getCellData(i,10);
			String email = ExcelUtils.getCellData(i,11);
			String amount = ExcelUtils.getCellData(i,12);



			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			//biz logout
			LogOut_Page.logout(driver);

			//Challan url
			driver.get(Constant.challanurl);

			driver.findElement(By.id("invd_name")).sendKeys(name);
			driver.findElement(By.id("invd_phone")).sendKeys(phone);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			driver.findElement(By.name("invd_l[1]")).sendKeys("abc");
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(amount);

			driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/form/div[2]/div[2]/button")).click();

			//Discount code 
			driver.findElement(By.id("discount_code_field")).sendKeys(codename);
			//apply button
			driver.findElement(By.id("apply_button")).click();
			//confirm and pay
			driver.findElement(By.xpath("html/body/div[2]/div[1][3]/a[2]/strong")).click();
			//login to biz
			driver.get(Constant.URL);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		}
	}

	@Test
	public void codename_mandatory() throws Exception
	{	

		System.out.println("code name is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			/*Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; */

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			//driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Discount Code field is required."))

			{
				System.out.println("pass:The Discount Code field is required.");

			}
			else
			{

				System.out.println("fail:The Discount Code field is required.");

			}

		}
	}


	@Test
	public void validfrom_mandatory() throws Exception
	{	

		System.out.println("valid from date is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			driver.findElement(By.id("dcod_valid_from")).clear();
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Valid From Date field is required."))

			{
				System.out.println("pass:The Valid From Date field is required.");

			}
			else
			{

				System.out.println("fail:The Valid From Date field is required.");

			}

		}
	}

	@Test
	public void validto_mandatory() throws Exception
	{	

		System.out.println("valid to date is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Valid To Date field is required."))

			{
				System.out.println("pass:The Valid To Date field is required.");

			}
			else
			{

				System.out.println("fail:The Valid To Date field is required.");

			}

		}
	}

	@Test
	public void activefield_mandatory() throws Exception
	{	

		System.out.println("active field is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Select active or not");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[5]/div/p")).getText().equalsIgnoreCase("The Active or Not field is required."))

			{
				System.out.println("pass:The Active or Not field is required.");

			}
			else
			{

				System.out.println("fail:The Active or Not field is required.");

			}

		}
	}

	@Test
	public void discounttype_mandatory() throws Exception
	{	

		System.out.println("discount type is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Choose Discount Type");
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);

			/*driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit); */

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[6]/div/p")).getText().equalsIgnoreCase("The Discount Type field is required."))

			{
				System.out.println("pass:The Discount Type field is required.");

			}
			else
			{

				System.out.println("fail:The Discount Type field is required.");

			}

		}	
	}
	@Test
	public void discountvalue_mandatory() throws Exception
	{	

		System.out.println("discount value is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			//driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[7]/div/p")).getText().equalsIgnoreCase("The dcod discount value field is required."))

			{
				System.out.println("pass:The dcod discount value field is required.");

			}
			else
			{

				System.out.println("fail:The dcod discount value field is required.");

			}

		}
	}

	@Test
	public void mintransactionamount_mandatory() throws Exception
	{	

		System.out.println("minimum transaction amount is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			//driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[8]/div/p")).getText().equalsIgnoreCase("The Minimum Transaction Amount field is required."))

			{
				System.out.println("pass:The Minimum Transaction Amount field is required.");

			}
			else
			{

				System.out.println("fail:The Minimum Transaction Amount field is required.");

			}

		}
	}

	@Test
	public void maxdiscountamount_mandatory() throws Exception
	{	

		System.out.println("maximum discount amount is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			//driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Maximum Discount Amount field is required."))

			{
				System.out.println("pass:The Maximum Discount Amount field is required.");

			}
			else
			{

				System.out.println("fail:The Maximum Discount Amount field is required.");

			}

		}
	}
	@Test
	public void redemptionslimitperuser_mandatory() throws Exception
	{	

		System.out.println("redemptions limit per user is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			//driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[10]/div/p")).getText().equalsIgnoreCase("The Redemption Limit Per User field is required."))

			{
				System.out.println("pass:The Redemption Limit Per User field is required.");

			}
			else
			{

				System.out.println("fail:The Redemption Limit Per User field is required.");

			}

		}
	}

	@Test
	public void totalcode_redemptionslimit_mandatory() throws Exception
	{	

		System.out.println("total code redemptions limit is mandatory");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			//driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[11]/div/p")).getText().equalsIgnoreCase("The Total Code Redemption Limit field is required."))

			{
				System.out.println("pass:The Total Code Redemption Limit field is required.");

			}
			else
			{

				System.out.println("fail:The Total Code Redemption Limit field is required.");

			}

		}
	}

	@Test
	public void edit_discountcode() throws Exception
	{	

		System.out.println("edit discount code");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//edit button 
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[1]")).click();

			expectedTitle = "TraknPay Edit Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Load discount code excel sheet
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");
			String editdiscountvalue = ExcelUtils.getCellData(i,13);

			driver.findElement(By.id("dcod_discount_value")).clear();
			driver.findElement(By.id("dcod_discount_value")).sendKeys(editdiscountvalue);


			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();


		}
	}

	@Test
	public void duplicate_codename() throws Exception
	{	

		System.out.println("Already exist Dicount code");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Copy already existed discount code name
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
			String strng = element.getText();
			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(strng);   
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Discount Code has already been taken."))

			{
				System.out.println("pass:The Discount Code has already been taken.");

			}
			else
			{

				System.out.println("fail:The Discount Code has already been taken.");

			}

		}
	}
	@Test
	public void validtobeforevalidfrom_validation() throws Exception
	{	

		System.out.println("valid to date must be a date after valid from date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validtobeforevalidfrom = ExcelUtils.getCellData(i,14);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validtobeforevalidfrom);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Valid To Date must be a date after Valid From Date."))

			{
				System.out.println("pass:The Valid To Date must be a date after Valid From Date.");

			}
			else
			{

				System.out.println("fail:The Valid To Date must be a date after Valid From Date.");

			}

		}
	}
	@Test
	public void discountcode_Notactive() throws Exception
	{	

		System.out.println("discount code is not in active mode");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName;
			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);
			String name = ExcelUtils.getCellData(i,9);
			String phone = ExcelUtils.getCellData(i,10);
			String email = ExcelUtils.getCellData(i,11);
			String amount = ExcelUtils.getCellData(i,12);



			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("No");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			//biz logout
			LogOut_Page.logout(driver);

			//Challan url
			driver.get(Constant.challanurl);

			driver.findElement(By.id("invd_name")).sendKeys(name);
			driver.findElement(By.id("invd_phone")).sendKeys(phone);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			driver.findElement(By.name("invd_l[1]")).sendKeys("abc");
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(amount);

			driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/form/div[2]/div[2]/button")).click();

			//Discount code 
			driver.findElement(By.id("discount_code_field")).sendKeys(codename);
			//apply button
			driver.findElement(By.id("apply_button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/div/table/tbody/tr[7]/td/span")).getText().equalsIgnoreCase("Sorry, Discount code is not active"))

			{
				System.out.println("pass:Sorry, Discount code is not active");

			}
			else
			{

				System.out.println("fail:Sorry, Discount code is not active");

			}
		}
		//login to biz
		driver.get(Constant.URL);
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		}
	}

	@Test
	public void discountcode_postvaliddate() throws Exception
	{	

		System.out.println("discount code is valid on valid from date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName;
			String postvalidfrom= ExcelUtils.getCellData(i,15);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);
			String name = ExcelUtils.getCellData(i,9);
			String phone = ExcelUtils.getCellData(i,10);
			String email = ExcelUtils.getCellData(i,11);
			String amount = ExcelUtils.getCellData(i,12);

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			driver.findElement(By.id("dcod_valid_from")).clear();
			driver.findElement(By.id("dcod_valid_from")).sendKeys(postvalidfrom);

			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("No");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			//biz logout
			LogOut_Page.logout(driver);

			//Challan url
			driver.get(Constant.challanurl);

			driver.findElement(By.id("invd_name")).sendKeys(name);
			driver.findElement(By.id("invd_phone")).sendKeys(phone);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			driver.findElement(By.name("invd_l[1]")).sendKeys("abc");
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(amount);

			driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/form/div[2]/div[2]/button")).click();

			//Discount code 
			driver.findElement(By.id("discount_code_field")).sendKeys(codename);
			//apply button
			driver.findElement(By.id("apply_button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/div/table/tbody/tr[7]/td/span")).getText().equalsIgnoreCase("Sorry, Discount code is not active"))

			{
				System.out.println("pass:Sorry, Discount code is not active");

			}
			else
			{

				System.out.println("fail:Sorry, Discount code is not active");

			}
		}
		//login to biz
		driver.get(Constant.URL);
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		}
	}
	@Test
	public void discountcode_deletebutton() throws Exception
	{	

		System.out.println("delete discount code");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName;
			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);



			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[9]/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[6]/div[7]/div/button")).click();

		}
	}
	@Test
	public void lessthanminimum_transactionamount() throws Exception
	{	

		System.out.println("try to make challan payment with less than minimum transaction amount");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName;
			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);
			String name = ExcelUtils.getCellData(i,9);
			String phone = ExcelUtils.getCellData(i,10);
			String email = ExcelUtils.getCellData(i,11);
			String lessthanmintransactionamount = ExcelUtils.getCellData(i,16);



			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(Keys.RETURN);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);
			//driver.findElement(By.id("dcod_valid_to")).sendKeys(Keys.RETURN);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Discount by Percent");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(discountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(minimumtransactionamount);
			driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);

			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			//biz logout
			LogOut_Page.logout(driver);

			//Challan url
			driver.get(Constant.challanurl);

			driver.findElement(By.id("invd_name")).sendKeys(name);
			driver.findElement(By.id("invd_phone")).sendKeys(phone);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			driver.findElement(By.name("invd_l[1]")).sendKeys("abc");
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(lessthanmintransactionamount);

			driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/form/div[2]/div[2]/button")).click();

			//Discount code 
			driver.findElement(By.id("discount_code_field")).sendKeys(codename);
			//apply button
			driver.findElement(By.id("apply_button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[1][2]/div[2]/div/table/tbody/tr[7]/td/span")).getText().equalsIgnoreCase("Sorry, Minimum transaction amount to apply this discount must be atleast 50.00"))

			{
				System.out.println("pass:Sorry, Minimum transaction amount to apply this discount must be atleast 50.00");

			}
			else
			{

				System.out.println("fail:Sorry, Minimum transaction amount to apply this discount must be atleast 50.00");

			}
		}
		//login to biz
		driver.get(Constant.URL);
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		}

	}
	@Test
	public void discountcode_filters() throws Exception
	{	

		System.out.println("filters of discount codes");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			//ID
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			
			//Discount code
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			
			//Status
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Active");
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/button")).click();
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText("Not Active");
			Thread.sleep(2000);
			
			
		}	
		}
	
	@Test
	public void flatdiscount_lessthan_mintransactionamount() throws Exception
	{	

		System.out.println("Flat discount value less than minimum transaction amount");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			//click on discount codes
			driver.findElement(By.xpath(Xpath.discountcodes)).click();

			String expectedTitle = "TraknPay Discount Code";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			//Add button
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/div/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay Create Discount Code";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_DiscountCodes,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			CodeName=ExcelUtils.getCellData(i,1)+formattedDate;
			String codename = CodeName; 

			//String validfrom = ExcelUtils.getCellData(i,2);
			String validto = ExcelUtils.getCellData(i,3);
			String discountvalue = ExcelUtils.getCellData(i,4);
			String minimumtransactionamount = ExcelUtils.getCellData(i,5);
			String maximumdiscountamount = ExcelUtils.getCellData(i,6);
			String redemptionlimitperuser = ExcelUtils.getCellData(i,7);
			String totalcoderedemptionslimit = ExcelUtils.getCellData(i,8);
			String flatdiscountvalue = ExcelUtils.getCellData(i,17);
			String mintransactionforflatdiscount = ExcelUtils.getCellData(i,18);
			

			driver.findElement(By.id("dcod_discount_code")).sendKeys(codename);   
			//driver.findElement(By.id("dcod_valid_from")).sendKeys(validfrom);
			driver.findElement(By.id("dcod_valid_to")).sendKeys(validto);

			WebElement mySelectElm = driver.findElement(By.id("dcod_active")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yes");

			WebElement mySelectElm1 = driver.findElement(By.id("discount_type")); 
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Flat Discount Amount");

			driver.findElement(By.id("dcod_discount_value")).sendKeys(flatdiscountvalue);
			driver.findElement(By.id("min_transaction_amount")).sendKeys(mintransactionforflatdiscount);
			//driver.findElement(By.id("max_discount_amount")).sendKeys(maximumdiscountamount);
			driver.findElement(By.id("redemptions_limit_per_user")).sendKeys(redemptionlimitperuser);
			driver.findElement(By.id("total_redemptions_limit")).sendKeys(totalcoderedemptionslimit);
			driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1][1]/section[2]/div/form/div[1]/div[8]/div/p")).getText().equalsIgnoreCase("The Minimum Transaction Amount must be at least 51."))

			{
				System.out.println("pass:The Minimum Transaction Amount must be at least 51.");

			}
			else
			{

				System.out.println("fail:The Minimum Transaction Amount must be at least 51.");

			}

		}
	}
}