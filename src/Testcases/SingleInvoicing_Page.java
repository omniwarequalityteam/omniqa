package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;


import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import java.io.File; 
import org.apache.commons.io.FileUtils; 
import org.openqa.selenium.OutputType; 
import org.openqa.selenium.TakesScreenshot;


public class SingleInvoicing_Page {


	public static WebDriver driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String Name;
	private static String designName;

	@BeforeTest
	public void beforeTest() 
	{
		
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}
	@Test
	public  void createdesign_singleinvoice_send() throws Exception
	{	

		System.out.println("createdesign_create to single invoice");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");

			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			//String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			//String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);


			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			// driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			//driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");



			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			expectedTitle = "TraknPay Create Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf.format(date);
			Name=ExcelUtils.getCellData(i,2)+formattedDate;
			String name =Name; 

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);

			driver.findElement(By.name("invd_name")).sendKeys(name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			String winHandleBefore = driver.getWindowHandle();
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[1]")).click();
			Thread.sleep(3000);

			for(String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
			driver.close();
			driver.switchTo().window(winHandleBefore);



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}

	}

	@Test
	public void singleInvoicing_edit() throws Exception
	{

		System.out.println("single invoicing edit");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name; 

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String fanedit = ExcelUtils.getCellData(1,14);

			driver.findElement(By.name("invd_name")).sendKeys(name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			//edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[3]")).click();

			Fans= ExcelUtils.getCellData(i,9);
			driver.findElement(By.name("invd_f5")).clear();
			driver.findElement(By.name("invd_f5")).sendKeys(fanedit);
			System.out.println("fan edit: " +fanedit);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);



		}
	}

	@Test
	public void singleInvoicing_pdfpage() throws Exception
	{

		System.out.println("single invoicing pdf page");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);



			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name; 


			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String fanedit = ExcelUtils.getCellData(1,14);

			
			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[1]/span")).click();

			String winHandleBefore = driver.getWindowHandle();

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[1]")).click();

			Thread.sleep(3000);

			// the set will contain only the child window now. Switch to child window and close it.
			for(String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			driver.close();
			driver.switchTo().window(winHandleBefore);


		}

	}


	@Test
	public void singleInvoicing_addItems() throws Exception
	{

		System.out.println("sinle invoice add items");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String item2 = ExcelUtils.getCellData(i,15);
			String largesize = ExcelUtils.getCellData(i,16);
			String item3= ExcelUtils.getCellData(i,28);
			String item4= ExcelUtils.getCellData(i,29);
			String item5 = ExcelUtils.getCellData(i,30);
			String fancost = ExcelUtils.getCellData(i,31);
			String accost = ExcelUtils.getCellData(i,32);
			String bulbcost = ExcelUtils.getCellData(i,33);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(item2);
			driver.findElement(By.name("invd_l_value[2]")).sendKeys(largesize);
			Thread.sleep(2000);



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[3]")).sendKeys(item3);
			driver.findElement(By.name("invd_l_value[3]")).sendKeys(fancost);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[4]")).sendKeys(item4);
			driver.findElement(By.name("invd_l_value[4]")).sendKeys(accost);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[5]")).sendKeys(item5);
			driver.findElement(By.name("invd_l_value[5]")).sendKeys(bulbcost);

			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}

	}


	@Test
	public void singleInvoicing_sendonpreviousdate() throws Exception
	{

		System.out.println("single invoice send on previous date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String sendondate= ExcelUtils.getCellData(i,17);

			driver.findElement(By.name("invd_name")).sendKeys(Name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);


			driver.findElement(By.name("invd_send_on")).clear();
			driver.findElement(By.name("invd_send_on")).sendKeys(sendondate);
			System.out.println("send on date: " +sendondate);


			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Send On Date must be a date after yesterday."))

			{
				System.out.println("pass:The Send On Date must be a date after yesterday");


			}
			else
			{

				System.out.println("fail:The Send On Date must be a date after yesterday");
			}
		}


	}

	@Test
	public void singleInvoicing_sendoncurrentdate() throws Exception
	{
		System.out.println("single invoice send on current date");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String sendoncurrentdate= ExcelUtils.getCellData(i,18);

			driver.findElement(By.name("invd_name")).sendKeys(Name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_send_on")).clear();
			driver.findElement(By.name("invd_send_on")).sendKeys(sendoncurrentdate);
			System.out.println("send on date: " +sendoncurrentdate);
			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(10000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
			
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}
	@Test
	public void singleInvoicing_mandatorynamefield() throws Exception
	{

		System.out.println("single invoice name mandatory field");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);


			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li[1]")).getText().equalsIgnoreCase("The Name field is required."))

			{
				System.out.println("pass:Name field is required");

			}
			else
			{

				System.out.println("fail:Name field is required");

			}

		}


	}

	@Test	   	    
	public void singleInvoicing_mandatoryamountfield() throws Exception
	{

		System.out.println("single invoice amount mandatory field");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name; 

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			//String mediumsize = ExcelUtils.getCellData(i,13);


			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			Thread.sleep(5000);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			//driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li[2]")).getText().equalsIgnoreCase("The Item 1 Amount field is required."))

			{
				System.out.println("pass:Item amount field is required");

			}
			else
			{

				System.out.println("fail:Item amount field is required");


			}

		}

	}
	@Test
	public void singleInvoicing_mandatoryphonefield() throws Exception
	{

		System.out.println("single invoice phone mandatory field");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);



			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			Thread.sleep(5000);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			//driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Phone field is required."))

			{
				System.out.println("pass: phone field is required");

			}
			else
			{

				System.out.println("fail: phone field is required");

			}

		}

	}
	@Test
	public void singleInvoicing_Businessprofilename() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_businessprofilename");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//business  name
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[1]"));
			String strng = element.getText();

			System.out.println("business name: "+strng);
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a/img")).click();
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();

			WebElement element1=driver.findElement(By.id("bude_name"));
			String strng1 = element1.getAttribute("value");
			System.out.println("business name: "+strng1);
			
			if(driver.findElement(By.id("bude_name")).getAttribute("value").equalsIgnoreCase(strng))
			{
				System.out.println("Pass:Business name is matched");

			}
			else
			{

				System.out.println("fail:Business name is mismatched");

			}

		}



	}
	@Test
	public void singleInvoicing_Businessprofileadress() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_businessprofileadress");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//business address in single invoice
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[2]"));
			String strng = element.getText();

			System.out.println("business adress: "+strng);
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a/img")).click();
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();

			//business adress in profile
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input"));
			String strng1 = element1.getText();
			System.out.println("business adress in profile: "+strng1);

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[2]/input")).getText().equalsIgnoreCase(strng1))
			{
				System.out.println("Pass:Business adress is matched");

			}
			else
			{

				System.out.println("fail:Business adress is mismatched");

			}


		}

	}
	@Test
	public void singleInvoicing_Businessprofilecity() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_businessprofilecity");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");


			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[4]"));

			String strng = element.getText();
			System.out.println("business city: "+strng);
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a/img")).click();
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input"));
			Thread.sleep(3000);
			String strng1 = element1.getAttribute("value");
			System.out.println("business city: "+strng1);
			Thread.sleep(3000);
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[2]/div/form/div/ul/li[1]/div/div[1]/div[4]/input")).getAttribute("value").equalsIgnoreCase(strng))


			{
				System.out.println("Pass:Business city is matched");

			}
			else
			{

				System.out.println("fail:Business city is mismatched");

			}

		}



	}
	@Test
	public void singleInvoicing_Businessprofilephone() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_businessprofilephoneno");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[6]"));

			String strng = element.getText();

			//substring
			strng = strng.replace("Tel: +91 ","");
			System.out.println("business no: "+strng);
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a/img")).click();
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
			WebElement element1=driver.findElement(By.id("bude_business_contactnum"));

			String strng1 = element1.getAttribute("value");
			System.out.println("business no: "+strng1);
			if(driver.findElement(By.id("bude_business_contactnum")).getAttribute("value").equalsIgnoreCase(strng))
			{
				System.out.println("Pass:Business number is matched");

			}
			else
			{

				System.out.println("fail:Business number is mismatched");

			}


		}



	}

	@Test
	public void singleInvoicing_Businessprofilepincode() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_businessprofilepincode");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[5]"));

			String strng = element.getText();

			//substring
			strng = strng.replace("Karnataka ","");
			System.out.println("business pincode: "+strng);
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/a/img")).click();
			driver.findElement(By.xpath("html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a")).click();

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/ul/li[2]/div/div/div[1]/div/button")).click();
			WebElement element1=driver.findElement(By.id("bude_zipcode"));

			String strng1 = element1.getAttribute("value");
			System.out.println("business pincode: "+strng1);
			if(driver.findElement(By.id("bude_zipcode")).getAttribute("value").equalsIgnoreCase(strng))
			{
				System.out.println("Pass:Business pincode is matched");

			}
			else
			{

				System.out.println("fail:Business pincode is mismatched");

			}

		}


	}  

	@Test
	public void singleInvoicing_backbutton() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("singleInvoicing_backbuton");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/a")).click();
			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");

		}

	}

	@Test
	public void singleInvoicing_addcustomer() throws Exception
	{

		System.out.println("singe invoicing add customer");
		System.out.println("---------------------------------------------------------");

		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,23);
			
				//Define implict Wait
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
				System.out.println("Design name: " +Designname);
				System.out.println("design name created successfully");
				// driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
				driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
				driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
				driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
				//driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
				driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
				driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
				driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
				driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
				driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);

				driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
				driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
				driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
				Thread.sleep(7000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
				expectedTitle = "TraknPay Show Invoice Design";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				System.out.println("Traknpay show invoice design page");
				Thread.sleep(9000);

			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			 expectedTitle = "TraknPay Create Invoice";
			 actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div/div/div/a/span/i[3]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[10]/div[7]/div/button")).click();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_AddCustomer_1s,"Sheet1");

			// create customer

			expectedTitle = "TraknPay Business Customers";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Business Customers");


			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMmmss");
			String formattedDate1 = sdf1.format(date1);
			String CustomerName = ExcelUtils.getCellData(i,1)+formattedDate1;


			String CustomerEmail = ExcelUtils.getCellData(i,2);

			Date date11 = new Date();
			SimpleDateFormat sdf11 = new SimpleDateFormat("MMmmss");
			String formattedDate11 = sdf11.format(date11);
			String CustomerMobile = ExcelUtils.getCellData(i,3)+formattedDate11;

			String CustomerAddressLine1 = ExcelUtils.getCellData(i,4);

			String CustomerAddressLine2 = ExcelUtils.getCellData(i,5);
			String CustomerCity = ExcelUtils.getCellData(i,6);

			String CustomerState = ExcelUtils.getCellData(i,7);
			String Customercountry = ExcelUtils.getCellData(i,8);

			String CustomerZipCode = ExcelUtils.getCellData(i,9);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElement(By.xpath(".//*[@id='bcus_name']")).sendKeys(CustomerName);
			// System.out.println("Design name: " +Designname);
			// System.out.println("design name created successfully");
			driver.findElement(By.name("bcus_email")).sendKeys(CustomerEmail);
			driver.findElement(By.name("bcus_phone")).sendKeys(CustomerMobile);
			Thread.sleep(5000);
			driver.findElement(By.name("bcus_address_line_1")).sendKeys(CustomerAddressLine1);

			driver.findElement(By.name("bcus_address_line_2")).sendKeys(CustomerAddressLine2);
			driver.findElement(By.name("bcus_city")).sendKeys(CustomerCity);

			driver.findElement(By.name("bcus_state")).sendKeys(CustomerState);
			driver.findElement(By.name("bcus_country")).sendKeys(Customercountry);

			driver.findElement(By.name("bcus_zipcode")).sendKeys(CustomerZipCode);

			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]/div"));
			String strng = element.getText();
			System.out.println(strng);

			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[3]/a")).click();

			driver.findElement(By.id("autocomplete-ajax")).sendKeys(strng);
			//driver.findElement(By.name("bcus_search")).sendKeys(Name);
			Thread.sleep(2000);
			List <WebElement> listItems = driver.findElements(By.xpath("html/body/div[8]/div"));

			listItems.get(0).click();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");

			String Itemname3 = ExcelUtils.getCellData(i,13);
			String tax = ExcelUtils.getCellData(i,43);
			String servicetax1 = ExcelUtils.getCellData(i,44);

			Thread.sleep(3000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Itemname3);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_f18")).sendKeys(tax);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_f19")).sendKeys(servicetax1);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}
	}

	@Test
	public void singleInvoicing_addItemsandremove() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("singleInvoicing_addItems and remove");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);




			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);

			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String item2 = ExcelUtils.getCellData(i,15);
			String largesize = ExcelUtils.getCellData(i,16);
			String item3= ExcelUtils.getCellData(i,28);
			String item4= ExcelUtils.getCellData(i,29);
			String item5 = ExcelUtils.getCellData(i,30);
			String fancost = ExcelUtils.getCellData(i,31);
			String accost = ExcelUtils.getCellData(i,32);
			String bulbcost = ExcelUtils.getCellData(i,33);


			driver.findElement(By.name("invd_name")).sendKeys(name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			//html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span
			driver.findElement(By.name("invd_l[2]")).sendKeys(item2);
			driver.findElement(By.name("invd_l_value[2]")).sendKeys(largesize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[3]")).sendKeys(item3);
			driver.findElement(By.name("invd_l_value[3]")).sendKeys(fancost);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[4]")).sendKeys(item4);
			driver.findElement(By.name("invd_l_value[4]")).sendKeys(accost);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[1]/th[1]/a/span")).click();
			driver.findElement(By.name("invd_l[5]")).sendKeys(item5);
			driver.findElement(By.name("invd_l_value[5]")).sendKeys(bulbcost);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[2]/table/tbody/tr[2]/td[1]/a/span")).click();



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();

			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}

	}



	@Test
	public void singleInvoicing_vendornameerror() throws Exception
	{

		System.out.println("vendor name error");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);



			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String erroramount = ExcelUtils.getCellData(i,34);
			String sendondate= ExcelUtils.getCellData(i,18);


			driver.findElement(By.xpath(".//*[@id='invd_split_pay_flag']")).click();


			driver.findElement(By.name("invd_name")).sendKeys(Name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);


			driver.findElement(By.name("invd_send_on")).clear();
			driver.findElement(By.name("invd_send_on")).sendKeys(sendondate);
			System.out.println("send on date: " +sendondate);


			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(erroramount);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Vendor field is required when Split Pay is present."))

			{
				System.out.println(" pass:The Vendor field is required when Split Pay is present.");


			}
			else
			{
				System.out.println(" fail:The Vendor field is required when Split Pay is present.");


			}
		}

	}

	@Test
	public void manageinvoice_addnewinvoice() throws Exception
	{
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("addnewinvoice in manage invoices");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_ManageInvoicing,"Sheet1");
			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[5]/a/span")).click();
			String expectedTitle = "TraknPay List Invoices";
			String   actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[1]/div/div/div[1]/div/a")).click();
			expectedTitle = "TraknPay Create Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			String designname = ExcelUtils.getCellData(i,1);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,2)+formattedDate;
			String name =Name;
			// String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);


			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(2000);


			driver.findElement(By.className("confirm")).click();


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}


	}
	@Test
	public void singleInvoicing_splitpayment() throws Exception
	{

		System.out.println("single invoice split payment");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			driver.findElement(By.xpath(Xpath.managevendors)).click();
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Active");
			Thread.sleep(3000); 


			WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/select"));
			Select mySelect1= new Select(mySelectElm1);
			mySelect1.selectByVisibleText("Approved");
			Thread.sleep(3000); 

			WebElement element11111=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[2]/div"));
			String strng11111 = element11111.getText();

			Thread.sleep(2000);

			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);

			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String splitpercentage = ExcelUtils.getCellData(i,36);


			driver.findElement(By.xpath(".//*[@id='invd_split_pay_flag']")).click();

			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div[2]/div[1]/div/div/div/select"));
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByVisibleText(strng11111);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div[2]/div[2]/div/div/div[1]/input")).sendKeys(splitpercentage);
			Thread.sleep(5000);



			driver.findElement(By.name("invd_name")).sendKeys(name);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(9000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(9000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

		}

	}	   

	@Test
	public void singleInvoicing_splitpayment_name_error() throws Exception
	{

		System.out.println("single invoice splitpayment name error");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);

			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String splitpercentage = ExcelUtils.getCellData(i,36);
			String vendornameerror = ExcelUtils.getCellData(i,38);



			driver.findElement(By.xpath(".//*[@id='invd_split_pay_flag']")).click();

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div[2]/div[2]/div/div/div[1]/input")).sendKeys(splitpercentage);
			Thread.sleep(5000);



			driver.findElement(By.name("invd_name")).sendKeys(name);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Vendor field is required when Split Pay is present."))

			{
				System.out.println("pass:vendor field is required when split payment is present");


			}
			else
			{
				System.out.println("fail:vendor field is required when split payment is present");


			}

		}


	}




	@Test
	public  void singleinvoice_Quantity_price() throws Exception

	{	

		driver.get(Constant.URL);
		System.out.println("singleinvoice quantity-price");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			//create single invoice
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			Thread.sleep(2000);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_SingleInvoicingData,"Sheet1");
			System.out.println("single invoicing excel sheet loaded");
			//String designname = ExcelUtils.getCellData(i,1);
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,2)+formattedDate1;
			String name =Name;

			//String name = ExcelUtils.getCellData(i,2);
			String Adress1 = ExcelUtils.getCellData(i,3);
			String Adress2 = ExcelUtils.getCellData(i,4);
			String Adress3 = ExcelUtils.getCellData(i,5);
			String Pincode = ExcelUtils.getCellData(i,6);
			String Phone = ExcelUtils.getCellData(i,7);
			String Email = ExcelUtils.getCellData(i,8);
			String Fans = ExcelUtils.getCellData(i,9);
			String Ac = ExcelUtils.getCellData(i,10);
			String Bulbs = ExcelUtils.getCellData(i,11);
			String Tv = ExcelUtils.getCellData(i,12);
			String mediumsize = ExcelUtils.getCellData(i,13);
			String quantity = ExcelUtils.getCellData(i,40);
			String unitprice = ExcelUtils.getCellData(i,41);


			driver.findElement(By.name("invd_name")).sendKeys(name);

			System.out.println("customer name: " +name);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_f1")).sendKeys(Adress1);
			driver.findElement(By.name("invd_f2")).sendKeys(Adress2);
			driver.findElement(By.name("invd_f3")).sendKeys(Adress3);
			driver.findElement(By.name("invd_f4")).sendKeys(Pincode);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fans);
			driver.findElement(By.name("invd_f6")).sendKeys(Ac);
			driver.findElement(By.name("invd_f7")).sendKeys(Bulbs);
			driver.findElement(By.name("invd_f8")).sendKeys(Tv);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[1]/div/a/span")).click();
			//html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[4]/div[1]/div/a/span
			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='toggle_quantity_price_column']")).click();
			driver.findElement(By.name("invd_q_value[1]")).sendKeys(quantity);
			driver.findElement(By.name("invd_p_value[1]")).sendKeys(unitprice);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);

			String winHandleBefore = driver.getWindowHandle();
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[1]")).click();
			Thread.sleep(3000);

			// the set will contain only the child window now. Switch to child window and close it.
			for(String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
			driver.close();
			driver.switchTo().window(winHandleBefore);



			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);


		}
	}
	@Test
	public void singleInvoicing_vendordefaultaccount() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("singleInvoicing vendor default account is same as business name");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			// copy business  name
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/div[1]/div/div/div[1]/div[2]/div[1]"));
			String strng = element.getText();
			System.out.println("business name: "+strng);
			
			//
			WebElement element1=driver.findElement(By.id("bbac_account_name"));

			String strng1 = element1.getAttribute("value");
			System.out.println("Default account: "+strng1);
			if(driver.findElement(By.id("bbac_account_name")).getAttribute("value").equalsIgnoreCase(strng))
			{
				System.out.println("Pass:Business name is matched with default account");

			}
			else
			{

				System.out.println("fail:Business name is mismatched with default account");

			}

		}
	}
	@Test
	public void singleInvoicing_addvendorinsingleinvoice() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("Add vendor in single invoicing page");
			System.out.println("--------------------------------");

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();

			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/div/div/div/input")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div[2]/div[1]/div/div/div/a/span/i[3]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("html/body/div[10]/div[7]/div/button")).click();
			Thread.sleep(1000);
			//Vendor page
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 

			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(vendorname);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(vendoremail);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(vendorphone);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(vendoradress);


				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(Bankaccountno);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(Accounthholdername);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(BankIFSC);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[8]/div/input")).sendKeys(Bankname);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
			
		}
	}

			

			

	@Test
	public  void singleinvoice_sessiontimeout() throws Exception 
	{	

		System.out.println("single invoice session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
			
			

}
	}
}