package Testcases;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class paymentform {
	
	public static WebDriver	driver = new FirefoxDriver();

	private static WebElement element = null;
	private static String designName;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	@Test 
	public  void paymentform_Filters() throws Exception  
	{	
		System.out.println("--------------------------------");
		System.out.println("paymentform_Filters Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		System.out.println("loaded: " +i);
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
		driver.findElement(By.xpath(Xpath.paymentform)).click();
			
		String expectedTitle="TraknPay Payment Forms";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle,actualTitle);
		System.out.println("Traknpay Payment Forms");
	
		
		//Define implict Wait           
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		Thread.sleep(2000);	
		
		//Payment Form Id
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/button")).click();
		WebElement element =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
		String strng= element.getText();                 
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
		Thread.sleep(3000);	 

		//Payment Form Name
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/button")).click();
		WebElement element1 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]/div"));
		String strng1= element1.getText();                 
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
		Thread.sleep(3000);	 

		//Payment Form Description
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/button")).click();
		WebElement element2 =driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/div"));
		String strng2= element2.getText();
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng2);
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
		Thread.sleep(3000);

		//phone 
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/button")).click();
		}
	}

	
	@Test 
	public  void paymentform_show_entries() throws Exception  
	{	
		System.out.println("--------------------------------");
		System.out.println("paymentform_show_entries Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		System.out.println("loaded: " +i);
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
		driver.findElement(By.xpath(Xpath.paymentform)).click();
			
		String expectedTitle="TraknPay Payment Forms";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle,actualTitle);
		System.out.println("Traknpay Payment Forms");
	

		//dropdown 
		WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect= new Select(mySelectElm);
		mySelect.selectByVisibleText("10");
		Thread.sleep(4000);

		//dropdown
		WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect2= new Select(mySelectElm);
		mySelect.selectByVisibleText("25");
		Thread.sleep(4000);

		//dropdown 
		WebElement mySelectElm3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect3= new Select(mySelectElm);
		mySelect.selectByVisibleText("50");
		Thread.sleep(4000);

		//dropdown 
		WebElement mySelectElm4 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect4= new Select(mySelectElm);
		mySelect.selectByVisibleText("100");
		Thread.sleep(4000);
		
		//dropdown 
		WebElement mySelectElm5 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect5= new Select(mySelectElm);
		mySelect.selectByVisibleText("200");
		Thread.sleep(4000);
		
		//dropdown 
		WebElement mySelectElm6 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
		Select mySelect6= new Select(mySelectElm);
		mySelect.selectByVisibleText("All");
		Thread.sleep(4000);
		}
	}

	@Test 
	public  void paymentform_delete() throws Exception  
	{	
		System.out.println("------------------------");
		System.out.println("paymentform_delete Start");
		System.out.println("------------------------");
		for(int i=1 ; i<2; i++ ) {
			
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		System.out.println("loaded: " +i);
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
		driver.findElement(By.xpath(Xpath.paymentform)).click();
			
		String expectedTitle="TraknPay Payment Forms";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle,actualTitle);
		System.out.println("Traknpay Payment Forms");
	
		//add
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		Thread.sleep(2000);
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
		String formattedDate = sdf.format(date);
		designName=ExcelUtils.getCellData(i,1)+formattedDate;
		String Designname = designName;
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
		System.out.println("Design name: " +Designname);
		System.out.println("design name created successfully");
		Thread.sleep(4000);
		
		//update
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
		Thread.sleep(1000);
		//delete
		driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]/a[2]/span")).click();
		// yes proceed
		driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
		Thread.sleep(4000);
		}
	}
	
		@Test
		public  void paymentform_previous_next() throws Exception  
		{	
			System.out.println("-------------------------------");
			System.out.println("paymentform_previous_next Start");
			System.out.println("-------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
				
				//2
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/ul/li[3]/a")).click();
				Thread.sleep(3000);
				// 3
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/ul/li[4]/a")).click();
				Thread.sleep(3000);

				//previous
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[2]/div/ul/li[1]/a")).click();
				Thread.sleep(4000);
      			}
			}
	

		@Test 
		public  void paymentform_add() throws Exception  
		{	
			System.out.println("--------------------------------");
			System.out.println("paymentform_add Start");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(3000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(place_holder);
			Thread.sleep(3000);
		   
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(3000);

			//Name email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(3000);
			
			//Name Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(3000);
			
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
				}
			}
		
		@Test 
		public  void paymentform_add_All_labels() throws Exception  
		{	
			System.out.println("--------------------------------");
			System.out.println("paymentform_add_All_labels Start");
			System.out.println("--------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//checkbox
			System.out.println("Add check box");
			//add check box
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[1]")).click();
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[6]/div[1]/a[1]")).click();
		
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[6]/div[3]/div/div[3]/div/input")).clear();
			String checkbox = ExcelUtils.getCellData(i,13);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[6]/div[3]/div/div[3]/div/input")).sendKeys(checkbox);
			Thread.sleep(3000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[6]/div[3]/div/a")).click();
		
		   
			System.out.println("Add Number");
			//add number
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[2]/span")).click();
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[7]/div[1]/a[1]")).click();
		                                
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[7]/div[3]/div/div[2]/div/input")).clear();
			String Number = ExcelUtils.getCellData(i,14);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[7]/div[3]/div/div[2]/div/input")).sendKeys(Number);
			Thread.sleep(2000);
			
			// close
			 driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[7]/div[3]/div/a")).click();
				 
			 System.out.println("Add Select");
			//add select
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[3]")).click();
			Thread.sleep(2000);
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[1]/a[1]")).click();
			                            
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[2]/div/input")).clear();
			String select = ExcelUtils.getCellData(i,15);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[2]/div/input")).sendKeys(select);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[4]/div/input")).clear();
			String select_placeholder = ExcelUtils.getCellData(i,16);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[4]/div/input")).sendKeys(select_placeholder);
			Thread.sleep(3000);
							
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[1]/input[2]")).clear();
			String option1 = ExcelUtils.getCellData(i,17);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[1]/input[2]")).sendKeys(option1);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[1]/input[3]")).clear();
			String option1v = ExcelUtils.getCellData(i,17);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[1]/input[3]")).sendKeys(option1v);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[2]/input[2]")).clear();
			String option2 = ExcelUtils.getCellData(i,18);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[2]/input[2]")).sendKeys(option2);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[2]/input[3]")).clear();
			String option2v = ExcelUtils.getCellData(i,18);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[2]/input[3]")).sendKeys(option2v);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[3]/input[2]")).clear();
			String option3 = ExcelUtils.getCellData(i,19);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[3]/input[2]")).sendKeys(option3);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[3]/input[3]")).clear();
			String option3v = ExcelUtils.getCellData(i,19);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[3]/input[3]")).sendKeys(option3v);
			Thread.sleep(3000);
			//add option 
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/div/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[4]/input[2]")).clear();
			String select_label = ExcelUtils.getCellData(i,20);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[4]/input[2]")).sendKeys(select_label);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[4]/input[3]")).clear();
			String select_value= ExcelUtils.getCellData(i,20);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/div[9]/div/ol/li[4]/input[3]")).sendKeys(select_value);
			Thread.sleep(2000);
		
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[8]/div[3]/div/a")).click();
			Thread.sleep(3000);

			System.out.println("Add Text Field");
			//add Text Field
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[4]")).click();
			Thread.sleep(3000);
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[1]/a[1]")).click();
			Thread.sleep(2000);         
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[2]/div/input")).clear();
			String textfield = ExcelUtils.getCellData(i,21);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[2]/div/input")).sendKeys(textfield);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[5]/div/input")).clear();
			String tf_placeholder = ExcelUtils.getCellData(i,21);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[5]/div/input")).sendKeys(tf_placeholder);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[10]/div/input")).clear();
			String MaxLeng = ExcelUtils.getCellData(i,22);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/div[10]/div/input")).sendKeys(MaxLeng);
			Thread.sleep(3000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[9]/div[3]/div/a")).click();
		
			System.out.println("Add Text Area");	
			//add Text Area
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[5]")).click();
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[1]/a[1]")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[2]/div/input")).clear();
			String TextArea = ExcelUtils.getCellData(i,23);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[2]/div/input")).sendKeys(TextArea);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[4]/div/input")).clear();
			String TextArea_p = ExcelUtils.getCellData(i,23);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[4]/div/input")).sendKeys(TextArea_p);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[5]/div/input")).clear();
			String Textarea_row = ExcelUtils.getCellData(i,24);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[5]/div/input")).sendKeys( Textarea_row );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[10]/div/input")).clear();
			String Textarea_max = ExcelUtils.getCellData(i,22);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/div[10]/div/input")).sendKeys(Textarea_max);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[10]/div[3]/div/a")).click();
	
			System.out.println("Add address ");	
			//add Address
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[7]")).click();
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[1]/a[1]")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[2]/div/input")).clear();
			String Addressline1 = ExcelUtils.getCellData(i,25);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[2]/div/input")).sendKeys(Addressline1);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[5]/div/input")).clear();
			String Address_p = ExcelUtils.getCellData(i,25);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[5]/div/input")).sendKeys(Address_p);
			Thread.sleep(3000);
			//max length
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[10]/div/input")).clear();
			String Address_Max = ExcelUtils.getCellData(i,22);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/div[10]/div/input")).sendKeys(Address_Max);
			Thread.sleep(3000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[11]/div[3]/div/a")).click();
						
			System.out.println("Add City");
			//Edit City
		    driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[1]/a[1]")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[2]/div/input")).clear();
			String City = ExcelUtils.getCellData(i,26);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[2]/div/input")).sendKeys(City);
			Thread.sleep(3000);
					
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[5]/div/input")).clear();
			String City_p = ExcelUtils.getCellData(i,26);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[5]/div/input")).sendKeys(City_p);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[10]/div/input")).clear();
			String City_max = ExcelUtils.getCellData(i,22);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/div[10]/div/input")).sendKeys(City_max);
			Thread.sleep(3000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[12]/div[3]/div/a")).click();
					
			System.out.println("Add State");
			//Edit State
		    driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[1]/a[1]")).click();
			                             
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[2]/div/input")).clear();
			String State = ExcelUtils.getCellData(i,27);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[2]/div/input")).sendKeys(State);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[5]/div/input")).clear();
			String State_p = ExcelUtils.getCellData(i,27);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[5]/div/input")).sendKeys(State_p);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[10]/div/input")).clear();
			String State_max = ExcelUtils.getCellData(i,22);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[3]/div/div[10]/div/input")).sendKeys(State_max);
			Thread.sleep(3000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[13]/div[1]/a[1]")).click();
			                             
			System.out.println("Add Pincode");	
			//Edit pincode
		    driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[1]/a[1]")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[2]/div/input")).clear();
			String Pincode = ExcelUtils.getCellData(i,28);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[2]/div/input")).sendKeys(Pincode);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[4]/div/input")).clear();
			String  Pincode_min = ExcelUtils.getCellData(i,29);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[4]/div/input")).sendKeys(Pincode_min);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[5]/div/input")).clear();
			String Pincode_max = ExcelUtils.getCellData(i,29);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/div[5]/div/input")).sendKeys(Pincode_max);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[14]/div[3]/div/a")).click();
		
			System.out.println("Add Gender");	
			//add Gender
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[2]/ul/li[8]")).click();
			// click on edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[1]/a[1]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[2]/div/input")).clear();
			String Gender = ExcelUtils.getCellData(i,30);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[2]/div/input")).sendKeys(Gender);
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[4]/div/input")).clear();
			String Gender_p = ExcelUtils.getCellData(i,30);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[4]/div/input")).sendKeys(Gender_p);
			Thread.sleep(7000);
			//add option
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[9]/div/div/a")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[9]/div/ol/li[3]/input[2]")).clear();
			String Gender_option_label = ExcelUtils.getCellData(i,31);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[9]/div/ol/li[3]/input[2]")).sendKeys(Gender_option_label);
			Thread.sleep(6000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[9]/div/ol/li[3]/input[3]")).clear();
			String Gender_option_value = ExcelUtils.getCellData(i,31);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/div[9]/div/ol/li[3]/input[3]")).sendKeys(Gender_option_value);
			Thread.sleep(5000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[15]/div[3]/div/a")).click();
			Thread.sleep(10000);
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			
		driver.get(strng);  
		String name = ExcelUtils.getCellData(i,32);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
		String mobile1 = ExcelUtils.getCellData(i,33);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

		String email = ExcelUtils.getCellData(i,34);
		driver.findElement(By.id("invd_email")).sendKeys(email);
		String payment = ExcelUtils.getCellData(i,35);
		driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
		
		String amount = ExcelUtils.getCellData(i,36);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
		//click on checkbox
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[6]/div/input")).click();
		
		String number = ExcelUtils.getCellData(i,37);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[7]/div/input")).sendKeys(number);
		
		//dropdown
		WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[8]/div/select")); 
		Select mySelect= new Select(mySelectElm);
		mySelect.selectByVisibleText("chocolate");
		Thread.sleep(3000);
		
		String TextField = ExcelUtils.getCellData(i,38);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[9]/div/input")).sendKeys(TextField);
		String TextArea1 = ExcelUtils.getCellData(i,39);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[10]/div/textarea")).sendKeys(TextArea1);
		String address = ExcelUtils.getCellData(i,40);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[11]/div/input")).sendKeys(address);
		
		String city = ExcelUtils.getCellData(i,41);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[12]/div/input")).sendKeys(city);
		String state = ExcelUtils.getCellData(i,42);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[13]/div/input")).sendKeys(state);
		String pincode = ExcelUtils.getCellData(i,29);
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[14]/div/input")).sendKeys(pincode);
		
		//dropdown
		WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[15]/div/select")); 
		Select mySelect1= new Select(mySelectElm1);
		mySelect1.selectByVisibleText("Female");
		Thread.sleep(3000);
	
		//click on generate
		driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[3]/div[2]/button")).click();
    
		driver.get(Constant.URL);
		Thread.sleep(3000);		
					}
			}


		@Test 
		public  void paymentform_mandatoryfiled_Name() throws Exception  
		{	
			System.out.println("-------------------------------------");
			System.out.println("paymentform_mandatoryfiled_Name Start");
			System.out.println("-------------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			driver.get(strng);  
	
			//String name = ExcelUtils.getCellData(i,32);
			//driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
			
			String mobile1 = ExcelUtils.getCellData(i,33);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

			String email = ExcelUtils.getCellData(i,34);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			
			String payment = ExcelUtils.getCellData(i,35);
			driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
			
			String amount = ExcelUtils.getCellData(i,36);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
			
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[3]/div[2]/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/span")).getText().equalsIgnoreCase("The Name1: field is required."))
			{                            
				System.out.println("pass :The Name1: field is required.");
			}
			else
			{
				System.out.println("Fail:The Name1: field is required.");
			}
			driver.get(Constant.URL);
			Thread.sleep(3000);		
			
			}
		}

		@Test 
		public  void paymentform_mandatoryfiled_Mobile() throws Exception  
		{	
			System.out.println("---------------------------------------");
			System.out.println("paymentform_mandatoryfiled_Mobile Start");
			System.out.println("---------------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			driver.get(strng);  
	
			String name = ExcelUtils.getCellData(i,32);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
			
			//String mobile1 = ExcelUtils.getCellData(i,33);
			//driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

			String email = ExcelUtils.getCellData(i,34);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			
			String payment = ExcelUtils.getCellData(i,35);
			driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
			
			String amount = ExcelUtils.getCellData(i,36);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
			
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/form/div[2]/div[2]/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/span")).getText().equalsIgnoreCase("The mobile1 field is required."))
			{                            
				System.out.println("pass :The mobile1 field is required.");
			}
			else
			{
				System.out.println("Fail:The mobile1 field is required.");
			}
			driver.get(Constant.URL);
			Thread.sleep(3000);		
			
			}
		}


		@Test 
		public  void paymentform_mandatoryfiled_Email() throws Exception  
		{	
			System.out.println("---------------------------------------");
			System.out.println("paymentform_mandatoryfiled_Email Start");
			System.out.println("---------------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			driver.get(strng);  
	
			String name = ExcelUtils.getCellData(i,32);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
			
			//String mobile1 = ExcelUtils.getCellData(i,33);
			//driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

			String email = ExcelUtils.getCellData(i,34);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			
			String payment = ExcelUtils.getCellData(i,35);
			driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
			
			String amount = ExcelUtils.getCellData(i,36);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
			
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[3]/div[2]/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[3]/div/span")).getText().equalsIgnoreCase("The email1 field is required."))
			{                            
				System.out.println("pass :The email1 field is required.");
			}
			else
			{
				System.out.println("Fail:The email1 field is required.");
			}
			driver.get(Constant.URL);
			Thread.sleep(3000);		
			}
		}

		@Test 
		public  void paymentform_mandatoryfiled_Payment() throws Exception  
		{	
			System.out.println("----------------------------------------");
			System.out.println("paymentform_mandatoryfiled_Payment Start");
			System.out.println("----------------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			driver.get(strng);  
	
			String name = ExcelUtils.getCellData(i,32);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
			
			String mobile1 = ExcelUtils.getCellData(i,33);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

			String email = ExcelUtils.getCellData(i,34);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			
			//String payment = ExcelUtils.getCellData(i,35);
			//driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
			
			String amount = ExcelUtils.getCellData(i,36);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
			
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[3]/div[2]/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[4]/div/span")).getText().equalsIgnoreCase("The payment1 field is required."))
			{                            
				System.out.println("pass :The payment1 field is required.");
			}
			else
			{
				System.out.println("Fail:The payment1 field is required.");
			}
			driver.get(Constant.URL);
			Thread.sleep(3000);		
			
			}
		}


		@Test 
		public  void paymentform_mandatoryfiled_Amount() throws Exception  
		{	
			System.out.println("---------------------------------------");
			System.out.println("paymentform_mandatoryfiled_Amount Start");
			System.out.println("---------------------------------------");
			for(int i=1 ; i<2; i++ ) {
				
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_paymentform,"Sheet1");
			driver.findElement(By.xpath(Xpath.paymentform)).click();
				
			String expectedTitle="TraknPay Payment Forms";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Payment Forms");
		
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
		
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("pafo_form_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
					
			String Description = ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("pafo_form_description")).sendKeys(Description);
			Thread.sleep(1000);
			//Name edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).clear();
			String Name = ExcelUtils.getCellData(i,3);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[2]/div/input")).sendKeys(Name);
			Thread.sleep(1000);
						
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).clear();
			String Name_place_holder = ExcelUtils.getCellData(i,4);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/div[5]/div/input")).sendKeys(Name_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[1]/div[3]/div/a")).click();
			
			//Name mobile
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).clear();
			String mobile = ExcelUtils.getCellData(i,5);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[2]/div/input")).sendKeys(mobile);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).clear();
			String mobile_place_holder = ExcelUtils.getCellData(i,6);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/div[5]/div/input")).sendKeys(mobile_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[2]/div[3]/div/a")).click();
		
			//email
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).clear();
			String email_label = ExcelUtils.getCellData(i,7);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[2]/div/input")).sendKeys(email_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).clear();
			String email_place_holder = ExcelUtils.getCellData(i,8);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/div[5]/div/input")).sendKeys(email_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[3]/div[3]/div/a")).click();
			
			//Purpose of payment
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).clear();
			String payment_label = ExcelUtils.getCellData(i,9);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[2]/div/input")).sendKeys(payment_label );
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).clear();
			String payment_place_holder = ExcelUtils.getCellData(i,10);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/div[5]/div/input")).sendKeys(payment_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[4]/div[3]/div/a")).click();
			
		
			//Name Amount
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[1]/a")).click();
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).clear();
			String Amount_label = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[2]/div/input")).sendKeys(Amount_label );
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).clear();
			String Amount_place_holder = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/div[5]/div/input")).sendKeys(Amount_place_holder);
			Thread.sleep(2000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div/div[2]/span/div/div[1]/ul/li[5]/div[3]/div/a")).click();
			
			//add
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(3000);
		
			WebElement element = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]/a"));
			String strng = element.getText();
			driver.get(strng);  
	
			String name = ExcelUtils.getCellData(i,32);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/input")).sendKeys(name);
			
			String mobile1 = ExcelUtils.getCellData(i,33);
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/input")).sendKeys(mobile1);

			String email = ExcelUtils.getCellData(i,34);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			
			String payment = ExcelUtils.getCellData(i,35);
			driver.findElement(By.id("invd_l[1]")).sendKeys(payment);
			
			//String amount = ExcelUtils.getCellData(i,36);
			//driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/input")).sendKeys(amount);
			
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[3]/div[2]/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div[2]/form/div[5]/div/span")).getText().equalsIgnoreCase("The Amount1 field is required."))
			{                            
				System.out.println("pass :The Amount1 field is required.");
			}
			else
			{
				System.out.println("Fail:The Amount1 field is required.");
			}
			driver.get(Constant.URL);
			Thread.sleep(3000);		
			
			}
		}

}
