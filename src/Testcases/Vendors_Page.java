package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class Vendors_Page {

	public static WebDriver driver = new FirefoxDriver();

	private static WebElement element = null;
	private static String Name;
	
	@BeforeTest
	public void beforeTest() 
	{
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}

	@Test
	public void vendor_addvendor() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{

			System.out.println("Edit vendors");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);


			driver.findElement(By.xpath(Xpath.addvendors)).click();

			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 

			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/input")).sendKeys(vendorname);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/input")).sendKeys(vendoremail);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/input")).sendKeys(vendorphone);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/input")).sendKeys(vendoradress);


				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/input")).sendKeys(Bankaccountno);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/input")).sendKeys(Accounthholdername);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/input")).sendKeys(BankIFSC);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[8]/div/input")).sendKeys(Bankname);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/input")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
			
		}
	}

	@Test
	public void vendor_editvendor() throws Exception
	{

		for(int i=1 ; i<2; i++ ) {

			System.out.println("Edit vendors");
			System.out.println("--------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			//driver.findElement(By.xpath(Xpath.managevendors)).click();
			driver.findElement(By.xpath(Xpath.managevendors)).click();
			String expectedTitle = "TraknPay Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			


			String Editvendorcontactno = ExcelUtils.getCellData(i,11);
			

				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[10]/a[1]")).click();
				Thread.sleep(2000);
				driver.findElement(By.name("bven_vendor_contact_num")).clear();
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(Editvendorcontactno);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
		}
	}

	@Test
	public void vendor_addvendorbutton() throws Exception
	{

		
		for(int i=1 ; i<2; i++ ) {

			System.out.println("Add vendors button");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.managevendors)).click();
			String expectedTitle = "TraknPay Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 


			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);


				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				Thread.sleep(2000);
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();


				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
		}
	}


	@Test	   	    
	public void vendor_mandatorynamefield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			System.out.println("vendor_mandatory name field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			

			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();



				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Vendor Name field is required."))

				{
					System.out.println("pass:The Vendor Name field is required");
					
				}
				else
				{

					System.out.println("fail:The Vendor Name field is required");
					
				}
		}

	}



	@Test	   	    
	public void vendor_mandatorEmailfield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			System.out.println("vendor_mandatory email field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				//driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/p")).getText().equalsIgnoreCase("The Vendor Email field is required."))

				{
					System.out.println("pass:The Vendor Email field is required");
					

				}
				else
				{

					System.out.println("fail:The Vendor Email field is required");
					
					
				}
		}
	}


	@Test	   	    
	public void vendor_mandatorycontactnofield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("vendor_mandatory number field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			// String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				//driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);

				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Vendor Contact No field is required."))

				{
					System.out.println("pass:The Vendor contact no field is required");
					
				}
				else
				{

					System.out.println("fail:The Vendor contact no field is required");
	
					
				}

		}
	}	


	@Test	   	    
	public void vendor_mandatoryaddressfield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("vendor_mandatory adress field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			// String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				// driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[4]/div/p")).getText().equalsIgnoreCase("The Vendor Address field is required."))

				{
					System.out.println("pass:The Vendor address field is required");
					
				}
				else
				{
					System.out.println("fail:The Vendor address field is required");

					
				}

		}


	}	

	@Test	   	    
	public void vendor_mandatoryaccountnofield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("vendor_mandatory account no field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			//String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				//driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/p")).getText().equalsIgnoreCase("The Vendor account number field is required."))

				{
					System.out.println("pass:The Vendor account number field is required");
					
				}
				else
				{

					System.out.println("fail:The Vendor account number field is required");
					
				}

		}
	}	




	@Test	   	    
	public void vendor_mandatoryaccountholdernamefield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("vendor_mandatory account holder name field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			//String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			

				// driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				// driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div/p")).getText().equalsIgnoreCase("The Vendor holder name field is required."))

				{
					System.out.println("pass:The Vendor holder name field is required");
					

				}
				else
				{
					System.out.println("fail:The Vendor holder name field is required");
					
				}

				
		}
	}	


	@Test	   	    
	public void vendor_mandatoryIFSCcodefield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			System.out.println("vendor_mandatory IFSC code field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			//String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

		
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				//driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div/p")).getText().equalsIgnoreCase("The Vendor IFSC field is required."))

				{
					System.out.println("pass:The Vendor holder name field is required");
					
				}
				else
				{

					System.out.println("fail:The Vendor holder name field is required");
					
					
				}

		}
	}



	@Test	   	    
	public void vendor_mandatorybanknamefield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			System.out.println("vendor_mandatory bank name field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			//String Bankname = ExcelUtils.getCellData(i,8);
			String Bankbranch = ExcelUtils.getCellData(i,9);

			
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				//driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[8]/div/p")).getText().equalsIgnoreCase("The Bank name field is required."))

				{
					System.out.println("pass:The Vendor bank name field is required");
					
				}
				else
				{
					System.out.println("fail:The Vendor bank name field is required");
				}

				}

				
	}	



	@Test	   	    
	public void vendor_mandatorybankbranchfield() throws Exception
	{
		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			System.out.println("vendor_mandatory bank branch name field");
			System.out.println("--------------------------------");


			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.addvendors)).click();
			String expectedTitle = "TraknPay Business Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			Name=ExcelUtils.getCellData(i,1)+formattedDate;
			String vendorname =Name; 
			String vendoremail = ExcelUtils.getCellData(i,2);
			String vendorphone = ExcelUtils.getCellData(i,3);
			String vendoradress = ExcelUtils.getCellData(i,4);
			String Bankaccountno = ExcelUtils.getCellData(i,5);
			String Accounthholdername = ExcelUtils.getCellData(i,6);
			String BankIFSC = ExcelUtils.getCellData(i,7);
			String Bankname = ExcelUtils.getCellData(i,8);
			// String Bankbranch = ExcelUtils.getCellData(i,9);

			
				//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();
				driver.findElement(By.name("bven_vendor_name")).sendKeys(vendorname);
				driver.findElement(By.name("bven_vendor_contact_email")).sendKeys(vendoremail);
				driver.findElement(By.name("bven_vendor_contact_num")).sendKeys(vendorphone);
				driver.findElement(By.name("bven_vendor_contact_address")).sendKeys(vendoradress);


				driver.findElement(By.name("bvac_account_number")).sendKeys(Bankaccountno);
				driver.findElement(By.name("bvac_account_name")).sendKeys(Accounthholdername);
				driver.findElement(By.name("bvac_ifsc_code")).sendKeys(BankIFSC);
				driver.findElement(By.name("bvac_bank_name")).sendKeys(Bankname);
				// driver.findElement(By.name("bvac_bank_branch")).sendKeys(Bankbranch);
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

				if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[9]/div/p")).getText().equalsIgnoreCase("The Bank branch field is required."))

				{
					System.out.println("pass:The Vendor bank name field is required");
					
				}
				else
				{
					System.out.println("fail:The Vendor bank name field is required");
					

				}

				
		}
	}	



	@Test
	public void vendor_filters() throws Exception
	{

		
		for(int i=1 ; i<2; i++ ) {

			System.out.println("filter search");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			
				driver.findElement(By.xpath(Xpath.managevendors)).click();
				String expectedTitle = "TraknPay Vendors";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");

				//Filters
				//vendor id
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement element11=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
				String strng11 = element11.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng11);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);

				//vendor name
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement element111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
				String strng111 = element111.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng111);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
				
				//vendor email
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement element1111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]"));
				String strng1111 = element1111.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng1111);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);

				//vendor phone
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement element11111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
				String strng11111 = element11111.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng11111);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
				
				//vendor adress
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement element111111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]"));
				String strng111111 = element111111.getText();
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(strng111111);
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(Keys.RETURN);
				
				//vendor status
				//Active
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
				Select mySelect= new Select(mySelectElm);
				mySelect.selectByVisibleText("Active");
				Thread.sleep(3000); 
				
				//Not active
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm2 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
				Select mySelect2= new Select(mySelectElm2);
				mySelect2.selectByVisibleText("Not Active");
				Thread.sleep(3000); 
				
				//Approved
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/select"));
				Select mySelect1= new Select(mySelectElm1);
				mySelect1.selectByVisibleText("Approved");
				Thread.sleep(3000); 
				
				//Not approved
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm3 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/select"));
				Select mySelect3= new Select(mySelectElm3);
				mySelect3.selectByVisibleText("Not Approved");
				Thread.sleep(3000);
				
				//Entries
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
				Select mySelect11= new Select(mySelectElm11);
				mySelect11.selectByVisibleText("25");
				
		}
	}

	@Test
	public void vendor_activateanddeactivate() throws Exception
	{

		
		for(int i=1 ; i<2; i++ ) {

			System.out.println("Activate and deactivate vendor");
			System.out.println("--------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

				driver.findElement(By.xpath(Xpath.managevendors)).click();
				String expectedTitle = "TraknPay Vendors";
				String actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");


				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
				Select mySelect= new Select(mySelectElm);
				mySelect.selectByVisibleText("Active");
				Thread.sleep(2000); 
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[8]/a")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
				
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
				Select mySelect1= new Select(mySelectElm1);
				mySelect1.selectByVisibleText("Not Active");
				Thread.sleep(2000); 
				
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[8]/a")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
				Thread.sleep(2000); 
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
				
				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
		}
	}

	@Test
	public void vendor_split() throws Exception
	{

		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("vendors in split payment");
			System.out.println("--------------------------");

		
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);



			driver.findElement(By.xpath(Xpath.managevendors)).click();
			String expectedTitle = "TraknPay Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");

		
				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/button")).click();
				WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/select"));
				Select mySelect= new Select(mySelectElm);
				mySelect.selectByVisibleText("Active");
				Thread.sleep(3000); 


				WebElement mySelectElm1 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[9]/select"));
				Select mySelect1= new Select(mySelectElm1);
				mySelect1.selectByVisibleText("Approved");
				Thread.sleep(3000); 


				WebElement element11111=driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[2]/div"));
				String strng11111 = element11111.getText();



				driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[3]/a/span")).click();
				driver.findElement(By.xpath(".//*[@id='invd_split_pay_flag']")).click();

				Select dropdown1 = new Select(driver.findElement(By.name("invs_bven_id[0]")));
				dropdown1.selectByVisibleText(strng11111);
				System.out.println("vendor name: " +strng11111);

		}
	}

	@Test
	public void vendor_delete() throws Exception
	{

		
		for(int i=1 ; i<2; i++ ) {

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("vendors delete");
			System.out.println("--------------------------");

			

			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);



			driver.findElement(By.xpath(Xpath.managevendors)).click();
			String expectedTitle = "TraknPay Vendors";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Vendors,"Sheet1");

			

				driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[4]/td[8]/a[2]")).click();
				driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
				expectedTitle = "TraknPay Vendors";
				actualTitle = driver.getTitle();
				Assert.assertEquals(expectedTitle,actualTitle);
				
	}

}

	
	@Test
	public  void vendor_sessiontimeout() throws Exception 
	{	

		System.out.println("vendor page session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			
			driver.findElement(By.xpath(Xpath.managevendors)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 
			

			
		}
}
}

