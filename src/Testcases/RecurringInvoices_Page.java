package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class RecurringInvoices_Page {
	public static WebDriver driver = new FirefoxDriver();
	private static WebElement element = null;
	private static String Name;
	private static String scheduleName;
	private static String designName;

	@BeforeTest
	public void beforeTest() 
	{

		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}

	@AfterMethod
	public void aftermethod() throws Exception 
	{
		Thread.sleep(2000);
		LogOut_Page.logout(driver);

	}

	@Test
	public void singleinvoice_weekly_recurringinvoice() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice in single invoice");
			System.out.println("--------------------------------");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1s,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			//String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			//String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);

			//try{
			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			// driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			//driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(3000);


			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);

			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 

			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			expectedTitle = "TraknPay Create Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);


		}


	}

	@Test
	public void recurringinvoice_schedulenamemandatory() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice schedule name mandatory");
			System.out.println("--------------------------------");


			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf2.format(date2);
			Name=ExcelUtils.getCellData(i,4)+formattedDate2;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[2]/div[2]/div/div/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div[1]/div[1]/div/div/p")).getText().equalsIgnoreCase("The Schedule Name field is required when Recurring Invoice is present."))

			{
				System.out.println(" pass:schedule Name field is required");

			}
			else
			{

				System.out.println(" fail:schedule Name field is required");


			}

		}

	}
	@Test
	public void recurringinvoice_scheduleenddatemandatory() throws Exception
	{

		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice schedule end date mandatory");
			System.out.println("--------------------------------");

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName; 
			//String scheduleenddate = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div[2]/div[2]/div/div/p")).getText().equalsIgnoreCase("The Schedule End Date field is required when Recurring Invoice is present."))

			{
				System.out.println(" Error:The Schedule End Date field is required when Recurring Invoice is present.");

			}
			else
			{
				System.out.println(" Error:The Schedule End Date field is required when Recurring Invoice is present.");


			} 

		}


	}

	@Test
	public void recurringinvoice_duedaysmandatory() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice due days mandatory");
			System.out.println("--------------------------------");
			//load recurring invoice excel


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName; 
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			//String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			//Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div[3]/div[2]/div/div/p")).getText().equalsIgnoreCase("The Invoice Due Within field is required when Recurring Invoice is present."))

			{
				System.out.println("Pass:The Invoice Due Within field is required when Recurring Invoice is present.");


			}
			else
			{

				System.out.println("Pass:The Invoice Due Within field is required when Recurring Invoice is present.");

			}


		}

	}

	@Test
	public void recurringinvoice_scheduleenddate_beforestartdate() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice end date before start date");
			System.out.println("--------------------------------");

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName; 
			String beforescheduleenddate = ExcelUtils.getCellData(i,14);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[2]/div[2]/div/div/input")).sendKeys(beforescheduleenddate);
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li[2]")).getText().equalsIgnoreCase("The Schedule End Date must be a date after Schedule Start Date."))

			{
				System.out.println("Pass:The Schedule End Date must be a date after Schedule Start Date.");

			}
			else
			{

				System.out.println("fail:The Schedule End Date must be a date after Schedule Start Date.");

			}
		}

	}



	@Test
	public void recurringinvoice_Addbutton() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("Add button in recurring invoice");
			System.out.println("--------------------------------");


			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			String expectedTitle = "TraknPay List Recurring Invoice Schedules";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay List Recurring Invoice Schedules");
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[1]/div/a")).click();

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			expectedTitle = "TraknPay Create Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

		}
	}


	@Test
	public void recurringinvoice_editdisabled() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice edit button disable");
			System.out.println("--------------------------------");

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(1000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(1000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			//driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[2]/div[2]/div/div/input")).sendKeys(Keys.RETURN);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(1000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//send invoice if occurence is 4
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[3]/span")).click();
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[3]/span")).click();
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[3]/span")).click();
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[3]/span")).click();
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);

			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);



		}

	}

	@Test
	public void recurringinvoice_editinvoiceschedule() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice edit invoice schedule");
			System.out.println("--------------------------------");


			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;

			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String editinvoiceoccurence = ExcelUtils.getCellData(i,18);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 

			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//edit invoice schedule
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[8]/a[1]/span")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/section[1]/div/div[2]/form/div/div[5]/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/section[1]/div/div[2]/form/div/div[5]/div/input")).sendKeys(editinvoiceoccurence);


		}

	}
	@Test
	public void recurringinvoice_editinvoice() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice edit invoice");
			System.out.println("--------------------------------");

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;

			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String editinvoiceoccurence = ExcelUtils.getCellData(i,18);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 

			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);
			String editItem1cost= ExcelUtils.getCellData(i,20);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//edit invoice item cost
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[8]/a[1]/span")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/section[2]/div/div[2]/div/table/tbody/tr[2]/td[3]/a/span")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/div[4]/div[2]/table/tbody/tr[2]/td[3]/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[3]/div/div[4]/div[2]/table/tbody/tr[2]/td[3]/input")).sendKeys(editItem1cost);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();

		}

	}


	@Test
	public void recurringinvoice_backbutton() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice back button");
			System.out.println("--------------------------------");
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]/a[1]/span")).click();

			String expectedTitle = "TraknPay Update Invoice Schedule";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Update Invoice Schedule");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/section[1]/div/div[1]/div/a")).click();

			expectedTitle = "TraknPay List Recurring Invoice Schedules";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay List Recurring Invoice Schedules");

		}

	}

	@Test
	public void recurringinvoice_invoiceoccurence_lessthan2() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("invoice occurence less than 2");
			System.out.println("--------------------------------");

			//load recurring invoice excel

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurencelessthan2 = ExcelUtils.getCellData(i,22);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurencelessthan2);
			Thread.sleep(3000);
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Weekly");
			Thread.sleep(3000); 


			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[7]/div[3]/div[1]/div/div/p")).getText().equalsIgnoreCase("The Invoice Occurrence must be at least 2."))

			{
				System.out.println("pass:The Invoice Occurrence must be at least 2.");

			}
			else
			{

				System.out.println("fail:The Invoice Occurrence must be at least 2.");

			}


		}
	}
	@Test
	public void monthly_recurringinvoice() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("Monthly recurring invoice");
			System.out.println("--------------------------------");


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);

			//weekly invoice
			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Monthly");
			Thread.sleep(3000); 

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);

		}

	}
	@Test
	public void Quarterly_recurringinvoice() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("Quarterly recurring invoice");
			System.out.println("--------------------------------");


			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);

			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(3000);

			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Quarterly");
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(3000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(3000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);

		}
	}

	@Test
	public void Yearly_recurringinvoice() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("Yearly recurring invoice");
			System.out.println("--------------------------------");

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RecurringInvoice,"Sheet1");
			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate1 = sdf1.format(date1);
			scheduleName=ExcelUtils.getCellData(i,1)+formattedDate1;
			String schedulename = scheduleName;
			String invoiceoccurence = ExcelUtils.getCellData(i,2);
			String duewithindays = ExcelUtils.getCellData(i,3);
			Date date2 = new Date();
			SimpleDateFormat sdf2 = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate2 = sdf1.format(date1);
			Name=ExcelUtils.getCellData(i,4)+formattedDate1;
			String name =Name; 
			String Phone = ExcelUtils.getCellData(i,5);
			String Email = ExcelUtils.getCellData(i,6);
			String Fieldfive = ExcelUtils.getCellData(i,7);
			String Fieldsix= ExcelUtils.getCellData(i,8);
			String Item1cost= ExcelUtils.getCellData(i,9);


			driver.findElement(By.xpath(Xpath.singleinvoicing)).click();
			String expectedTitle = "TraknPay Create Invoice";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			//Recurring invoice
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[5]/div/div/div/div/input")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[1]/div/div/input")).sendKeys(schedulename);
			Thread.sleep(2000);

			WebElement mySelectElm = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[1]/div[2]/div/div/select"));
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Yearly");
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).clear();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[1]/div/div/input")).sendKeys(invoiceoccurence);
			Thread.sleep(2000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[6]/div[3]/div[2]/div/div/input")).sendKeys(duewithindays);
			Thread.sleep(2000);

			driver.findElement(By.name("invd_name")).sendKeys(name);
			System.out.println("customer name: " +name);
			Thread.sleep(2000);
			driver.findElement(By.name("invd_phone")).sendKeys(Phone);
			driver.findElement(By.name("invd_email")).sendKeys(Email);
			driver.findElement(By.name("invd_f5")).sendKeys(Fieldfive);
			driver.findElement(By.name("invd_f6")).sendKeys(Fieldsix);
			driver.findElement(By.name("invd_n1")).sendKeys("Welcome");
			Thread.sleep(2000);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(Item1cost);
			driver.findElement(By.name("invd_n2")).sendKeys("Thank you");
			Thread.sleep(2000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);
			expectedTitle = "TraknPay Show Invoice";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice page");

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div[1]/div/a[2]")).click();
			Thread.sleep(3000);
			driver.findElement(By.className("confirm")).click();
			Thread.sleep(3000);


			expectedTitle = "TraknPay List Invoices";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay list invoice page");
			Thread.sleep(2000);
			//check recurring invoice in list of invoices
			//Clear filter
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(name);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div[2]/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//check invoice in recurring invoice
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(schedulename);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);

		}

	}
	@Test
	public void recurringinvoice_filters() throws Exception
	{
		for(int i=1 ; i<2; i++ ) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			System.out.println("recurring invoice filters");
			System.out.println("--------------------------------");
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();

			//Filter schedule
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement element=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Filter schedule name
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement element1=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]/div"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Invoice occurence
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement element11=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]"));
			String strng11 = element11.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng11);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Filter due date
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement element111=driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]"));
			String strng111 = element111.getText();
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng111);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);

			//Schedule frequency weeks
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement mySelectElm12 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect12= new Select(mySelectElm12);
			mySelect12.selectByVisibleText("Weeks");
			Thread.sleep(2000);

			//Schedule frequency months
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement mySelectElm121 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect121= new Select(mySelectElm121);
			mySelect121.selectByVisibleText("Months");
			Thread.sleep(2000);

			//Schedule frequency Quarters
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement mySelectElm1211 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect1211= new Select(mySelectElm1211);
			mySelect1211.selectByVisibleText("Quarters");
			Thread.sleep(2000);

			//Schedule frequency years
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement mySelectElm12111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[7]/select")); 
			Select mySelect12111= new Select(mySelectElm12111);
			mySelect12111.selectByVisibleText("Years");
			Thread.sleep(2000);

			//show entries
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/button")).click();
			WebElement mySelectElm121111 = driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div/label/select")); 
			Select mySelect121111= new Select(mySelectElm121111);
			mySelect121111.selectByVisibleText("10");

		}
	}

	@Test
	public  void recurringinvoice_sessiontimeout() throws Exception 
	{	

		System.out.println("recurring invoice page session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData1s,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			
			driver.findElement(By.xpath(Xpath.recurringinvoice)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			System.out.println(dateFormat.format(date)); 
		
}
	}



}
