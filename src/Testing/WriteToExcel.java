package Testing;

import java.io.File;
import java.io.FileInputStream;
import Common.Constant;
import java.io.FileOutputStream;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.ComparisonFailure;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;



public class WriteToExcel {
	public static String writedatapath = "C:\\eclipse\\workspace\\TraknPay_testing\\testdocs";
	private static String Name;
/*	 public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException{

	      //Create a object of File class to open xlsx file

	        File file =    new File(filePath+"\\"+fileName);

	        //Create an object of FileInputStream class to read excel file

	        FileInputStream inputStream = new FileInputStream(file);

	        Workbook guru99Workbook = null;

	        //Find the file extension by spliting file name in substing and getting only extension name

	        String fileExtensionName = fileName.substring(fileName.indexOf("."));

	        //Check condition if the file is xlsx file

	        if(fileExtensionName.equals(".xlsx")){

	        //If it is xlsx file then create object of XSSFWorkbook class

	        guru99Workbook = new XSSFWorkbook(inputStream);

	        }

	        //Check condition if the file is xls file

	        else if(fileExtensionName.equals(".xlsx")){

	            //If it is xls file then create object of XSSFWorkbook class

	            guru99Workbook = new HSSFWorkbook(inputStream);
              
	        }

	        
	        System.out.println("value of 1:"+guru99Workbook);
	    //Read excel sheet by sheet name    

	    Sheet sheet = guru99Workbook.getSheet(sheetName);
	    System.out.println("value of sheet:"+sheet);

	    //Get the current count of rows in excel file

         int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
	     System.out.println("row count is:"+rowCount);

	    //Get the first row from the sheet

	    Row row = sheet.getRow(0);

	    //Create a new row and append it at last of sheet

	    Row newRow = sheet.createRow(rowCount+1);

	    //Create a loop over the cell of newly created Row

	    for(int j = 0; j < row.getLastCellNum(); j++){

	        //Fill data in row

	        Cell cell = newRow.createCell(j);

	        cell.setCellValue(dataToWrite[j]);

	    }

	    //Close input stream

	    inputStream.close();

	    //Create an object of FileOutputStream class to create write data in excel file

	    FileOutputStream outputStream = new FileOutputStream(file);

	    //write data in the excel file

	    guru99Workbook.write(outputStream);

	    //close output stream

	    outputStream.close();

	    

	    }

	    

	    public static void main(String args[]) throws IOException{

	        //Create an array with the data in the same order in which you expect to be filled in excel file

	        String[] valueToWrite = {"hotel management","8786876876"};
	        //Create an object of current class

	        WriteToExcel objExcelFile = new WriteToExcel();
	        
	        System.out.println("value is "+valueToWrite[0]);

	        //Write the file using file name , sheet name and the data to be filled

	        //objExcelFile.writeExcel(System.getProperty("user.dir")+"D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files","shravya.xlsx","Sheet1",valueToWrite);
	        objExcelFile.writeExcel(System.getProperty("user.dir")+"\\InputFiles\\excel files","shravya.xlsx","Sheet1",valueToWrite);
	    }

	}

		
	*/
	/*
	     	@Test
		 public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException{

		        //Create a object of File class to open xlsx file
	     		sheetName="Invoice Data";
		        File file =    new File(filePath+"\\"+fileName);

		        //Create an object of FileInputStream class to read excel file

		        FileInputStream inputStream = new FileInputStream(file);

		        Workbook guru99Workbook = null;

		        //Find the file extension by spliting file name in substing and getting only extension name

		        String fileExtensionName = fileName.substring(fileName.indexOf("."));

		        //Check condition if the file is xlsx file

		        if(fileExtensionName.equals(".xlsx")){

		        //If it is xlsx file then create object of XSSFWorkbook class

		        guru99Workbook = new XSSFWorkbook(inputStream);

		        }

		  

		    //Read excel sheet by sheet name    
		        System.out.println("sheet name:"+sheetName);
		        
		    Sheet sheet = guru99Workbook.getSheet(sheetName);
		   
		    System.out.println("value of sheet is: "+sheet);
		    //Get the current count of rows in excel file

		    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
		    System.out.println("rowcount: "+rowCount);
		    //Get the first row from the sheet

		    Row row = sheet.getRow(0);

		    //Create a new row and append it at last of sheet

		    Row newRow = sheet.createRow(rowCount+1);

		    //Create a loop over the cell of newly created Row

		    for(int j = 0; j < row.getLastCellNum(); j++){

		        //Fill data in row

		        Cell cell = newRow.createCell(j);

		        cell.setCellValue(dataToWrite[j]);

		    }

		    //Close input stream

		    inputStream.close();

		    //Create an object of FileOutputStream class to create write data in excel file

		    FileOutputStream outputStream = new FileOutputStream(file);

		    //write data in the excel file

		    guru99Workbook.write(outputStream);

		    //close output stream

		    outputStream.close();

		    

		    }

		    

		    public static void main(String args[]) throws IOException{

		        //Create an array with the data in the same order in which you expect to be filled in excel file

		        String[] valueToWrite = {"shravya","8786876876","shravya@gmail.com","hsr","5","14-jul-2016","26-jul-2016","45","B","100","100","14-jul-2016"};
		        
		        //Create an object of current class

		        WriteToExcel objExcelFile = new WriteToExcel();
		        
		        System.out.println("value is "+valueToWrite[0]);

		        //Write the file using file name , sheet name and the data to be filled

		        //objExcelFile.writeExcel(System.getProperty("user.dir")+"D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files","shravya.xlsx","Sheet1",valueToWrite);
		        objExcelFile.writeExcel(System.getProperty("user.dir")+"\\InputFiles\\excel files","excel_file_dakshi07132016163608.xlsx","Sheet1",valueToWrite);
		    }

		}

*/
	
	 @Test
	   @Parameters({"filePath","fileName","sheetName","dataToWrite"})
public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException{

    //Create a object of File class to open xlsx file
		sheetName="Invoice Data";
    File file = new File(filePath+"\\"+fileName);
    System.out.println("file name:"+file);
    //Create an object of FileInputStream class to read excel file

    FileInputStream inputStream = new FileInputStream(file);

    Workbook guru99Workbook = null;

    //Find the file extension by spliting file name in substing and getting only extension name

    String fileExtensionName = fileName.substring(fileName.indexOf("."));

    //Check condition if the file is xlsx file

    if(fileExtensionName.equals(".xlsx")){

    //If it is xlsx file then create object of XSSFWorkbook class

    guru99Workbook = new XSSFWorkbook(inputStream);

    }



//Read excel sheet by sheet name    
    System.out.println("sheet name:"+sheetName);
    
Sheet sheet = guru99Workbook.getSheet(sheetName);

System.out.println("value of sheet is: "+sheet);
//Get the current count of rows in excel file

int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
System.out.println("rowcount: "+rowCount);
//Get the first row from the sheet

Row row = sheet.getRow(0);

//Create a new row and append it at last of sheet

Row newRow = sheet.createRow(rowCount+1);

//Create a loop over the cell of newly created Row

for(int j = 0; j < row.getLastCellNum(); j++){

    //Fill data in row

    Cell cell = newRow.createCell(j);

    cell.setCellValue(dataToWrite[j]);

}

//Close input stream

inputStream.close();

//Create an object of FileOutputStream class to create write data in excel file

FileOutputStream outputStream = new FileOutputStream(file);

//write data in the excel file

guru99Workbook.write(outputStream);

//close output stream

outputStream.close();



}


public static void main(String args[]) throws Exception{

    //Create an array with the data in the same order in which you expect to be filled in excel file
    
	
	
	
	 for(int i=0 ; i<1; i++ )
		 {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Bulk_InvoiceData,"Sheet1");
		 Date date = new Date();
	     SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHH");
	     String formattedDate = sdf.format(date);
	           Name=ExcelUtils.getCellData(i,0)+formattedDate;
	           String name = Name;
			    String phoneno = ExcelUtils.getCellData(i,1);
				String email = ExcelUtils.getCellData(i,2);
				String adress = ExcelUtils.getCellData(i,3);
				String standard = ExcelUtils.getCellData(i,4);
				String invoicedate = ExcelUtils.getCellData(i,5);
				String duedate = ExcelUtils.getCellData(i,6);
				String rollno = ExcelUtils.getCellData(i,7);
				String section = ExcelUtils.getCellData(i,8);
				String schoolfee = ExcelUtils.getCellData(i,9);
				String nettotal = ExcelUtils.getCellData(i,10);
				String sendondate = ExcelUtils.getCellData(i,11);
				
				 String[] valueToWrite = {name,phoneno,email,adress,standard,invoicedate,duedate,rollno,section,schoolfee,nettotal,sendondate};
				    
    //Create an object of current class

    WriteToExcel objExcelFile = new WriteToExcel();
    
    System.out.println("value is "+valueToWrite );

    //Write the file using file name , sheet name and the data to be filled

    //objExcelFile.writeExcel(System.getProperty("user.dir")+"D:\\eclipse\\workspace\\TraknPay_testing\\testdocs\\InputFiles\\excel files","shravya.xlsx","Sheet1",valueToWrite);
    objExcelFile.writeExcel(System.getProperty("user.dir")+"\\InputFiles\\excel files","excel_file_dakshi07132016163608.xlsx","Invoice Data",valueToWrite);

    ExcelUtils.setCellData("Pass", i,12 ,Constant.Path_TestData + Constant.File_Bulk_InvoiceData);	 
		 
		 
		 }

}
			}
