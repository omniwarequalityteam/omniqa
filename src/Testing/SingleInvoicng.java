package Testing;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.thoughtworks.selenium.Wait;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;

public class SingleInvoicng {
	
	private static WebElement element = null;

	
	 public static WebElement SingleInvoicng_1(WebDriver driver) throws Exception
	 {
		 System.out.println("SingleInvoicng_1 Start");
		 
		 for(int i=1 ; i<2; i++ ) {
			 
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		     System.out.println("loaded: " +i);
		     
		     String sUserName = ExcelUtils.getCellData(i,1);
		     String sPassword = ExcelUtils.getCellData(i,2);
		     driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		     
		     
		     LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
				
		    ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");

		     
		     
		     driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[3]/a/span")).click();
		     String expectedTitle = "TraknPay Create Invoice";
		     String actualTitle = driver.getTitle();
		     Assert.assertEquals(expectedTitle,actualTitle);
		     System.out.println("Traknpay Create Invoice page");
	     
		     String DesignName = ExcelUtils.getCellData(i,1);
		    // String Name = ExcelUtils.getCellData(i,2);
			 //String Phone = ExcelUtils.getCellData(i,3);
			 String fans = ExcelUtils.getCellData(i,4);
			 String ac = ExcelUtils.getCellData(i,5);
			 String bulbs = ExcelUtils.getCellData(i,6);
			 String TV = ExcelUtils.getCellData(i,7);
			 String mediumsize = ExcelUtils.getCellData(i,8);	
			 String NetTotal = ExcelUtils.getCellData(i,9);	
			
       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    
   
       WebElement element=  driver.findElement(By.name("invd_ctmp_id"));
		  Select se=new Select(element);
			se.selectByValue(DesignName);
			
		    System.out.println("Design name: " +DesignName);
		    System.out.println("design name created successfully");
	
       
          // driver.findElement(By.name("invd_name")).sendKeys(Name);
         //  System.out.println("Name: " +Name);
		   // System.out.println("design name created successfully");
          // driver.findElement(By.name("invd_phone")).sendKeys(Phone);
           //System.out.println("Phone: " +Phone);
           
       
           try{ 
  		     driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  		     
		        driver.findElement(By.id("autocomplete-ajax")).sendKeys("asha");
		        Thread.sleep(3000);
		        List <WebElement> listItems = driver.findElements(By.xpath("html/body/div[6]"));
		        listItems.get(0).click();
	        
		    driver.findElement(By.name("invd_f5")).sendKeys(fans);
		    driver.findElement(By.name("invd_f6")).sendKeys(ac);
		    driver.findElement(By.name("invd_f7")).sendKeys(bulbs);
		    driver.findElement(By.name("invd_f8")).sendKeys(TV);
		    driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
		    driver.findElement(By.name("invd_f20")).sendKeys(NetTotal);
		    
			
			
			 Thread.sleep(3000);
			    driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			   
		     
			    expectedTitle = "TraknPay Show Invoice";
			    actualTitle = driver.getTitle();
			    Assert.assertEquals(expectedTitle,actualTitle);
			    System.out.println("Traknpay show invoice design page");
			    
			    driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div[2]/div[1]/div/a[3]")).click();
			    ac = ExcelUtils.getCellData(i,10);
			    driver.findElement(By.name("invd_f6")).clear();
			     driver.findElement(By.name("invd_f6")).sendKeys(ac);
				 Thread.sleep(7000);	    
				 mediumsize = ExcelUtils.getCellData(i,11);
				 driver.findElement(By.name("invd_l_value[1]")).clear();
			     driver.findElement(By.name("invd_l_value[1]")).sendKeys(mediumsize);
			     Thread.sleep(5000);
				 driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
				 Thread.sleep(5000);	
				 
				 expectedTitle = "TraknPay Show Invoice";
				    actualTitle = driver.getTitle();
				    Assert.assertEquals(expectedTitle,actualTitle);
				    System.out.println("Traknpay show invoice design page");
				    Thread.sleep(5000);	
				    driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div[2]/div[1]/div/a[1]")).click();
				    
				    
				 LogOut_Page.logout(driver); 
			    
			  ExcelUtils.setCellData("Pass", i,14 ,Constant.Path_TestData + Constant.File_LoginData);
		 }
	
	 
		 
		 catch (  ComparisonFailure tr)
			{
			 
				    System.out.println("Invalid input "+tr);
				    System.out.println("Login failed"); 
				    driver.findElement(By.id("bcus_name")).clear();
					   // System.out.println("Design name: " +Designname);
					   // System.out.println("design name created successfully");
				    
				    
				    driver.findElement(By.name("invd_ctmp_id")).clear();
			  
			    driver.findElement(By.name("invd_f5")).clear();
			    driver.findElement(By.name("invd_f6")).clear();
			    driver.findElement(By.name("invd_f7")).clear();
			    driver.findElement(By.name("invd_f8")).clear();
			    driver.findElement(By.name("invd_l_value[1]")).clear();
			    driver.findElement(By.name("invd_f20")).clear();
					   
				
				 ExcelUtils.setCellData("Failed",i,14,Constant.Path_TestData + Constant.File_LoginData);
			}
		     
	 }
		return element;
	 }
}
