package Common;
 
import org.openqa.selenium.By;

public class Xpath {
	
	//BIZ

	public static String createdesign  ="html/body/div[2]/aside/section/ul/li[8]/a/span";
	public static String managedesign  ="html/body/div[2]/aside/section/ul/li[9]/a/span";
	public static String payment_buttons ="html/body/div[2]/aside/section/ul/li[10]/a/span";
	
	public static String paymentform ="html/body/div[2]/aside/section/ul/li[12]/a/span";
	public static String Managecustomer ="html/body/div[2]/aside/section/ul/li[25]/a/span";
	public static String Addcustomer ="html/body/div[2]/aside/section/ul/li[26]/a/span";
	
	public static String Settlement ="html/body/div[2]/aside/section/ul/li[16]/a/span";
	public static String Settlement_Details ="html/body/div[2]/aside/section/ul/li[17]/a/span";
	public static String payment  ="html/body/div[2]/aside/section/ul/li[18]/a";
	public static String RefundRequest ="html/body/div[2]/aside/section/ul/li[20]/a/span";		

	public static String Profile ="html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a";

	
 //BizTest
	/*
	public static String createdesign  ="html/body/div[2]/aside/section/ul/li[8]/a/span";
	public static String managedesign  ="html/body/div[2]/aside/section/ul/li[9]/a/span";
	public static String payment_buttons ="html/body/div[2]/aside/section/ul/li[10]/a/span";
	
	public static String Managecustomer ="html/body/div[2]/aside/section/ul/li[25]/a/span";
	public static String Addcustomer ="html/body/div[2]/aside/section/ul/li[26]/a/span";
	public static String paymentform ="html/body/div[2]/aside/section/ul/li[12]/a/span";
	
	public static String Settlement ="html/body/div[2]/aside/section/ul/li[16]/a/span";
	public static String Settlement_Details ="html/body/div[2]/aside/section/ul/li[17]/a/span";
	public static String payment  ="html/body/div[2]/aside/section/ul/li[18]/a";
	
	public static String Profile ="html/body/div[2]/header/nav/div/ul/li[1]/ul/li[2]/div[1]/a";
	public static String RefundRequest ="html/body/div[2]/aside/section/ul/li[20]/a/span";	
	 */
	
	                            // shravya
	
	/*
	//Biz
		
		public static final String createdesign = "html/body/div[2]/aside/section/ul/li[8]/a/span";
		public static final String managedesign = "html/body/div[2]/aside/section/ul/li[9]/a/span";
		public static final String singleinvoicing = "html/body/div[2]/aside/section/ul/li[3]/a";
		public static final String manageinvoices = "html/body/div[2]/aside/section/ul/li[5]/a/span";
		public static final String bulkinvoices = "html/body/div[2]/aside/section/ul/li[4]/a/span";
		public static final String uploadexcelfile = "html/body/div[2]/aside/section/ul/li[4]/ul/li[1]/a/span";
		public static final String reviewandsendinvoices = "html/body/div[2]/aside/section/ul/li[4]/ul/li[2]/a/span";
		public static final String addvendors = "html/body/div[2]/aside/section/ul/li[33]/a/span";
		public static final String managevendors = "html/body/div[2]/aside/section/ul/li[32]/a/span";
		public static final String addproduct = "html/body/div[2]/aside/section/ul/li[30]/a/span";
		public static final String manageproduct = "html/body/div[2]/aside/section/ul/li[29]/a/span";
		public static final String addusers = "html/body/div[2]/aside/section/ul/li[36]/a/span";
		public static final String manageusers = "html/body/div[2]/aside/section/ul/li[35]/a/span";
		public static final String recurringinvoice = "html/body/div[2]/aside/section/ul/li[6]/a/span";
		public static final String NACHMandates = "html/body/div[2]/aside/section/ul/li[23]/a/span";
		public static final String NACHPullRequests = "html/body/div[2]/aside/section/ul/li[24]/a/span";
		public static final String challanpayments = "html/body/div[2]/aside/section/ul/li[20]/a/span";
		public static final String purchasecredits = "html/body/div[2]/aside/section/ul/li[15]/a/span";
		public static final String discountcodes = "html/body/div[2]/aside/section/ul/li[13]/a/span";
		
		*/

		//Biztest


		public static final String createdesign1 = "html/body/div/aside/section/ul/li[8]/a/span";
		public static final String managedesign1 = "html/body/div/aside/section/ul/li[9]/a/span";
		public static final String singleinvoicing = "html/body/div/aside/section/ul/li[3]/a/span";
		public static final String manageinvoices = "html/body/div/aside/section/ul/li[5]/a/span";
		public static final String bulkinvoices = "html/body/div/aside/section/ul/li[4]/a";
		public static final String uploadexcelfile = "html/body/div/aside/section/ul/li[4]/ul/li[1]/a/span";
		public static final String reviewandsendinvoices = "html/body/div/aside/section/ul/li[4]/ul/li[2]/a/span";
		public static final String addvendors = "html/body/div/aside/section/ul/li[32]/a/span";
		public static final String managevendors = "html/body/div/aside/section/ul/li[31]/a/span";
		public static final String addproduct = "html/body/div/aside/section/ul/li[29]/a/span";
		public static final String manageproduct = "html/body/div/aside/section/ul/li[28]/a/span";
		public static final String addusers = "html/body/div/aside/section/ul/li[35]/a/span";
		public static final String manageusers = "html/body/div/aside/section/ul/li[34]/a/span";
		public static final String recurringinvoice = "html/body/div/aside/section/ul/li[6]/a/span";
		public static final String NACHMandates = "html/body/div/aside/section/ul/li[22]/a/span	";
		public static final String NACHPullRequests = "html/body/div/aside/section/ul/li[23]/a/span";
		public static final String challanpayments = "html/body/div/aside/section/ul/li[19]/a/span";
		public static final String purchasecredits = "html/body/div/aside/section/ul/li[14]/a/span";
		public static final String discountcodes = "html/body/div/aside/section/ul/li[13]/a/span";
	
	}
