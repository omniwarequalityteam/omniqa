package Common;

public class Constant {


	public static final String URL= "https://biz.traknpay.in";
  //public static final String URL = "https://biztest.traknpay.in";   
  //public static final String URL = "https://biz.localhost.com";
	
	public static final String URL2= "https://www.gmail.com/";
	public static final String URLalert= "https://biz.traknpay.in/shravya";
	
	public static final String Path_TestData = System.getProperty("user.dir") + "//InputFiles//";
	public static final String Path_Downloads = System.getProperty("user.dir") + "//DownloadFiles//";
	   
	public static final String File_LoginData = "LoginData.xlsx";
	public static final String File_CreateDesignData1 = "CreateDesignData1.xlsx";
	public static final String File_Managedesigns_1 = "Managedesigns_1.xlsx";
	
	public static final String File_AddCustomer_1 = "AddCustomer.xlsx";
	public static final String File_Managecustomer = "Managecustomer.xlsx";
	public static final String File_payment_button = "payment_button.xlsx";
	
	public static final String File_paymentform = "paymentform.xlsx";
	public static final String File_Settlement = "Settlement.xlsx";
	public static final String File_Settlement_Details = "Settlement_Details.xlsx";
	
	public static final String File_Settlement_login = "Settlement_login.xlsx";
	public static final String File_payment = "payment.xlsx";
	public static final String File_RefundRequest= "RefundRequest.xlsx";
	    
	public static final String File_Registration_1 = "Registration_1.xlsx";
	public static final String File_New_login = "New_login.xlsx";
	public static final String File_Profile = "profile.xlsx";
	public static final String File_email_sms = "email_sms.xlsx";
	
 	
                            // shravya
	
	public static final String URL1= "http://biz.traknpay.in/auth/login";
	public static final String challanurl= "http://biz.traknpay.in/asha";
	
	public static final String File_LoginData1s = "LoginData1s.xlsx";
	public static final String File_CreateDesignData1s = "CreateDesignData1s.xlsx";
	public static final String File_AddCustomer_1s = "AddCustomer1s.xlsx";
	public static final String File_Registration_1s = "Registration_1s.xlsx";
	public static final String File_SingleInvoicingData = "SingleInvoicingData.xlsx";
	public static final String File_ManageInvoicing = "ManageInvoicing.xlsx";
	public static final String File_BulkInvoicing = "BulkInvoicing.xlsx";	  
	public static final String File_Vendors = "Vendors.xlsx";
	public static final String File_Products = "Products.xlsx";
	public static final String File_Bulk_InvoiceData= "Bulk_InvoiceData.xlsx";
	public static final String File_Users= "Users.xlsx";
	public static final String File_RecurringInvoice= "RecurringInvoice.xlsx";
	public static final String File_Nach= "NACH.xlsx";
	public static final String File_DiscountCodes= "Discountcodes.xlsx";




}


